(************** Content-type: application/mathematica **************
                     CreatedBy='Mathematica 5.0'

                    Mathematica-Compatible Notebook

This notebook can be used with any Mathematica-compatible
application, such as Mathematica, MathReader or Publicon. The data
for the notebook starts with the line containing stars above.

To get the notebook into a Mathematica-compatible application, do
one of the following:

* Save the data starting with the line of stars above into a file
  with a name ending in .nb, then open the file inside the
  application;

* Copy the data starting with the line of stars above to the
  clipboard, then use the Paste menu command inside the application.

Data for notebooks contains only printable 7-bit ASCII and can be
sent directly in email or through ftp in text mode.  Newlines can be
CR, LF or CRLF (Unix, Macintosh or MS-DOS style).

NOTE: If you modify the data for this notebook not in a Mathematica-
compatible application, you must delete the line below containing
the word CacheID, otherwise Mathematica-compatible applications may
try to use invalid cache data.

For more information on notebooks and Mathematica-compatible 
applications, contact Wolfram Research:
  web: http://www.wolfram.com
  email: info@wolfram.com
  phone: +1-217-398-0700 (U.S.)

Notebook reader applications are available free of charge from 
Wolfram Research.
*******************************************************************)

(*CacheID: 232*)


(*NotebookFileLineBreakTest
NotebookFileLineBreakTest*)
(*NotebookOptionsPosition[     19152,        453]*)
(*NotebookOutlinePosition[     19831,        476]*)
(*  CellTagsIndexPosition[     19787,        472]*)
(*WindowFrame->Normal*)



Notebook[{
Cell[BoxData[
    \(\(\(\ \ \ \ \ \ \ \ \ \)\( (*\ \ \ \ \ Here\ you\ can\ Import\ your\ \
photo\ in\ order\ to\ manipulate\ it\  . \ \[IndentingNewLine]\ \ \ \ \ \ \ \ \
The\ appropriate\ format\ is\ GIF\ \ or\ TIFF\ or\ BMP\ , \ 
      not\ Jpeg\  . \[IndentingNewLine]\ \ \ \ \ \ \ \ Mathematica\ can\ not\ \
handle\ well\ Jpeg\ /\ I\ don' 
        t\ know\ \(\(why\)\(\ \)\(/\)\)\ \[IndentingNewLine]\ \ \ \ \ \ \ \ \ \
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ *) \)\(\[IndentingNewLine]\
\)\(\[IndentingNewLine]\)\(\[IndentingNewLine]\)\(Photo = 
        Import[Experimental`FileBrowse[False]];\)\)\)], "Input"],

Cell[BoxData[
    \(\(Show[Photo];\)\)], "Input"],

Cell[BoxData[
    \(\(\(\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \)\( (*\ \ \
\ Here\ we\ convert\ our\ colored - Photo\ into\ gray - 
        Photo\ \ \ \[IndentingNewLine]\ \ \ \ \ \ \ \ \ \ \ \ \ \ It\ is\ \
mathematicaly\ more\ easier\ to\ handle\ gray - 
        photos\ \ \
*) \)\(\[IndentingNewLine]\)\(\[IndentingNewLine]\)\(GrayScalePhoto = 
        Map[\(Part[#, 1]*0.3 + Part[#, 2]*0.59 + Part[#, 3]*0.11\)\/255 &, 
          N[Part[Photo, 1, 1]], {2}];\)\)\)], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
    \(Dimensions[GrayScalePhoto]\)], "Input"],

Cell[BoxData[
    \({401, 528}\)], "Output"]
}, Open  ]],

Cell[BoxData[{
    \(\(xmax = 
        Part[Dimensions[GrayScalePhoto], 2];\)\), "\[IndentingNewLine]", 
    \(\(ymax = Part[Dimensions[GrayScalePhoto], 1];\)\)}], "Input"],

Cell[BoxData[
    \(\(\(\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \
\)\( (*\ \ \ You\ can\ see\ your\ photo\ right\ below\ \ \ \ \ *) \)\(\
\[IndentingNewLine]\)\(\[IndentingNewLine]\)\(\[IndentingNewLine]\)\(\
GrayPhoto = 
        Show[Graphics[Raster[GrayScalePhoto]], AspectRatio \[Rule] Automatic, 
          ImageSize \[Rule] {xmax, ymax}];\)\)\)], "Input"],

Cell[BoxData[
    \(\(\(\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \
\ \ \ \ \ \ \ \ \ \ \ \)\( (*\ \ The\ Negative\ of\ your\ gray - 
        photo\ *) \)\(\[IndentingNewLine]\)\(\[IndentingNewLine]\)\(\
GrayNegativePhoto = 
        Show[Graphics[Raster[1 - GrayScalePhoto]], 
          AspectRatio \[Rule] Automatic, 
          ImageSize \[Rule] {xmax, ymax}];\)\)\)], "Input"],

Cell[BoxData[
    \(\(\(\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \)\( (*\ \ We\ construct\ here\ \
The\ ContrastFunction\  . \[IndentingNewLine]\ \ \ \ \ \ \ It\ is\ a\ way\ to\
\ change\ the\ contrast\ of\ gray - 
          photo\  . \ \[IndentingNewLine]\ \ \ \ \ \ \ For\ a = \(1\  && \ 
            b = 0\ you\ take\ back\ the\ original\ photo . \
\[IndentingNewLine]\ \ \ \ \ \ {a}\ can\ lie\ at\ \ the\ interval\ 0 \
\[LessEqual] a \[LessEqual] 
            Infinity\ \ /\ \[IndentingNewLine]\ \ \ \ \ as\ you\ increase\ \
{a}\ you\ convert\ the\ gray - photo\ to\ white\ \[IndentingNewLine]
        \ \ \ \ \ \ but\ you\ can\ balance\ the\ result\ increasing\ the\ \
\(\({b}\)\(\ \)\(.\)\)\)\ \[IndentingNewLine]\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \
\ \ \ \ \ \ \ \ \ \ \ \ *) \)\(\[IndentingNewLine]\)\(\ \ \ \ \ \ \ \ \ \ \ \ \
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \)\(\
\[IndentingNewLine]\)\(\[IndentingNewLine]\)\(\(Clear[a, b, 
        ContrastFunction];\)\[IndentingNewLine]\[IndentingNewLine]
    \(ContrastFunction[a_, b_] := 
        Show[Graphics[Raster[GrayScalePhoto*a - b]], 
          AspectRatio \[Rule] Automatic, 
          ImageSize \[Rule] {xmax, ymax}];\)\)\)\)], "Input"],

Cell[BoxData[
    \(\(ContrastFunction[2, 0.5];\)\)], "Input"],

Cell[BoxData[
    \(\(\(\ \)\( (*\ \ \ \ \ \ \ \ \ \ \ \ \ \ Here\ we\ construct\ The\ \
PosterizeFunction\  . \[IndentingNewLine]\ \ \ \ \ \ \ \ \ As\ you\ increase\ \
{n}\ the\ gray - 
        photo\ more\ looks\ like\ the\ \(\(original\)\(\ \)\(.\)\)\ \ \ \ \ \ \
\ *) \)\(\[IndentingNewLine]\)\(\[IndentingNewLine]\)\(\(Clear[
        PosterizeFunction];\)\[IndentingNewLine]
    \(Clear[n];\)\[IndentingNewLine]\[IndentingNewLine]
    \(PosterizeFunction[n_] := 
        Show[Graphics[Raster[Round[n*GrayScalePhoto]/n]], 
          AspectRatio \[Rule] Automatic, 
          ImageSize \[Rule] {xmax, ymax}];\)\)\)\)], "Input"],

Cell[BoxData[
    \(\(PosterizeFunction[10];\)\)], "Input"],

Cell[BoxData[
    \(\(\(\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \)\( (*\ 
      Here\ we\ construct\ the\ BlurryFunction\  . \[IndentingNewLine]\ \ \ \ \
\ \ As\ you\ increase\ {n}\ \ the\ more\ gray - 
        photo\ gets\ blurry\  . \[IndentingNewLine]\ \ \ \ \ \ For\ {n = 
              1}\ \ you\ return\ back\ the\ \(\(original\)\(\ \)\(.\)\)\ \
*) \)\(\[IndentingNewLine]\)\(\[IndentingNewLine]\)\(\[IndentingNewLine]\)\(\(\
Clear[BlurryFunction];\)\[IndentingNewLine]
    \(Clear[n];\)\[IndentingNewLine]\[IndentingNewLine]
    \(BlurryFunction[n_] := 
        Show[Graphics[
            Raster[ListConvolve[Evaluate[Table[1\/n\^2, {n}, {n}]], 
                GrayScalePhoto]], AspectRatio \[Rule] Automatic, 
            ImageSize \[Rule] {xmax, ymax}]];\)\)\)\)], "Input"],

Cell[BoxData[
    \(\(BlurryFunction[10];\)\)], "Input"],

Cell[BoxData[
    RowBox[{
    "                                                                     ", \
\( (*\ \ No\ \(comments\ \ !!\)\ *) \), "\[IndentingNewLine]", 
      "\[IndentingNewLine]", 
      RowBox[{
        RowBox[{"EdgeDetection", "=", 
          RowBox[{"Show", "[", 
            RowBox[{"Graphics", "[", 
              RowBox[{
                RowBox[{"Raster", "[", 
                  RowBox[{"ListConvolve", "[", 
                    RowBox[{
                      RowBox[{"(", GridBox[{
                            {"1", "1", "1"},
                            {"1", \(-8\), "1"},
                            {"1", "1", "1"}
                            }], ")"}], ",", "GrayScalePhoto"}], "]"}], "]"}], 
                ",", \(AspectRatio \[Rule] Automatic\), 
                ",", \(ImageSize \[Rule] {xmax, ymax}\)}], "]"}], "]"}]}], 
        ";"}]}]], "Input"],

Cell[BoxData[
    RowBox[{
    "                                                                    ", \
\( (*\ No\ \(comments\ !\)\ *) \), "\[IndentingNewLine]", 
      "\[IndentingNewLine]", 
      RowBox[{
        RowBox[{"MotionBlur", "=", 
          RowBox[{"Show", "[", 
            RowBox[{"Graphics", "[", 
              RowBox[{
                RowBox[{"Raster", "[", 
                  RowBox[{"ListConvolve", "[", 
                    RowBox[{
                      RowBox[{"(", GridBox[{
                            {"0", "0", "0", "0", "0", "0", "0.8"},
                            {"0", "0", "0", "0", "0", "0", "0"},
                            {"0", "0", "0", "0", "0", "0", "0"},
                            {"0", "0", "0", "0", "0", "0", "0"},
                            {"0", "0", "0", "0", "0", "0", "0"},
                            {"0", "0", "0", "0", "0", "0", "0"},
                            {"0.5", "0", "0", "0", "0", "0", "0"}
                            }], ")"}], ",", "GrayScalePhoto"}], "]"}], "]"}], 
                ",", \(AspectRatio \[Rule] Automatic\), 
                ",", \(ImageSize \[Rule] {xmax, ymax}\)}], "]"}], "]"}]}], 
        ";"}]}]], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
    \(PhotoFunction = ListInterpolation[Transpose[GrayScalePhoto]]\)], "Input"],

Cell[BoxData[
    TagBox[\(InterpolatingFunction[{{1.`, 528.`}, {1.`, 401.`}}, "<>"]\),
      False,
      Editable->False]], "Output"]
}, Open  ]],

Cell[BoxData[{
    \(\(xmax = 
        Part[Dimensions[GrayScalePhoto], 2];\)\), "\[IndentingNewLine]", 
    \(\(ymax = 
        Part[Dimensions[GrayScalePhoto], 1];\)\), "\[IndentingNewLine]", 
    \(\(\(Clear[Photo3D];\)\(\[IndentingNewLine]\)
    \)\), "\[IndentingNewLine]", 
    \(\(Photo3D[points_, {ViewX_, ViewY_, ViewZ_}] := 
        Plot3D[PhotoFunction[x, y], {x, 1, xmax - 1}, {y, 1, ymax - 1}, 
          PlotPoints \[Rule] {points, points}, 
          ViewPoint \[Rule] {ViewX, ViewY, ViewZ}, Lighting \[Rule] False, 
          Mesh \[Rule] False, Axes \[Rule] False, 
          Boxed \[Rule] True];\)\), "\[IndentingNewLine]", 
    \(\)}], "Input"],

Cell[BoxData[
    \(\(Photo3D[300, {0.0, \(-0.1\), 2}];\)\)], "Input"],

Cell[BoxData[{
    \(\(\(Clear[DensityFunction, x1, x2, y1, y2];\)\(\[IndentingNewLine]\)
    \)\), "\[IndentingNewLine]", 
    \(\(DensityFunction[{x2_}, {y2_}, points_] := 
        DensityPlot[PhotoFunction[x, y], {x, 1, x2}, {y, 1, y2}, 
          PlotPoints \[Rule] points, Mesh \[Rule] False, 
          AspectRatio \[Rule] Automatic, Frame \[Rule] None];\)\)}], "Input"],

Cell[BoxData[
    \(\(DensityFunction[{xmax}, {ymax}, 50];\)\)], "Input"],

Cell[BoxData[
    \(\(DensityFunction[{xmax}, {ymax}, 300];\)\)], "Input"],

Cell[BoxData[
    \(\(\(\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \)\( (*\ 
      Here\ you\ can\ cut\ and\ Zoom\ *) \)\(\[IndentingNewLine]\)\(\
\[IndentingNewLine]\)\(\(Clear[ZoomFunction, x1, x2, y1, y2, 
        points];\)\[IndentingNewLine]\[IndentingNewLine]
    \(ZoomFunction[{x1_, x2_}, {y1_, y2_}, points_] := 
        DensityPlot[PhotoFunction[x, y], {x, x1, x2}, {y, y1, y2}, 
          PlotPoints \[Rule] points, Mesh \[Rule] False, 
          AspectRatio \[Rule] Automatic, 
          Frame \[Rule] None];\)\)\)\)], "Input"],

Cell[BoxData[
    \(\(ZoomFunction[{65, 185}, {230, 360}, 300];\)\)], "Input"],

Cell[BoxData[{
    \(\(\(Clear[DerivativePhotoFunctionX, 
        DerivativePhotoFunctionY];\)\(\[IndentingNewLine]\)
    \)\), "\[IndentingNewLine]", 
    \(\(DerivativePhotoFunctionX = \(Derivative[1, 0]\)[
          PhotoFunction];\)\), "\[IndentingNewLine]", 
    \(\(DerivativePhotoFunctionY = \(Derivative[0, 1]\)[
          PhotoFunction];\)\[IndentingNewLine]\[IndentingNewLine]\
\[IndentingNewLine]\), "\[IndentingNewLine]", 
    \(\(\(Clear[DerivativePhotoFunctionXY];\)\(\[IndentingNewLine]\)
    \)\), "\[IndentingNewLine]", 
    \(\(DerivativePhotoFunctionXY[x_, y_] := 
        DerivativePhotoFunctionX[x, y] + 
          DerivativePhotoFunctionY[x, 
            y];\)\[IndentingNewLine]\[IndentingNewLine]\[IndentingNewLine]\), \
"\[IndentingNewLine]", 
    \(\(\(Clear[GradPhotoFunction];\)\(\[IndentingNewLine]\)
    \)\), "\[IndentingNewLine]", 
    \(\(GradPhotoFunction[x_, 
          y_] := \@\(\((DerivativePhotoFunctionX[x, y])\)\^2 + \
\((DerivativePhotoFunctionY[x, y])\)\^2\);\)\), "\[IndentingNewLine]", 
    \(\)}], "Input"],

Cell[BoxData[
    \(\(\(\ \ \ \ \ \ \ \ \ \ \ \ \ \ \)\( (*\ \ \ \ The\ algorithm\ how\ we\ \
can\ detect\ the\ edges\ of\ the\ gray - 
        photo\ *) \)\(\[IndentingNewLine]\)\(\[IndentingNewLine]\)\(\(Clear[
        EdgeDetectionFunction];\)\[IndentingNewLine]\[IndentingNewLine]
    \(EdgeDetectionFunction[{pointsX_, pointsY_}] := 
        DensityPlot[GradPhotoFunction[x, y], {x, 1, xmax}, {y, 1, ymax}, 
          PlotPoints \[Rule] {pointsX, pointsY}, Mesh \[Rule] False, 
          AspectRatio \[Rule] \ Automatic, 
          Frame \[Rule] None];\)\)\)\)], "Input"],

Cell[BoxData[
    \(\(EdgeDetectionFunction[{300, 300}];\)\)], "Input"],

Cell[BoxData[
    \(\(\(\ \ \ \ \ \ \ \ \ \)\( (*\ \ \ \ \ The\ "\<EmbossFunction\>"\ means\
\ \[IndentingNewLine]\ \ \ \ \ \ \ \ "\< pernv to Anaglifo \>"\ \ \ *) \)\(\
\[IndentingNewLine]\)\(\[IndentingNewLine]\)\(\[IndentingNewLine]\)\(\(Clear[
        EmbossFunction];\)\[IndentingNewLine]\[IndentingNewLine]
    \(EmbossFunction[{pointsX_, pointsY_}] := 
        DensityPlot[
          DerivativePhotoFunctionXY[x, y], {x, 1, xmax}, {y, 1, ymax}, 
          PlotPoints \[Rule] {pointsX, pointsY}, Mesh \[Rule] False, 
          AspectRatio \[Rule] \ Automatic, 
          Frame \[Rule] None];\)\[IndentingNewLine]
    \)\)\)], "Input"],

Cell[BoxData[
    \(\(EmbossFunction[{200, 200}];\)\)], "Input"],

Cell[BoxData[{
    \(\(Clear[
        ColorFunction3D];\)\[IndentingNewLine]\n\), "\[IndentingNewLine]", 
    \(\(ColorFunction3D = 
        ListInterpolation[
          Transpose[Part[Photo, 1, 1]]/
            256. , {{\(-1\), 1}, {\(-\(ymax\/xmax\)\), ymax\/xmax}, {1, 3}}, 
          InterpolationOrder \[Rule] {3, 3, 2}];\)\), "\[IndentingNewLine]", 
    \(\)}], "Input"],

Cell[BoxData[
    \(\(\(\[IndentingNewLine]\)\(\ \ \ \ \ \ \ \ \ \)\( (*\ 
      Here\ we\ manipulate\ our\ Colored - \(\(Photo\)\(\ \)\(.\)\)*) \)\(\
\[IndentingNewLine]\)\(\[IndentingNewLine]\)\(\[IndentingNewLine]\)\(\(Clear[
        xmax, ymax];\)\[IndentingNewLine]
    \(Clear[
        ColorPhotoFunction];\)\[IndentingNewLine]\[IndentingNewLine]\
\[IndentingNewLine]
    \(ColorPhotoFunction[x_, y_] := 
        Table[ColorFunction3D[x, y, c], {c, 1, 
            3}];\)\[IndentingNewLine]\[IndentingNewLine]\[IndentingNewLine]
    \(Options[PlotColorImage] = {PlotPoints \[Rule] 20};\)\[IndentingNewLine]
    \(Attributes[PlotColorImage] = {HoldFirst};\)\[IndentingNewLine]
    PlotColorImage[func_, {x_, xmin_, xmax_}, {y_, ymin_, ymax_}, 
        opts_] := \[IndentingNewLine]Module[{plotPoints}, \
\[IndentingNewLine]plotPoints = \(PlotPoints /. {opts}\) /. 
            Options[PlotColorImage]; \[IndentingNewLine]If[
          Head[plotPoints] =!= List, 
          plotPoints = {plotPoints, 
              plotPoints}]; \[IndentingNewLine]\[IndentingNewLine]Show[
          Graphics[
            Raster[Table[
                func, {y, ymin, 
                  ymax, \(ymax - ymin\)\/Part[plotPoints, 2]}, {x, xmin, 
                  xmax, \(xmax - xmin\)\/Part[plotPoints, 1]}], {{xmin, 
                  ymin}, {xmax, ymax}}, ColorFunction \[Rule] RGBColor], 
            AspectRatio \[Rule] Automatic, 
            PlotRange \[Rule] {{xmin, xmax}, {ymin, 
                  ymax}}]];]\[IndentingNewLine]
    \)\)\)], "Input"],

Cell[BoxData[{
    \(\(xmax = Part[Dimensions[GrayScalePhoto], 2];\)\), "\n", 
    \(\(\(ymax = 
        Part[Dimensions[GrayScalePhoto], 1];\)\(\[IndentingNewLine]\)
    \)\), "\[IndentingNewLine]", 
    \(\(xmaxNew = 1;\)\), "\[IndentingNewLine]", 
    \(\(ymaxNew = ymax\/xmax;\)\)}], "Input"],

Cell[BoxData[
    \(Off[InterpolatingFunction::dmval]\)], "Input"],

Cell[BoxData[
    \(PlotColorImage[
      ColorPhotoFunction[x, y], {x, \(-1\), 1}, {y, \(-ymaxNew\), ymaxNew}, 
      PlotPoints \[Rule] 150]\)], "Input"],

Cell[BoxData[
    \(PlotColorImage[
      ColorPhotoFunction[x, y + Sin[20\ x]/20], {x, \(-1\), 
        1}, {y, \(-ymaxNew\), ymaxNew}, PlotPoints \[Rule] 150]\)], "Input"],

Cell[BoxData[
    \(\(\(\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \)\( (*\ \ Fill\ with\ "\<Red\>"\ \
the\ "\<white\>"\ space\ \ \
*) \)\(\[IndentingNewLine]\)\(\[IndentingNewLine]\)\(\(highLightBlaze[{r_, 
            g_, b_}] := 
        If[r > 0.8 && g > 0.8 && b > 0.8, {1, 0, 0}, {r, g, 
            b}];\)\[IndentingNewLine]\[IndentingNewLine]\[IndentingNewLine]
    PlotColorImage[
      highLightBlaze[ColorPhotoFunction[x, y]], {x, \(-1\), 
        1}, {y, \(-ymaxNew\), ymaxNew}, 
      PlotPoints \[Rule] 200]\)\)\)], "Input"],

Cell[BoxData[
    \(\(\(\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \)\( (*\ \ Fill\ with\ "\<Yellow\>"\
\ the\ "\<white\>"\ space\ \ \
*) \)\(\[IndentingNewLine]\)\(\[IndentingNewLine]\)\(\(highLightBlaze[{r_, 
            g_, b_}] := 
        If[r > 0.8 && g > 0.8 && b > 0.8, {1, 1, 0}, {r, g, 
            b}];\)\[IndentingNewLine]\[IndentingNewLine]\[IndentingNewLine]
    PlotColorImage[
      highLightBlaze[ColorPhotoFunction[x, y]], {x, \(-1\), 
        1}, {y, \(-ymaxNew\), ymaxNew}, 
      PlotPoints \[Rule] 200]\)\)\)], "Input"],

Cell[BoxData[{
    RowBox[{\(ColorPhotoFunction2[{x_, y_}] := ColorPhotoFunction[x, y];\), 
      "\[IndentingNewLine]", "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
    RowBox[{"PlotColorImage", "[", 
      RowBox[{
        RowBox[{"ColorPhotoFunction2", "[", 
          RowBox[{\(u = 1.0\), ";", 
            RowBox[{
              RowBox[{"(", GridBox[{
                    {\(Cos[u]\), \(Sin[u]\)},
                    {\(-Sin[u]\), \(Cos[u]\)}
                    }], ")"}], ".", \({x, y}\)}]}], "]"}], 
        ",", \({x, \(-1\), 1}\), ",", \({y, \(-ymaxNew\), ymaxNew}\), 
        ",", \(PlotPoints \[Rule] 200\)}], "]"}]}], "Input"],

Cell[BoxData[
    RowBox[{"\[IndentingNewLine]", 
      RowBox[{"PlotColorImage", "[", 
        RowBox[{
          RowBox[{"ColorPhotoFunction2", "[", 
            RowBox[{\(u = \@\(x\^2 + y\^2\)\), ";", 
              RowBox[{
                RowBox[{"(", GridBox[{
                      {\(Cos[u]\), \(Sin[u]\)},
                      {\(-Sin[u]\), \(Cos[u]\)}
                      }], ")"}], ".", \({x, y}\)}]}], "]"}], 
          ",", \({x, \(-1\), 1}\), ",", \({y, \(-ymaxNew\), ymaxNew}\), 
          ",", \(PlotPoints \[Rule] 300\)}], "]"}]}]], "Input"],

Cell[BoxData[
    RowBox[{"\[IndentingNewLine]", 
      RowBox[{"PlotColorImage", "[", 
        RowBox[{
          RowBox[{"ColorPhotoFunction2", "[", 
            RowBox[{\(u = x\^4 + y\^4\), ";", 
              RowBox[{
                RowBox[{"(", GridBox[{
                      {\(Cos[u]\), \(Sin[u]\)},
                      {\(-Sin[u]\), \(Cos[u]\)}
                      }], ")"}], ".", \({x, y}\)}]}], "]"}], 
          ",", \({x, \(-1\), 1}\), ",", \({y, \(-ymaxNew\), ymaxNew}\), 
          ",", \(PlotPoints \[Rule] 300\)}], "]"}]}]], "Input"]
},
FrontEndVersion->"5.0 for Microsoft Windows",
ScreenRectangle->{{0, 800}, {0, 521}},
WindowSize->{796, 487},
WindowMargins->{{Automatic, 0}, {Automatic, 0}},
StyleDefinitions -> "Classroom2.nb"
]

(*******************************************************************
Cached data follows.  If you edit this Notebook file directly, not
using Mathematica, you must remove the line containing CacheID at
the top of  the file.  The cache data will then be recreated when
you save this file from within Mathematica.
*******************************************************************)

(*CellTagsOutline
CellTagsIndex->{}
*)

(*CellTagsIndex
CellTagsIndex->{}
*)

(*NotebookFileOutline
Notebook[{
Cell[1754, 51, 707, 10, 170, "Input"],
Cell[2464, 63, 49, 1, 50, "Input"],
Cell[2516, 66, 496, 8, 145, "Input"],

Cell[CellGroupData[{
Cell[3037, 78, 59, 1, 50, "Input"],
Cell[3099, 81, 44, 1, 49, "Output"]
}, Open  ]],
Cell[3158, 85, 172, 3, 70, "Input"],
Cell[3333, 90, 386, 6, 130, "Input"],
Cell[3722, 98, 404, 7, 110, "Input"],
Cell[4129, 107, 1290, 20, 290, "Input"],
Cell[5422, 129, 62, 1, 50, "Input"],
Cell[5487, 132, 630, 11, 190, "Input"],
Cell[6120, 145, 59, 1, 50, "Input"],
Cell[6182, 148, 775, 13, 265, "Input"],
Cell[6960, 163, 56, 1, 50, "Input"],
Cell[7019, 166, 884, 20, 164, "Input"],
Cell[7906, 188, 1196, 24, 244, "Input"],

Cell[CellGroupData[{
Cell[9127, 216, 93, 1, 50, "Input"],
Cell[9223, 219, 135, 3, 49, "Output"]
}, Open  ]],
Cell[9373, 225, 663, 13, 190, "Input"],
Cell[10039, 240, 70, 1, 50, "Input"],
Cell[10112, 243, 376, 6, 130, "Input"],
Cell[10491, 251, 73, 1, 50, "Input"],
Cell[10567, 254, 74, 1, 50, "Input"],
Cell[10644, 257, 546, 9, 170, "Input"],
Cell[11193, 268, 78, 1, 50, "Input"],
Cell[11274, 271, 1052, 21, 413, "Input"],
Cell[12329, 294, 576, 9, 170, "Input"],
Cell[12908, 305, 71, 1, 50, "Input"],
Cell[12982, 308, 640, 11, 230, "Input"],
Cell[13625, 321, 64, 1, 50, "Input"],
Cell[13692, 324, 376, 8, 163, "Input"],
Cell[14071, 334, 1543, 29, 525, "Input"],
Cell[15617, 365, 296, 6, 141, "Input"],
Cell[15916, 373, 66, 1, 50, "Input"],
Cell[15985, 376, 155, 3, 50, "Input"],
Cell[16143, 381, 173, 3, 70, "Input"],
Cell[16319, 386, 527, 10, 170, "Input"],
Cell[16849, 398, 530, 10, 170, "Input"],
Cell[17382, 410, 644, 13, 144, "Input"],
Cell[18029, 425, 561, 12, 104, "Input"],
Cell[18593, 439, 555, 12, 104, "Input"]
}
]
*)



(*******************************************************************
End of Mathematica Notebook file.
*******************************************************************)

