(* :Title: ExportP. *)

(* :Authors: Paul Galiatsatos. *)

(* :Context: PaulsTools`ExportP` *)

(* :Package Version: 1.0 *)

(* :Copyright: No Copyright *)

(* :History: 
	Version 1.0 by Paul Galiatsatos, December 2004.
*)

(* :Keywords: *)

(* :Source: None. *)

(* :Warning: *)

(* :Mathematica Version: 5.1 *)

(* :Limitation: *)


BeginPackage["PaulsTools`ExportP`"]


ExportP::usage = "ExportP is the head of a new function
\nwhich generalizes the build-in function Export.
\nThe goal in construction the ExportP function was to make possible
\nto export in automatic mode numbered frames.
\nOne important aspect is that you can set the same options 
\nas for the original build-in Export.
\nYou can use ExportP function exactly in the same way as Export.
\nExplanations:
\n1....
\nExportP[{{firstLetter_},{i_},{format_},{path_String},{frameFunction_}},opts_\
__?\
OptionQ]
\n2....
\nExportP[file_String,expr_,format_String,opts___?OptionQ],
\n3....
\nExportP[file_String,expr_,opts___?OptionQ].
\nAt the form 2 && 3 the ExportP function is identical to 
\nbuild-in function Export."

Unprotect[PathExport]

ClearAll[PathExport]


PathExport::usage = "PathExport is a String.
\nPathExport has the special value: 
\nF:\\Documents and Settings\\paulsepolia\\Desktop\\New Folder (2).
"


Begin["`Private`"]


Unprotect["PaulsTools`ExportP`*"]


ClearAll[helpExportP]


PathExport = "F:\\Documents and Settings\\paulsepolia\\Desktop\\New Folder \
(2)"



helpExportP[{{\[Alpha]_, \[Theta]_, \[Tau]_}, {\[CurlyEpsilon]_}}] := 
  StringReplace[
    "\[CurlyEpsilon]\\\[Alpha]\[Theta].\[Tau]", {"\[Theta]" -> 
        ToString[\[Theta]], "\[CurlyEpsilon]" -> ToString[\[CurlyEpsilon]], 
      "\[Tau]" -> ToString[\[Tau]], "\[Alpha]" -> ToString[\[Alpha]]}]


ExportP[{{firstLetter_},{i_}, {format_}, {path_},{frameFunction_}}, 
    opts___?OptionQ] := 
  Export[helpExportP[{{firstLetter, i, format}, {path}}], frameFunction, 
    StringReplace["format", {"format" -> ToString[format]}], opts]
    
    
ExportP[file_String,expr_,format_String,opts___?OptionQ]:=Export[file,expr,\
format,opts]

ExportP[file_String,expr_,opts___?OptionQ]:=Export[file,expr,opts]    

End[]


Protect["PaulsTools`ExportP`*"]

EndPackage[]









