(************** Content-type: application/mathematica **************
                     CreatedBy='Mathematica 4.2'

                    Mathematica-Compatible Notebook

This notebook can be used with any Mathematica-compatible
application, such as Mathematica, MathReader or Publicon. The data
for the notebook starts with the line containing stars above.

To get the notebook into a Mathematica-compatible application, do
one of the following:

* Save the data starting with the line of stars above into a file
  with a name ending in .nb, then open the file inside the
  application;

* Copy the data starting with the line of stars above to the
  clipboard, then use the Paste menu command inside the application.

Data for notebooks contains only printable 7-bit ASCII and can be
sent directly in email or through ftp in text mode.  Newlines can be
CR, LF or CRLF (Unix, Macintosh or MS-DOS style).

NOTE: If you modify the data for this notebook not in a Mathematica-
compatible application, you must delete the line below containing
the word CacheID, otherwise Mathematica-compatible applications may
try to use invalid cache data.

For more information on notebooks and Mathematica-compatible 
applications, contact Wolfram Research:
  web: http://www.wolfram.com
  email: info@wolfram.com
  phone: +1-217-398-0700 (U.S.)

Notebook reader applications are available free of charge from 
Wolfram Research.
*******************************************************************)

(*CacheID: 232*)


(*NotebookFileLineBreakTest
NotebookFileLineBreakTest*)
(*NotebookOptionsPosition[     29235,        879]*)
(*NotebookOutlinePosition[     30004,        905]*)
(*  CellTagsIndexPosition[     29960,        901]*)
(*WindowFrame->Normal*)



Notebook[{
Cell[BoxData[
    \(p[n_] := 
      Graphics[{{PointSize[ .08], RGBColor[0, 0, 1], 
            Point[{Sin[\((2*Pi*800)\)*n], 0}]}, 
          Line[{{0, \(-0.2\)}, {0, 0.2}}], Line[{{1, 0.2}, {1, \(-0.2\)}}], 
          Line[{{\(-1\), 0.2}, {\(-1\), \(-0.2\)}}]}, Axes -> {True, False}, 
        AxesLabel \[Rule] {StyleForm["\<x(metre)\>", FontSize \[Rule] 15, 
              FontWeight \[Rule] "\<Bold\>"], StyleForm[q]}, 
        AspectRatio \[Rule] 1/10, 
        AxesStyle \[Rule] {{RGBColor[1, 0.5, 0], Thickness[0.01]}, {}}, 
        PlotRange \[Rule] {{\(-1.08\), 1.075}, {\(-0.1\), 0.1}}, 
        ImageSize \[Rule] {900, 100}, 
        Prolog \[Rule] {Thickness[0.01], RGBColor[1, 0, 0]}]\)], "Input"],

Cell[BoxData[
    \(\(g1[n_] := 
        Graphics[{Thickness[ .018 - 0.017*Abs[Sin[2*Pi*800*n]]], 
            RGBColor[0, 1, 0], 
            Line[{{1/40*Cos[2*Pi*400*n], 
                  0}, {Sin[\((2*Pi*800)\)*n] - 1/40*Cos[2*Pi*n*400], 0}}]}, 
          PlotRange \[Rule] {{\(-1.08\), 1.075}, {\(-0.2\), 0.2}}, 
          ImageSize \[Rule] {900, 100}];\)\)], "Input"],

Cell[BoxData[
    \(\(g2 = 
        Graphics[{PointSize[ .025], RGBColor[0, 0, 0], 
            Point[{0, 0}]}];\)\)], "Input"],

Cell[BoxData[
    \(\(g3[n_] := 
        Graphics[{PointSize[ .022], RGBColor[1, 1, 0], 
            Point[{Sin[\((2*Pi*800)\)*n], 0}]}];\)\)], "Input"],

Cell[BoxData[
    \(\(g4 = 
        Graphics[{Line[{{1, 0.2}, {1, \(-0.2\)}}], 
            Line[{{\(-1\), 0.2}, {\(-1\), \(-0.2\)}}]}, 
          Prolog \[Rule] {Thickness[0.01], RGBColor[1, 0, 0]}];\)\)], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
    \(Show[g2]\)], "Input"],

Cell[GraphicsData["PostScript", "\<\
%!
%%Creator: Mathematica
%%AspectRatio: .61803 
MathPictureStart
/Mabs {
Mgmatrix idtransform
Mtmatrix dtransform
} bind def
/Mabsadd { Mabs
3 -1 roll add
3 1 roll add
exch } bind def
%% Graphics
%%IncludeResource: font Courier
%%IncludeFont: Courier
/Courier findfont 10  scalefont  setfont
% Scaling calculations
0.5 0.47619 0.309017 0.294302 [
[ 0 0 0 0 ]
[ 1 .61803 0 0 ]
] MathScale
% Start of Graphics
1 setlinecap
1 setlinejoin
newpath
0 0 m
1 0 L
1 .61803 L
0 .61803 L
closepath
clip
newpath
0 0 0 r
.025 w
.5 .30902 Mdot
% End of Graphics
MathPictureEnd
\
\>"], "Graphics",
  ImageSize->{288, 177.938},
  ImageMargins->{{0, 0}, {0, 0}},
  ImageRegion->{{0, 1}, {0, 1}},
  ImageCache->GraphicsData["Bitmap", "\<\
CF5dJ6E]HGAYHf4PAg9QL6QYHg<PAVmbKF5d0`40004P0000/A000`40O003h00Oogoo8Goo003oOolQ
Ool00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00
ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQ
Ool00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00
ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQ
Ool00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00
ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQ
Ool00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00
ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQ
Ool00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00
ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQ
Ool00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00
ogoo8Goo002>Ool3002?Ool008eoo`D008ioo`00S7oo1`00SGoo002<Ool7002=Ool008aoo`L008eo
o`00SGoo1@00SWoo002>Ool3002?Ool00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo
8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol0
0?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo
8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol0
0?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo
8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol0
0?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo
8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol0
0?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo
8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol0
0?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00ogoo
8Goo003oOolQOol00?moob5oo`00ogoo8Goo003oOolQOol00?moob5oo`00\
\>"],
  ImageRangeCache->{{{0, 287}, {176.938, 0}} -> {-1.05261, -1.05001, \
0.00733523, 0.0118686}}],

Cell[BoxData[
    TagBox[\(\[SkeletonIndicator]  Graphics  \[SkeletonIndicator]\),
      False,
      Editable->False]], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(Show[p[1]]\)], "Input"],

Cell[GraphicsData["PostScript", "\<\
%!
%%Creator: Mathematica
%%AspectRatio: .1 
%%ImageSize: 900 100 
MathPictureStart
/Mabs {
Mgmatrix idtransform
Mtmatrix dtransform
} bind def
/Mabsadd { Mabs
3 -1 roll add
3 1 roll add
exch } bind def
%% Graphics
%%IncludeResource: font Courier
%%IncludeFont: Courier
/Courier findfont 10  scalefont  setfont
% Scaling calculations
0.50116 0.464037 0.05 0.5 [
[.03712 .0375 -6 -9 ]
[.03712 .0375 6 0 ]
[.26914 .0375 -12 -9 ]
[.26914 .0375 12 0 ]
[.50116 .0375 -3 -9 ]
[.50116 .0375 3 0 ]
[.73318 .0375 -9 -9 ]
[.73318 .0375 9 0 ]
[.9652 .0375 -3 -9 ]
[.9652 .0375 3 0 ]
[1.025 .05 0 -7.0625 ]
[1.025 .05 76.3125 7.0625 ]
[ -0.005 0 0 0 ]
[ 1.005 .1 0 0 ]
] MathScale
% Start of Graphics
1 setlinecap
1 setlinejoin
newpath
0 g
.25 Mabswid
[ ] 0 setdash
.03712 .05 m
.03712 .05625 L
s
[(-1)] .03712 .0375 0 1 Mshowa
.26914 .05 m
.26914 .05625 L
s
[(-0.5)] .26914 .0375 0 1 Mshowa
.50116 .05 m
.50116 .05625 L
s
[(0)] .50116 .0375 0 1 Mshowa
.73318 .05 m
.73318 .05625 L
s
[(0.5)] .73318 .0375 0 1 Mshowa
.9652 .05 m
.9652 .05625 L
s
[(1)] .9652 .0375 0 1 Mshowa
.125 Mabswid
.08353 .05 m
.08353 .05375 L
s
.12993 .05 m
.12993 .05375 L
s
.17633 .05 m
.17633 .05375 L
s
.22274 .05 m
.22274 .05375 L
s
.31555 .05 m
.31555 .05375 L
s
.36195 .05 m
.36195 .05375 L
s
.40835 .05 m
.40835 .05375 L
s
.45476 .05 m
.45476 .05375 L
s
.54756 .05 m
.54756 .05375 L
s
.59397 .05 m
.59397 .05375 L
s
.64037 .05 m
.64037 .05375 L
s
.68677 .05 m
.68677 .05375 L
s
.77958 .05 m
.77958 .05375 L
s
.82599 .05 m
.82599 .05375 L
s
.87239 .05 m
.87239 .05375 L
s
.91879 .05 m
.91879 .05375 L
s
1 .5 0 r
.01 w
0 .05 m
1 .05 L
s
0 g
gsave
1.025 .05 -61 -11.0625 Mabsadd m
1 1 Mabs scale
currentpoint translate
0 22.125 translate 1 -1 scale
/g { setgray} bind def
/k { setcmykcolor} bind def
/p { gsave} bind def
/r { setrgbcolor} bind def
/w { setlinewidth} bind def
/C { curveto} bind def
/F { fill} bind def
/L { lineto} bind def
/rL { rlineto} bind def
/P { grestore} bind def
/s { stroke} bind def
/S { show} bind def
/N {currentpoint 3 -1 roll show moveto} bind def
/Msf { findfont exch scalefont [1 0 0 -1 0 0 ] makefont setfont} bind def
/m { moveto} bind def
/Mr { rmoveto} bind def
/Mx {currentpoint exch pop moveto} bind def
/My {currentpoint pop exch moveto} bind def
/X {0 rmoveto} bind def
/Y {0 exch rmoveto} bind def
63.000 14.375 moveto
%%IncludeResource: font Courier
%%IncludeFont: Courier
/Courier findfont 15.000 scalefont
[1 0 0 -1 0 0 ] makefont setfont
0.000 0.000 0.000 setrgbcolor
0.000 0.000 rmoveto
63.000 14.375 moveto
%%IncludeResource: font Courier
%%IncludeFont: Courier
/Courier findfont 15.000 scalefont
[1 0 0 -1 0 0 ] makefont setfont
0.000 0.000 0.000 setrgbcolor
(x) show
%%IncludeResource: font Mathematica2Mono-Bold
%%IncludeFont: Mathematica2Mono-Bold
/Mathematica2Mono-Bold findfont 15.000 scalefont
[1 0 0 -1 0 0 ] makefont setfont
0.000 0.000 0.000 setrgbcolor
72.000 14.375 moveto
(H) show
81.313 14.375 moveto
%%IncludeResource: font Courier
%%IncludeFont: Courier
/Courier findfont 15.000 scalefont
[1 0 0 -1 0 0 ] makefont setfont
0.000 0.000 0.000 setrgbcolor
(metre) show
%%IncludeResource: font Mathematica2Mono-Bold
%%IncludeFont: Mathematica2Mono-Bold
/Mathematica2Mono-Bold findfont 15.000 scalefont
[1 0 0 -1 0 0 ] makefont setfont
0.000 0.000 0.000 setrgbcolor
126.313 14.375 moveto
(L) show
135.313 14.375 moveto
%%IncludeResource: font Courier
%%IncludeFont: Courier
/Courier findfont 15.000 scalefont
[1 0 0 -1 0 0 ] makefont setfont
0.000 0.000 0.000 setrgbcolor
0.000 0.000 rmoveto
1.000 setlinewidth
grestore
0 0 m
1 0 L
1 .1 L
0 .1 L
closepath
clip
newpath
0 0 1 r
.08 w
.50116 .05 Mdot
1 0 0 r
.01 w
.50116 0 m
.50116 .1 L
s
.9652 .1 m
.9652 0 L
s
.03712 .1 m
.03712 0 L
s
% End of Graphics
MathPictureEnd
\
\>"], "Graphics",
  ImageSize->{900, 100},
  ImageCache->GraphicsData["Bitmap", "\<\
CF5dJ6E]HGAYHf4PAg9QL6QYHg<PAVmbKF5d0`4000>40000I1000`40O003h00OogooogooogooQgoo
003oOoooOoooOon7Ool00?mooomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7Ool00?mo
oomooomoohMoo`00ogooogooogooQgoo000NOol7O03oOom]Ool7O03oOom/Ool7O01jOol001ioo`Ml
0?moofeoo`Ml0?moofaoo`Ml07Yoo`007Woo1g`0ogooKGoo1g`0ogooK7oo1g`0NWoo000NOol7O03o
Oom]Ool7O03oOom/Ool7O01jOol001ioo`Ml0?moofeoo`Ml0?moofaoo`Ml07Yoo`007Woo1g`0ogoo
KGoo1g`0ogooK7oo1g`0NWoo000NOol7O03oOom]Ool7O03oOom/Ool7O01jOol001ioo`Ml0?moofeo
o`Ml0?moofaoo`Ml07Yoo`007Woo1g`0ogooKGoo1g`0ogooK7oo1g`0NWoo000NOol7O03oOom]Ool7
O03oOom/Ool7O01jOol001ioo`Ml0?moofeoo`Ml0?moofaoo`Ml07Yoo`007Woo1g`0ogooKGoo1g`0
ogooK7oo1g`0NWoo000NOol7O03oOom[Ool201l7O03oOom/Ool7O01jOol001ioo`Ml0?moofIoo`L0
7`Ml00D07omoofMoo`Ml07Yoo`007Woo1g`0ogooHgoo2P0O1g`0200OogooI7oo1g`0NWoo000NOol7
O03oOomQOol<01l7O00:01ooOomROol7O01jOol001ioo`Ml0?moof1oo`d07`Ml00/07omoof5oo`Ml
07Yoo`007Woo1g`0ogooGWoo3`0O1g`03@0OogooGgoo1g`0NWoo000NOol7O03oOomMOol@01l7O00>
01ooOomNOol7O01jOol001ioo`Ml0?mooeaooa407`Ml00l07omooeeoo`Ml07Yoo`007Woo1g`0ogoo
FWoo4`0O1g`04@0OogooFgoo1g`0NWoo000NOol7O03oOomIOolD01l7O00B01ooOomJOol7O01jOol0
01ioo`Ml0?mooeQooaD07`Ml01<07omooeUoo`Ml07Yoo`007Woo1g`0ogooF7oo5@0O1g`04`0Oogoo
FGoo1g`0NWoo000NOol7O03oOomGOolF01l7O00D01ooOomHOol7O01jOol001ioo`Ml00800:moo`80
00Aoo`8000Aoo`8009IooaL07`Ml01D07iUoo`8000Aoo`8000Aoo`800:moo`Ml07Yoo`007Woo1g`0
/7oo00@007ooOol000Qoo`04001oogoo002DOolH01l7O00F01nGOol01000Oomoo`0027oo00@007oo
Ool00:ioo`Ml07Yoo`007Woo1g`0/7oo00@007ooOol000]oo`03001oogoo099ooaP07`Ml01H07iMo
o`04001oogoo000;Ool00`00Oomoo`2/Ool7O01jOol001aoo`8000Ml0:Yoo`@0009oo`04001oogoo
0009Ool2002DOolI01l7O00G01nFOol01000Oomoo`002Goo0P00[goo1g`0NWoo000NOol7O02`Ool0
1000Oomoo`002Goo00<007ooOol0TWoo6P0O1g`0600OUGoo00@007ooOol000Uoo`03001oogoo0:io
o`Ml07Yoo`007Woo1g`0/Goo0P002Woo0`00TWoo6P0O1g`0600OUWoo0P002Woo0`00[Woo1g`0NWoo
000NOol7O03oOomBOolK01l7O00I01ooOomCOol7O01jOol001ioo`Ml0?mooe9ooa/07`Ml01T07omo
oe=oo`Ml07Yoo`007Woo1g`0ogooDWoo6`0O1g`06@0OogooDgoo1g`0NWoo000NOol7O03oOomAOolL
01l7O00J01ooOomBOol7O01jOol001ioo`Ml0?mooe5ooa`07`Ml01X07omooe9oo`Ml07Yoo`007Woo
1g`0ogooDGoo700O1g`06P0OogooDWoo1g`0NWoo000NOol7O03oOomAOolL01l7O00J01ooOomBOol7
O01jOol001ioo`Ml0?mooe1ooad07`Ml01X07omooe9oo`Ml03aoo`80035oo`8000Uoo`007Woo1g`0
ogooD7oo7@0O1g`06`0OogooDGoo1g`0>goo0P00<goo0P0027oo000NON07O03oON1@ON0M01l7O00K
01ooON1AON07O00LON0OOol2000cOol20008Ool001imh0Ml0?mmh51mh1d07`Ml01/07ommh55mh0Ml
01amh1=oo`<0009oo`<000=oo`8000Aoo`<00004Ool00000Ool20003Ool40006Ool40003Ool50005
Ool40006Ool20007Ool001imh0Ml0?mmh51mh1d07`Ml01/07ommh55mh0Ml01amh1Eoo`04001oogoo
0005Ool20005Ool01000Oomoo`000Woo00D007ooOomoo`0000Aoo`03001oogoo009oo`03001oogoo
009oo`05001oogooOol00007Ool00`00Oomoo`02Ool00`00Oomoo`03Ool20007Ool001imh0Ml0?mm
h51mh1d07`Ml01/07ommh55mh0Ml01amh1Ioo`8000Ioo`8000Eoo`04001oogoo0002Ool01@00Oomo
ogoo00002Goo00<007ooOol01Woo00<007ooOol01Goo00<007ooOol027oo0P001goo000NON07O03o
ON1@ON0M01l7O00K01ooON1AON07O00LON0FOol20006Ool20005Ool01000Oomoo`000Woo00@007oo
Oomoo`H000Aoo`03001oogoo00Ioo`03001oogoo00Eoo`H000Eoo`8000Moo`007WgP1g`0oggPD7gP
7@0O1g`06`0OoggPDGgP1g`077gP5Goo00@007ooOol000Eoo`8000Eoo`800005Ool00000Ool00003
Ool00`00Oomoo`02Ool00`00Oomoo`02Ool00`00Oomoo`06Ool20006Ool00`00Oomoo`02Ool00`00
Oomoo`03Ool20007Ool001imh0Ml0?mmh51mh1d07`Ml01/07ommh55mh0Ml01amh1=oo`<0009oo`<0
00=oo`8000Aoo`800005Ool007ooOol00005Ool40003Ool70003Ool2000017oo0000000017oo1000
1Woo0P001goo000NOol7O02eOol00`00Oomoo`2GOolM01l7O00K01nJOol00`00Oomoo`2cOol7O00k
Ool2000HOol00`00Oomoo`0HOol20008Ool001ioo`Ml0;Eoo`03001oogoo09Mooad07`Ml01X07i]o
o`03001oogoo0;=oo`Ml03]oo`8001Qoo`03001oogoo01Qoo`8000Qoo`007Woo1g`0ogooDGoo700O
1g`06P0OogooDWoo1g`0?7oo0P00<Goo0P002Goo000NOol7O03oOomAOolL01l7O00J01ooOomBOol7
O01jOol001ioo`Ml0?mooe5ooa`07`Ml01X07omooe9oo`Ml07Yoo`007Woo1g`0ogooDGoo700O1g`0
6P0OogooDWoo1g`0NWoo000NOol7O03oOomBOolK01l7O00I01ooOomCOol7O01jOol001ioo`Ml0?mo
oe9ooa/07`Ml01T07omooe=oo`Ml07Yoo`007Woo1g`0ogooDWoo6`0O1g`06@0OogooDgoo1g`0NWoo
000NOol7O03oOomCOolJ01l7O00H01ooOomDOol7O01jOol001ioo`Ml0?mooe=ooaX07`Ml01P07omo
oeAoo`Ml07Yoo`007Woo1g`0ogooE7oo6@0O1g`05`0OogooEGoo1g`0NWoo000NOol7O03oOomEOolH
01l7O00F01ooOomFOol7O01jOol001ioo`Ml0?mooeEooaP07`Ml01H07omooeIoo`Ml07Yoo`007Woo
1g`0ogooEWoo5`0O1g`05@0OogooEgoo1g`0NWoo000NOol7O03oOomGOolF01l7O00D01ooOomHOol7
O01jOol001ioo`Ml0?mooeQooaD07`Ml01<07omooeUoo`Ml07Yoo`007Woo1g`0ogooF7oo5@0O1g`0
4`0OogooFGoo1g`0NWoo000NOol7O03oOomIOolD01l7O00B01ooOomJOol7O01jOol001ioo`Ml0?mo
oeYooa<07`Ml01407omooe]oo`Ml07Yoo`007Woo1g`0ogooG7oo4@0O1g`03`0OogooGGoo1g`0NWoo
000NOol7O03oOomMOol@01l7O00>01ooOomNOol7O01jOol001ioo`Ml0?mooeioo`l07`Ml00d07omo
oemoo`Ml07Yoo`007Woo1g`0ogooH7oo3@0O1g`02`0OogooHGoo1g`0NWoo000NOol7O03oOomQOol<
01l7O00:01ooOomROol7O01jOol001ioo`Ml0?moof=oo`X07`Ml00P07omoofAoo`Ml07Yoo`007Woo
1g`0ogooIWoo1`0O1g`01@0OogooIgoo1g`0NWoo000NOol7O03oOomZOol301l7O0000`0OOomoo`3o
OomYOol7O01jOol001ioo`Ml0?moofeoo`Ml0?moofaoo`Ml07Yoo`007Woo1g`0ogooKGoo1g`0ogoo
K7oo1g`0NWoo000NOol7O03oOom]Ool7O03oOom/Ool7O01jOol001ioo`Ml0?moofeoo`Ml0?moofao
o`Ml07Yoo`007Woo1g`0ogooKGoo1g`0ogooK7oo1g`0NWoo000NOol7O03oOom]Ool7O03oOom/Ool7
O01jOol001ioo`Ml0?moofeoo`Ml0?moofaoo`Ml07Yoo`007Woo1g`0ogooKGoo1g`0ogooK7oo1g`0
NWoo000NOol7O03oOom]Ool7O03oOom/Ool7O01jOol001ioo`Ml0?moofeoo`Ml0?moofaoo`Ml07Yo
o`007Woo1g`0ogooKGoo1g`0ogooK7oo1g`0NWoo000NOol7O03oOom]Ool7O03oOom/Ool7O01jOol0
0?mooomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7Ool00?mooomooomoohMoo`00ogoo
ogooogooQgoo003oOoooOoooOon7Ool00001\
\>"],
  ImageRangeCache->{{{0, 899}, {99, 0}} -> {-1.09079, -0.123949, 0.00269808, \
0.00250401}}],

Cell[BoxData[
    TagBox[\(\[SkeletonIndicator]  Graphics  \[SkeletonIndicator]\),
      False,
      Editable->False]], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(Show[g1[1]]\)], "Input"],

Cell[GraphicsData["PostScript", "\<\
%!
%%Creator: Mathematica
%%AspectRatio: .61803 
%%ImageSize: 900 100 
MathPictureStart
/Mabs {
Mgmatrix idtransform
Mtmatrix dtransform
} bind def
/Mabsadd { Mabs
3 -1 roll add
3 1 roll add
exch } bind def
%% Graphics
%%IncludeResource: font Courier
%%IncludeFont: Courier
/Courier findfont 10  scalefont  setfont
% Scaling calculations
0.50116 0.464037 0.309017 1.54508 [
[ 0 0 0 0 ]
[ 1 .61803 0 0 ]
] MathScale
% Start of Graphics
1 setlinecap
1 setlinejoin
newpath
0 0 m
1 0 L
1 .61803 L
0 .61803 L
closepath
clip
newpath
0 1 0 r
.018 w
[ ] 0 setdash
.51276 .30902 m
.48956 .30902 L
s
% End of Graphics
MathPictureEnd
\
\>"], "Graphics",
  ImageSize->{900, 100},
  ImageCache->GraphicsData["Bitmap", "\<\
CF5dJ6E]HGAYHf4PAg9QL6QYHg<PAVmbKF5d0`4000>40000I1000`40O003h00OogooogooogooQgoo
003oOoooOoooOon7Ool00?mooomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7Ool00?mo
oomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7Ool00?mooomooomoohMoo`00ogooogoo
ogooQgoo003oOoooOoooOon7Ool00?mooomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7
Ool00?mooomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7Ool00?mooomooomoohMoo`00
ogooogooogooQgoo003oOoooOoooOon7Ool00?mooomooomoohMoo`00ogooogooogooQgoo003oOooo
OoooOon7Ool00?mooomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7Ool00?mooomooomo
ohMoo`00ogooogooogooQgoo003oOoooOoooOon7Ool00?mooomooomoohMoo`00ogooogooogooQgoo
003oOoooOoooOon7Ool00?mooomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7Ool00?mo
oomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7Ool00?mooomooomoohMoo`00ogooogoo
ogooQgoo003oOoooOoooOon7Ool00?mooomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7
Ool00?mooomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7Ool00?mooomooomoohMoo`00
ogooogooogooQgoo003oOoooOoooOon7Ool00?mookmoo`H3h?mool5oo`00ogoo_goo1P?Pogoo`Goo
003oOoooOoooOon7Ool00?mooomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7Ool00?mo
oomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7Ool00?mooomooomoohMoo`00ogooogoo
ogooQgoo003oOoooOoooOon7Ool00?mooomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7
Ool00?mooomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7Ool00?mooomooomoohMoo`00
ogooogooogooQgoo003oOoooOoooOon7Ool00?mooomooomoohMoo`00ogooogooogooQgoo003oOooo
OoooOon7Ool00?mooomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7Ool00?mooomooomo
ohMoo`00ogooogooogooQgoo003oOoooOoooOon7Ool00?mooomooomoohMoo`00ogooogooogooQgoo
003oOoooOoooOon7Ool00?mooomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7Ool00?mo
oomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7Ool00?mooomooomoohMoo`00ogooogoo
ogooQgoo003oOoooOoooOon7Ool00?mooomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7
Ool00?mooomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7Ool00?mooomooomoohMoo`00
ogooogooogooQgoo0000\
\>"],
  ImageRangeCache->{{{0, 899}, {99, 0}} -> {-6.04972, -0.200003, 0.0134532, \
0.00404043}}],

Cell[BoxData[
    TagBox[\(\[SkeletonIndicator]  Graphics  \[SkeletonIndicator]\),
      False,
      Editable->False]], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(Show[p[\(1/800\)/4], g1[\(1/800\)/4], g3[\(1/800\)/4], g4, 
      g2]\)], "Input"],

Cell[GraphicsData["PostScript", "\<\
%!
%%Creator: Mathematica
%%AspectRatio: .1 
%%ImageSize: 900 100 
MathPictureStart
/Mabs {
Mgmatrix idtransform
Mtmatrix dtransform
} bind def
/Mabsadd { Mabs
3 -1 roll add
3 1 roll add
exch } bind def
%% Graphics
%%IncludeResource: font Courier
%%IncludeFont: Courier
/Courier findfont 10  scalefont  setfont
% Scaling calculations
0.50116 0.464037 0.05 0.5 [
[.03712 .0375 -6 -9 ]
[.03712 .0375 6 0 ]
[.26914 .0375 -12 -9 ]
[.26914 .0375 12 0 ]
[.50116 .0375 -3 -9 ]
[.50116 .0375 3 0 ]
[.73318 .0375 -9 -9 ]
[.73318 .0375 9 0 ]
[.9652 .0375 -3 -9 ]
[.9652 .0375 3 0 ]
[1.025 .05 0 -7.0625 ]
[1.025 .05 76.3125 7.0625 ]
[ -0.005 0 0 0 ]
[ 1.005 .1 0 0 ]
] MathScale
% Start of Graphics
1 setlinecap
1 setlinejoin
newpath
0 g
.25 Mabswid
[ ] 0 setdash
.03712 .05 m
.03712 .05625 L
s
[(-1)] .03712 .0375 0 1 Mshowa
.26914 .05 m
.26914 .05625 L
s
[(-0.5)] .26914 .0375 0 1 Mshowa
.50116 .05 m
.50116 .05625 L
s
[(0)] .50116 .0375 0 1 Mshowa
.73318 .05 m
.73318 .05625 L
s
[(0.5)] .73318 .0375 0 1 Mshowa
.9652 .05 m
.9652 .05625 L
s
[(1)] .9652 .0375 0 1 Mshowa
.125 Mabswid
.08353 .05 m
.08353 .05375 L
s
.12993 .05 m
.12993 .05375 L
s
.17633 .05 m
.17633 .05375 L
s
.22274 .05 m
.22274 .05375 L
s
.31555 .05 m
.31555 .05375 L
s
.36195 .05 m
.36195 .05375 L
s
.40835 .05 m
.40835 .05375 L
s
.45476 .05 m
.45476 .05375 L
s
.54756 .05 m
.54756 .05375 L
s
.59397 .05 m
.59397 .05375 L
s
.64037 .05 m
.64037 .05375 L
s
.68677 .05 m
.68677 .05375 L
s
.77958 .05 m
.77958 .05375 L
s
.82599 .05 m
.82599 .05375 L
s
.87239 .05 m
.87239 .05375 L
s
.91879 .05 m
.91879 .05375 L
s
1 .5 0 r
.01 w
0 .05 m
1 .05 L
s
0 g
gsave
1.025 .05 -61 -11.0625 Mabsadd m
1 1 Mabs scale
currentpoint translate
0 22.125 translate 1 -1 scale
/g { setgray} bind def
/k { setcmykcolor} bind def
/p { gsave} bind def
/r { setrgbcolor} bind def
/w { setlinewidth} bind def
/C { curveto} bind def
/F { fill} bind def
/L { lineto} bind def
/rL { rlineto} bind def
/P { grestore} bind def
/s { stroke} bind def
/S { show} bind def
/N {currentpoint 3 -1 roll show moveto} bind def
/Msf { findfont exch scalefont [1 0 0 -1 0 0 ] makefont setfont} bind def
/m { moveto} bind def
/Mr { rmoveto} bind def
/Mx {currentpoint exch pop moveto} bind def
/My {currentpoint pop exch moveto} bind def
/X {0 rmoveto} bind def
/Y {0 exch rmoveto} bind def
63.000 14.375 moveto
%%IncludeResource: font Courier
%%IncludeFont: Courier
/Courier findfont 15.000 scalefont
[1 0 0 -1 0 0 ] makefont setfont
0.000 0.000 0.000 setrgbcolor
0.000 0.000 rmoveto
63.000 14.375 moveto
%%IncludeResource: font Courier
%%IncludeFont: Courier
/Courier findfont 15.000 scalefont
[1 0 0 -1 0 0 ] makefont setfont
0.000 0.000 0.000 setrgbcolor
(x) show
%%IncludeResource: font Mathematica2Mono-Bold
%%IncludeFont: Mathematica2Mono-Bold
/Mathematica2Mono-Bold findfont 15.000 scalefont
[1 0 0 -1 0 0 ] makefont setfont
0.000 0.000 0.000 setrgbcolor
72.000 14.375 moveto
(H) show
81.313 14.375 moveto
%%IncludeResource: font Courier
%%IncludeFont: Courier
/Courier findfont 15.000 scalefont
[1 0 0 -1 0 0 ] makefont setfont
0.000 0.000 0.000 setrgbcolor
(metre) show
%%IncludeResource: font Mathematica2Mono-Bold
%%IncludeFont: Mathematica2Mono-Bold
/Mathematica2Mono-Bold findfont 15.000 scalefont
[1 0 0 -1 0 0 ] makefont setfont
0.000 0.000 0.000 setrgbcolor
126.313 14.375 moveto
(L) show
135.313 14.375 moveto
%%IncludeResource: font Courier
%%IncludeFont: Courier
/Courier findfont 15.000 scalefont
[1 0 0 -1 0 0 ] makefont setfont
0.000 0.000 0.000 setrgbcolor
0.000 0.000 rmoveto
1.000 setlinewidth
grestore
0 0 m
1 0 L
1 .1 L
0 .1 L
closepath
clip
newpath
0 0 1 r
.08 w
.9652 .05 Mdot
1 0 0 r
.01 w
.50116 0 m
.50116 .1 L
s
.9652 .1 m
.9652 0 L
s
.03712 .1 m
.03712 0 L
s
0 1 0 r
.001 w
.50936 .05 m
.95699 .05 L
s
1 1 0 r
.022 w
.9652 .05 Mdot
1 0 0 r
.01 w
.9652 .1 m
.9652 0 L
s
.03712 .1 m
.03712 0 L
s
0 0 0 r
.025 w
.50116 .05 Mdot
% End of Graphics
MathPictureEnd
\
\>"], "Graphics",
  ImageSize->{900, 100},
  ImageCache->GraphicsData["Bitmap", "\<\
CF5dJ6E]HGAYHf4PAg9QL6QYHg<PAVmbKF5d0`4000>40000I1000`40O003h00OogooogooogooQgoo
003oOoooOoooOon7Ool00?mooomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7Ool00?mo
oomooomoohMoo`00ogooogooogooQgoo000NOol7O03oOom]Ool7O03oOom/Ool7O01jOol001ioo`Ml
0?moofeoo`Ml0?moofaoo`Ml07Yoo`007Woo1g`0ogooKGoo1g`0ogooK7oo1g`0NWoo000NOol7O03o
Oom]Ool7O03oOom/Ool7O01jOol001ioo`Ml0?moofeoo`Ml0?moofaoo`Ml07Yoo`007Woo1g`0ogoo
KGoo1g`0ogooK7oo1g`0NWoo000NOol7O03oOom]Ool7O03oOom/Ool7O01jOol001ioo`Ml0?moofeo
o`Ml0?moofaoo`Ml07Yoo`007Woo1g`0ogooKGoo1g`0ogooK7oo1g`0NWoo000NOol7O03oOom]Ool7
O03oOom/Ool7O01jOol001ioo`Ml0?moofeoo`Ml0?moofaoo`Ml07Yoo`007Woo1g`0ogooKGoo1g`0
ogooK7oo1g`0NWoo000NOol7O03oOom]Ool7O03oOom[Ool00`0OO01l0005O0000`0OOomoo`1gOol0
01ioo`Ml0?moofeoo`Ml0?moofIoo`H07`Ml00H07gAoo`007Woo1g`0ogooKGoo1g`0ogooHgoo2@0O
1g`02@0OLGoo000NOol7O03oOom]Ool7O03oOomQOol;01l7O00;01m_Ool001ioo`Ml0?moofeoo`Ml
0?moof1oo``07`Ml00`07fioo`007Woo1g`0ogooKGoo1g`0ogooGWoo3P0O1g`03P0OK7oo000NOol7
O03oOom]Ool7O03oOomMOol?01l7O00?01m[Ool001ioo`Ml0?moofeoo`Ml0?mooeaooa007`Ml0100
7fYoo`007Woo1g`0ogooKGoo1g`0ogooFWoo4P0O1g`04P0OJ7oo000NOol7O03oOom]Ool7O03oOomI
OolC01l7O00C01mWOol001ioo`Ml0?moofeoo`Ml0?mooeQooa@07`Ml01@07fIoo`007Woo1g`0ogoo
KGoo1g`0ogooF7oo500O1g`0500OIWoo000NOol7O03oOom]Ool7O03oOomGOolE01l7O00E01mUOol0
01ioo`Ml00800:moo`8000Aoo`8000Aoo`800:eoo`Ml0:ioo`8000Aoo`8000Aoo`8009UooaH07`Ml
01H07fAoo`007Woo1g`0/7oo00@007ooOol000Qoo`04001oogoo002/Ool7O02]Ool01000Oomoo`00
27oo00@007ooOol009MooaL07`Ml01L07f=oo`007Woo1g`0/7oo00@007ooOol000]oo`03001oogoo
0:Yoo`Ml0:eoo`04001oogoo000;Ool00`00Oomoo`2EOolG01l7O00G01mSOol001aoo`8000Ml0:Yo
o`@0009oo`04001oogoo0009Ool2002]Ool7O02]Ool01000Oomoo`002Goo0P00Ugoo600O1g`0600O
HWoo000NOol7O02`Ool01000Oomoo`002Goo00<007ooOol0[7oo1g`0[Goo00@007ooOol000Uoo`03
001oogoo09EooaT07`Ml01T07f5oo`007Woo1g`0/Goo0P002Woo0`00[7oo1g`0[Woo0P002Woo0`00
UGoo6@0O1g`06@0OHGoo000NOol7O03oOom]Ool7O03oOomBOolJ01l7O00J01mPOol001ioo`Ml0?mo
ofeoo`Ml0?mooe9ooaX07`Ml01X07f1oo`007Woo1g`0ogooKGoo1g`0ogooDWoo6P0O1g`06P0OH7oo
000NOol7O03oOom]Ool50002O03oOomAOolK01l7O00K01mOOol001ioo`Ml0?moof]oo`T00?mooe5o
oa/07`Ml01/07emoo`007Woo1g`0ogooJGoo3@00ogooCgoo6P0O00=oh7`0O0001G`000=oh00O01l0
600OGgoo000NOol7O03oOomXOol?003oOom>OolI01l2On07O002On0I01mOOol001ioo`Ml0?moofQo
o`l00?moodeooaT07`=oh0Ml00=oh1P07b5oo`80035oo`8000Uoo`007Woo1g`0ogooIgoo4@00ogoo
C7oo600O17oP1g`017oP600O7goo0P00<goo0P0027oo000NON07O03oON1WON0A003oON1<ON0H01l4
On07O004On0H01lOOol2000cOol20008Ool001imh0Ml0?mmh6Imh1<00?mmh4]mh1L07`Eoh0Ml00Eo
h1L07a=oo`<0009oo`<000=oo`8000Aoo`<00004Ool00000Ool20003Ool40006Ool40003Ool50005
Ool40006Ool20007Ool001imh0Ml0?mmh6Imh1<00?mmh4]mh1L07`Eoh0Ml00Eoh1L07aEoo`04001o
ogoo0005Ool20005Ool01000Oomoo`000Woo00D007ooOomoo`0000Aoo`03001oogoo009oo`03001o
ogoo009oo`05001oogooOol00007Ool00`00Oomoo`02Ool00`00Oomoo`03Ool20007Ool001imh0Ml
0?mmh6Imh1<00?l3h683h0Eoh0Ml00Eoh1L07aIoo`8000Ioo`8000Eoo`04001oogoo0002Ool01@00
Oomoogoo00002Goo00<007ooOol01Woo00<007ooOol01Goo00<007ooOol027oo0P001goo000NON07
O03oON1VON0C003oON1;ON0G01l5On07O005On0G01lFOol20006Ool20005Ool01000Oomoo`000Woo
00@007ooOomoo`H000Aoo`03001oogoo00Ioo`03001oogoo00Eoo`H000Eoo`8000Moo`007WgP1g`0
oggPIWgP4`00oggPBggP5`0O1GoP1g`01GoP5`0O5Goo00@007ooOol000Eoo`8000Eoo`800005Ool0
0000Ool00003Ool00`00Oomoo`02Ool00`00Oomoo`02Ool00`00Oomoo`06Ool20006Ool00`00Oomo
o`02Ool00`00Oomoo`03Ool20007Ool001imh0Ml0?mmh6Mmh1400?mmh4amh1P07`Aoh0Ml00Aoh1P0
7a=oo`<0009oo`<000=oo`8000Aoo`800005Ool007ooOol00005Ool40003Ool70003Ool2000017oo
0000000017oo10001Woo0P001goo000NOol7O02eOol00`00Oomoo`2^OolA002aOol00`00Oomoo`2G
OolH01l4On07O004On0H01lOOol2000HOol00`00Oomoo`0HOol20008Ool001ioo`Ml0;Eoo`03001o
ogoo0:moo`l00;9oo`03001oogoo09MooaT07`=oh0Ml00=oh1P07b1oo`8001Qoo`03001oogoo01Qo
o`8000Qoo`007Woo1g`0ogooJ7oo3`00ogooCWoo6@0O0WoP1g`00WoP6@0O8Goo0P00<Goo0P002Goo
000NOol7O03oOomYOol=003oOom?OolJ01l00goPO01l0005O0000goP01l07`0H01mOOol001ioo`Ml
0?moof]oo`T00?mooe5ooa/07`Ml01/07emoo`007Woo1g`0ogooKGoo1@000W`0ogooDGoo6`0O1g`0
6`0OGgoo000NOol7O03oOom]Ool7O03oOomBOolJ01l7O00J01mPOol001ioo`Ml0?moofeoo`Ml0?mo
oe9ooaX07`Ml01X07f1oo`007Woo1g`0ogooKGoo1g`0ogooDWoo6P0O1g`06P0OH7oo000NOol7O03o
Oom]Ool7O03oOomCOolI01l7O00I01mQOol001ioo`Ml0?moofeoo`Ml0?mooe=ooaT07`Ml01T07f5o
o`007Woo1g`0ogooKGoo1g`0ogooE7oo600O1g`0600OHWoo000NOol7O03oOom]Ool7O03oOomEOolG
01l7O00G01mSOol001ioo`Ml0?moofeoo`Ml0?mooeEooaL07`Ml01L07f=oo`007Woo1g`0ogooKGoo
1g`0ogooEWoo5P0O1g`05P0OI7oo000NOol7O03oOom]Ool7O03oOomGOolE01l7O00E01mUOol001io
o`Ml0?moofeoo`Ml0?mooeQooa@07`Ml01@07fIoo`007Woo1g`0ogooKGoo1g`0ogooF7oo500O1g`0
500OIWoo000NOol7O03oOom]Ool7O03oOomIOolC01l7O00C01mWOol001ioo`Ml0?moofeoo`Ml0?mo
oeYooa807`Ml01807fQoo`007Woo1g`0ogooKGoo1g`0ogooG7oo400O1g`0400OJWoo000NOol7O03o
Oom]Ool7O03oOomMOol?01l7O00?01m[Ool001ioo`Ml0?moofeoo`Ml0?mooeioo`h07`Ml00h07fao
o`007Woo1g`0ogooKGoo1g`0ogooH7oo300O1g`0300OKWoo000NOol7O03oOom]Ool7O03oOomQOol;
01l7O00;01m_Ool001ioo`Ml0?moofeoo`Ml0?moof=oo`T07`Ml00T07g5oo`007Woo1g`0ogooKGoo
1g`0ogooIWoo1P0O1g`01P0OM7oo000NOol7O03oOom]Ool7O03oOomZOol201l7O00201mhOol001io
o`Ml0?moofeoo`Ml0?moofaoo`Ml07Yoo`007Woo1g`0ogooKGoo1g`0ogooK7oo1g`0NWoo000NOol7
O03oOom]Ool7O03oOom/Ool7O01jOol001ioo`Ml0?moofeoo`Ml0?moofaoo`Ml07Yoo`007Woo1g`0
ogooKGoo1g`0ogooK7oo1g`0NWoo000NOol7O03oOom]Ool7O03oOom/Ool7O01jOol001ioo`Ml0?mo
ofeoo`Ml0?moofaoo`Ml07Yoo`007Woo1g`0ogooKGoo1g`0ogooK7oo1g`0NWoo000NOol7O03oOom]
Ool7O03oOom/Ool7O01jOol001ioo`Ml0?moofeoo`Ml0?moofaoo`Ml07Yoo`007Woo1g`0ogooKGoo
1g`0ogooK7oo1g`0NWoo000NOol7O03oOom]Ool7O03oOom/Ool7O01jOol00?mooomooomoohMoo`00
ogooogooogooQgoo003oOoooOoooOon7Ool00?mooomooomoohMoo`00ogooogooogooQgoo003oOooo
OoooOon7Ool00001\
\>"],
  ImageRangeCache->{{{0, 899}, {99, 0}} -> {-1.09079, -0.123949, 0.00269808, \
0.00250401}}],

Cell[BoxData[
    TagBox[\(\[SkeletonIndicator]  Graphics  \[SkeletonIndicator]\),
      False,
      Editable->False]], "Output"]
}, Open  ]],

Cell[BoxData[
    \(Table[
      Show[p[n], g1[n], g3[n], g4, g2], {n, 0, 
        1/800, \(1/800\)/50}]\)], "Input"]
},
FrontEndVersion->"4.2 for Microsoft Windows",
ScreenRectangle->{{0, 1024}, {0, 690}},
WindowSize->{1016, 658},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
PrintingCopies->1,
PrintingPageRange->{Automatic, Automatic},
CellLabelAutoDelete->True,
StyleDefinitions -> "Classroom.nb"
]

(*******************************************************************
Cached data follows.  If you edit this Notebook file directly, not
using Mathematica, you must remove the line containing CacheID at
the top of  the file.  The cache data will then be recreated when
you save this file from within Mathematica.
*******************************************************************)

(*CellTagsOutline
CellTagsIndex->{}
*)

(*CellTagsIndex
CellTagsIndex->{}
*)

(*NotebookFileOutline
Notebook[{
Cell[1754, 51, 711, 12, 130, "Input"],
Cell[2468, 65, 373, 7, 90, "Input"],
Cell[2844, 74, 127, 3, 50, "Input"],
Cell[2974, 79, 152, 3, 50, "Input"],
Cell[3129, 84, 215, 4, 50, "Input"],

Cell[CellGroupData[{
Cell[3369, 92, 41, 1, 50, "Input"],
Cell[3413, 95, 2866, 71, 186, 620, 39, "GraphicsData", "PostScript", \
"Graphics"],
Cell[6282, 168, 130, 3, 49, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[6449, 176, 43, 1, 50, "Input"],
Cell[6495, 179, 9431, 283, 108, 3800, 210, "GraphicsData", "PostScript", \
"Graphics"],
Cell[15929, 464, 130, 3, 49, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[16096, 472, 44, 1, 50, "Input"],
Cell[16143, 475, 2810, 73, 108, 679, 43, "GraphicsData", "PostScript", \
"Graphics"],
Cell[18956, 550, 130, 3, 49, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[19123, 558, 102, 2, 50, "Input"],
Cell[19228, 562, 9738, 304, 108, 3965, 229, "GraphicsData", "PostScript", \
"Graphics"],
Cell[28969, 868, 130, 3, 49, "Output"]
}, Open  ]],
Cell[29114, 874, 117, 3, 50, "Input"]
}
]
*)



(*******************************************************************
End of Mathematica Notebook file.
*******************************************************************)

