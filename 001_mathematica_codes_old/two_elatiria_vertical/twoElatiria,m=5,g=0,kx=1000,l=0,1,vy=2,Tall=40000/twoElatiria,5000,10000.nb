(************** Content-type: application/mathematica **************
                     CreatedBy='Mathematica 4.2'

                    Mathematica-Compatible Notebook

This notebook can be used with any Mathematica-compatible
application, such as Mathematica, MathReader or Publicon. The data
for the notebook starts with the line containing stars above.

To get the notebook into a Mathematica-compatible application, do
one of the following:

* Save the data starting with the line of stars above into a file
  with a name ending in .nb, then open the file inside the
  application;

* Copy the data starting with the line of stars above to the
  clipboard, then use the Paste menu command inside the application.

Data for notebooks contains only printable 7-bit ASCII and can be
sent directly in email or through ftp in text mode.  Newlines can be
CR, LF or CRLF (Unix, Macintosh or MS-DOS style).

NOTE: If you modify the data for this notebook not in a Mathematica-
compatible application, you must delete the line below containing
the word CacheID, otherwise Mathematica-compatible applications may
try to use invalid cache data.

For more information on notebooks and Mathematica-compatible 
applications, contact Wolfram Research:
  web: http://www.wolfram.com
  email: info@wolfram.com
  phone: +1-217-398-0700 (U.S.)

Notebook reader applications are available free of charge from 
Wolfram Research.
*******************************************************************)

(*CacheID: 232*)


(*NotebookFileLineBreakTest
NotebookFileLineBreakTest*)
(*NotebookOptionsPosition[     13695,        339]*)
(*NotebookOutlinePosition[     14375,        362]*)
(*  CellTagsIndexPosition[     14331,        358]*)
(*WindowFrame->Normal*)



Notebook[{
Cell[BoxData[
    \(\(Clear[m, g, kx, ky, l, vx, vy, f, Tinitial, Tfinal];\)\)], "Input"],

Cell[BoxData[
    \(f1[m_, g_, kx_, ky_, l_, vx_, vy_, Tinitial_, Tfinal_] := 
      NDSolve[{m*\(x''\)[t] \[Equal] 
            ky*Abs[\((\((l - x[t])\)^2 + \((l + y[t])\)^2)\)^0.5 - 
                    l]*\((\(-1\))\)*
                Sign[\((\((l - x[t])\)^2 + \((l + y[t])\)^2)\)^0.5 - 
                    l]*\((x[t] - 
                      l)\)/\((\((l - x[t])\)^2 + \((l + y[t])\)^2)\)^0.5 + 
              kx*Abs[\((\((x[t])\)^2 + \((y[t])\)^2)\)^0.5 - l]*
                Sign[\((\((x[t])\)^2 + \((y[t])\)^2)\)^0.5 - l]*\((\(-1\))\)*
                x[t]/\((\((x[t])\)^2 + \((y[t])\)^2)\)^0.5 + m*g, 
          m*\(y''\)[t] \[Equal] 
            ky*Abs[\((\((l - x[t])\)^2 + \((l + y[t])\)^2)\)^0.5 - 
                    l]*\((\(-1\))\)*
                Sign[\((\((l - x[t])\)^2 + \((l + y[t])\)^2)\)^0.5 - 
                    l]*\((y[t] + 
                      l)\)/\((\((l - x[t])\)^2 + \((l + y[t])\)^2)\)^0.5 + 
              kx*Abs[\((\((x[t])\)^2 + \((y[t])\)^2)\)^0.5 - l]*\((\(-1\))\)*
                Sign[\((\((x[t])\)^2 + \((y[t])\)^2)\)^0.5 - l]*
                y[t]/\((\((x[t])\)^2 + \((y[t])\)^2)\)^0.5, x[0] \[Equal] l, 
          y[0] \[Equal] 0, \(x'\)[0] \[Equal] vx, \(y'\)[0] \[Equal] vy}, {x, 
          y}, {t, Tinitial, Tfinal}, MaxSteps \[Rule] 50000000000]\)], "Input"],

Cell[BoxData[
    \(m = 5; g = 0; kx = 1000; ky = 0; l = 0.1; vx = 0; vy = 2; f = 2; 
    Tinitial = 5000; Tfinal = 10000;\)], "Input"],

Cell[BoxData[
    \(\(s1 = f1[m, g, kx, ky, l, vx, vy, Tinitial, Tfinal];\)\)], "Input"],

Cell[BoxData[
    \(Clear[ti, dt]\)], "Input"],

Cell[BoxData[
    \(f2[ti_, dt_] := 
      ParametricPlot[{{x[t], y[t]} /. s1, 
          l*{Cos[2*Pi*f*t], Sin[2*Pi*f*t]}}, {t, ti, ti + dt}, 
        Compiled \[Rule] False, PlotPoints \[Rule] 500, MaxBend \[Rule] 0, 
        PlotStyle \[Rule] {{RGBColor[1, 0, 0], 
              Thickness[0.005]}, {RGBColor[0, 1, 0], Thickness[0.01]}}, 
        PlotRange \[Rule] All, Axes \[Rule] True, 
        AxesLabel \[Rule] {StyleForm["\<x(metre)\>", FontSize \[Rule] 15, 
              FontWeight \[Rule] "\<Bold\>"], 
            StyleForm["\<y(metre)\>", FontSize \[Rule] 15, 
              FontWeight -> "\<Bold\>"]}, AspectRatio \[Rule] Automatic, 
        AxesStyle \[Rule] {{RGBColor[0, 0, 0], 
              Thickness[0.004]}, {RGBColor[0, 0, 0], Thickness[0.004]}}, 
        ImageSize \[Rule] {500, 500}]\)], "Input"],

Cell[BoxData[
    \(Clear[sz, ti, tf, dt]\)], "Input"],

Cell[BoxData[
    \(f3[sz_, ti_, tf_, dt_] := 
      Table[ParametricPlot[{{x[t], y[t]} /. s1, 
            l*{Cos[2*Pi*f*t], Sin[2*Pi*f*t]}}, {t, ti, ti + dt}, 
          Compiled \[Rule] False, PlotPoints \[Rule] 50, MaxBend \[Rule] 0, 
          PlotStyle \[Rule] {{RGBColor[1, 0, 0], 
                Thickness[0.005]}, {RGBColor[0, 1, 0], Thickness[0.01]}}, 
          PlotRange \[Rule] {{\(-sz\), sz}, {\(-sz\), sz}}, 
          Axes \[Rule] False, 
          AxesLabel \[Rule] {StyleForm["\<x(metre)\>", FontSize \[Rule] 15, 
                FontWeight \[Rule] "\<Bold\>"], 
              StyleForm["\<y(metre)\>", FontSize \[Rule] 15, 
                FontWeight -> "\<Bold\>"]}, AspectRatio \[Rule] Automatic, 
          AxesStyle \[Rule] {{RGBColor[0, 0, 0], 
                Thickness[0.004]}, {RGBColor[0, 0, 0], Thickness[0.004]}}, 
          ImageSize \[Rule] {500, 500}], {n, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(\(Clear[X, Y, vX, vY, t];\)\)], "Input"],

Cell[BoxData[
    \(\(X[t_] = x[t] /. First[s1];\)\)], "Input"],

Cell[BoxData[
    \(\(vX[t_] = \(x'\)[t] /. First[s1];\)\)], "Input"],

Cell[BoxData[
    \(\(Y[t_] = y[t] /. First[s1];\)\)], "Input"],

Cell[BoxData[
    \(\(vY[t_] = \(y'\)[t] /. First[s1];\)\)], "Input"],

Cell[BoxData[
    \(\(Clear[s, dt, dS, En, meanEn, ti, dt, V, L, tf];\)\)], "Input"],

Cell[BoxData[
    \(dS[ti_, dt_] := 
      NIntegrate[\((\((vX[t])\)^2 + \((vY[t])\)^2)\)^0.5, {t, ti, ti + dt}, 
        MaxRecursion \[Rule] 50, WorkingPrecision \[Rule] 30, 
        Method \[Rule] QuasiMonteCarlo]\)], "Input"],

Cell[BoxData[
    \(dS1[ti_, dt_] := 
      NIntegrate[\((\((vX[t])\)^2 + \((vY[t])\)^2)\)^0.5, {t, ti, ti + dt}, 
        MinRecursion \[Rule] 5, MaxRecursion \[Rule] 50]\)], "Input"],

Cell[BoxData[
    \(dS2[ti_, dt_] := 
      NIntegrate[\((\((vX[t])\)^2 + \((vY[t])\)^2)\)^0.5, {t, ti, ti + dt}, 
        MaxRecursion \[Rule] 50]\)], "Input"],

Cell[BoxData[
    \(En[t_] := 
      1/2*kx*\((Abs[\((\((X[t])\)^2 + \((Y[t])\)^2)\)^0.5 - l])\)^2*
          Sign[\((\((\((X[t])\)^2 + \((Y[t])\)^2)\)^0.5 - 
                l)\)]*\((\(-1\))\) + 
        1/2*m*\((\((vX[t])\)^2 + \((vY[t])\)^2)\)\)], "Input"],

Cell[BoxData[
    \(V[t_] := \((\((vX[t])\)^2 + \((vY[t])\)^2)\)^0.5\)], "Input"],

Cell[BoxData[
    \(Ekin[t_] := 1/2*m*\((\((vX[t])\)^2 + \((vY[t])\)^2)\)\)], "Input"],

Cell[BoxData[
    \(Edyn[t_, kx_, ky_] := 
      1/2*kx*\((Abs[\((\((X[t])\)^2 + \((Y[t])\)^2)\)^0.5 - l])\)^2 + 
        1/2*ky*\((Abs[\((\((l - X[t])\)^2 + \((l + Y[t])\)^2)\)^0.5 - 
                  l])\)^2\)], "Input"],

Cell[BoxData[
    \(meanEn[ti_, dt_] := 
      NIntegrate[En[t], {t, ti, ti + dt}, MinRecursion \[Rule] 5, 
          MaxRecursion \[Rule] 50, WorkingPrecision \[Rule] 30, 
          Method \[Rule] QuasiMonteCarlo]/\((dt)\)\)], "Input"],

Cell[BoxData[
    \(meanEn1[ti_, dt_] := 
      NIntegrate[En[t], {t, ti, ti + dt}, MinRecursion \[Rule] 5, 
          MaxRecursion \[Rule] 20]/\((dt)\)\)], "Input"],

Cell[BoxData[
    \(meanEn2[ti_, dt_] := 
      NIntegrate[En[t], {t, ti, ti + dt}, 
          MaxRecursion -> 50]/\((dt)\)\)], "Input"],

Cell[BoxData[
    \(L[t_] := X[t]*m*vY[t] - Y[t]*m*vX[t]\)], "Input"],

Cell[BoxData[
    \(meanL[t_, dt_] := 
      NIntegrate[L[n]/dt, {n, t, t + dt}, MinRecursion \[Rule] 5, 
        MaxRecursion \[Rule] 50, WorkingPrecision \[Rule] 30, 
        Method \[Rule] QuasiMonteCarlo]\)], "Input"],

Cell[BoxData[
    \(meanL1[t_, dt_] := 
      NIntegrate[L[n]/dt, {n, t, t + dt}, MinRecursion \[Rule] 5, 
        MaxRecursion \[Rule] 20]\)], "Input"],

Cell[BoxData[
    \(meanL2[t_, dt_] := 
      NIntegrate[L[n]/dt, {n, t, t + dt}, MaxRecursion \[Rule] 50]\)], "Input"],

Cell[BoxData[
    \(f4dS[ti_, tf_, dt_] := Table[dS[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f4dS1[ti_, tf_, dt_] := Table[dS1[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f4dS2[ti_, tf_, dt_] := Table[dS2[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
    \(f4dS[5000, 9900, 100]\)], "Input"],

Cell[BoxData[
    \({127.73513970651672`, 127.04825904387522`, 126.3959304184959`, 
      125.68054143880791`, 125.05720488902661`, 124.3474850313006`, 
      123.72604004592235`, 123.0069939608181`, 122.35824214998834`, 
      121.63806522584903`, 120.89310409724358`, 120.23860089427899`, 
      119.5606890800781`, 118.794276832228`, 118.09392684736883`, 
      117.34424544178198`, 116.55585730200914`, 115.88919889371623`, 
      115.0829007437655`, 114.36679294347977`, 113.55900336379186`, 
      112.8516845139318`, 112.02882494268327`, 111.20487546521444`, 
      110.4606071937075`, 109.70277636785612`, 108.8942690436442`, 
      108.10658818958846`, 107.3375524260296`, 106.49434951284103`, 
      105.71239038277481`, 104.84709228000607`, 104.04108310297964`, 
      103.23828971114713`, 102.35784392787247`, 101.54327544183826`, 
      100.71409678561903`, 99.84751859483805`, 99.01326685176893`, 
      98.10231635078557`, 97.29786021701373`, 96.40625255520553`, 
      95.57711002842922`, 94.74586413843201`, 93.87746195973392`, 
      93.05149417329811`, 92.23560007255777`, 91.36360248436843`, 
      90.54992633087572`, 89.71931883090788`}\)], "Output"]
}, Open  ]],

Cell[BoxData[
    \(f5meanEn[ti_, tf_, dt_] := 
      Table[meanEn[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f5meanEn1[ti_, tf_, dt_] := 
      Table[meanEn1[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f5meanEn2[ti_, tf_, dt_] := 
      Table[meanEn2[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
    \(f5meanEn[5000, 9900, 100]\)], "Input"],

Cell[BoxData[
    \({2.4329328503535392`, 2.4119712459449976`, 2.3941227045849702`, 
      2.368877154945195`, 2.354871229266743`, 2.330045551699812`, 
      2.3159916120426813`, 2.290886307814467`, 2.2740855621212424`, 
      2.252053058001314`, 2.22639511993518`, 2.2107905923875064`, 
      2.1921482864941977`, 2.1675804588835614`, 2.1485789329916427`, 
      2.126511941783197`, 2.100307710734236`, 2.085131481176388`, 
      2.058870942974158`, 2.041403320132174`, 2.0154831595406053`, 
      1.9991875726535397`, 1.9730343915681177`, 1.947093794634148`, 
      1.929096613915654`, 1.9076310532039087`, 1.8842989025739934`, 
      1.8619635875756502`, 1.842992039036814`, 1.816546833839439`, 
      1.7967871282510404`, 1.7696244151402316`, 1.7492700891772825`, 
      1.7288687965412415`, 1.7018991606553624`, 1.6812514268550123`, 
      1.6591790027220876`, 1.6352166343612355`, 1.613270590895606`, 
      1.5870242791799791`, 1.5680443806432038`, 1.5420108609898588`, 
      1.5213807411944444`, 1.4997866594021383`, 1.4757558835183946`, 
      1.4545111646978373`, 1.4342732403910015`, 1.4091708880565392`, 
      1.389999493074471`, 1.36823932016129`}\)], "Output"]
}, Open  ]],

Cell[BoxData[
    \(f6meanL[ti_, tf_, dt_] := 
      Table[meanL[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f6meanL1[ti_, tf_, dt_] := 
      Table[meanL1[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f6meanL2[ti_, tf_, dt_] := 
      Table[meanL2[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
    \(f6meanL[5000, 9900, 100]\)], "Input"],

Cell[BoxData[
    \({0.7216707315975694`, 0.7160623664122234`, 0.7104534263623505`, 
      0.7049057827549493`, 0.6993929728648876`, 0.6939274275688144`, 
      0.6884701426499001`, 0.6829699058507815`, 0.6774038734302364`, 
      0.6718453442444011`, 0.6662471046076348`, 0.6606614940432399`, 
      0.6550350833121884`, 0.6493309045351348`, 0.6436229643638121`, 
      0.6379146112862689`, 0.6321935199923481`, 0.6264628685514501`, 
      0.6206934394270806`, 0.6149149666625933`, 0.6091328403029399`, 
      0.6032860389870919`, 0.5974546288558761`, 0.5916329968453253`, 
      0.5858263462477249`, 0.5800740779581175`, 0.5742835035688074`, 
      0.5684484357450864`, 0.5625722221286847`, 0.5567595620802733`, 
      0.5509094684017678`, 0.544994314525766`, 0.5390328822042764`, 
      0.5331144526242659`, 0.5271883000510788`, 0.5212608453775281`, 
      0.5153170230808622`, 0.5093822707473299`, 0.5034312604992703`, 
      0.49745889394511456`, 0.4915077715578113`, 0.48565924925648823`, 
      0.4798711002796716`, 0.47412263608169647`, 0.4684449014395634`, 
      0.4627678489937904`, 0.45715833265761696`, 0.4515963175311032`, 
      0.4460247016563957`, 0.4404449407497761`}\)], "Output"]
}, Open  ]],

Cell[BoxData[
    \(f7graphic[t_, sz_] := 
      Graphics[{{Circle[{0, 0}, l], RGBColor[0, 1, 0]}, {PointSize[ .04], 
            RGBColor[1, 0, 0], Point[{X[t], Y[t]}]}}, Axes \[Rule] True, 
        AxesLabel \[Rule] {StyleForm["\<x(metre)\>", FontSize \[Rule] 15, 
              FontWeight \[Rule] "\<Bold\>"], 
            StyleForm["\<y(metre)\>", FontSize -> 15, 
              FontWeight -> "\<Bold\>"]}, AspectRatio \[Rule] Automatic, 
        AxesStyle \[Rule] {{RGBColor[1, 0.5, 0], 
              Thickness[0.01]}, {RGBColor[1, 0.5, 0], Thickness[0.01]}}, 
        PlotRange \[Rule] {{\(-sz\), sz}, {\(-sz\), sz}}, 
        ImageSize \[Rule] {500, 500}, 
        Prolog \[Rule] {RGBColor[0, 1, 0], Thickness[0.02]}]\)], "Input"],

Cell[BoxData[
    \(Clear[ti, tf, dt]\)], "Input"],

Cell[BoxData[
    \(f8table[ti_, tf_, dt_] := 
      Table[Show[f7graphic[t, 0.22]], {t, ti, tf, dt}]\)], "Input"]
},
FrontEndVersion->"4.2 for Microsoft Windows",
ScreenRectangle->{{0, 1024}, {0, 690}},
WindowSize->{1016, 659},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
StyleDefinitions -> "Classroom.nb"
]

(*******************************************************************
Cached data follows.  If you edit this Notebook file directly, not
using Mathematica, you must remove the line containing CacheID at
the top of  the file.  The cache data will then be recreated when
you save this file from within Mathematica.
*******************************************************************)

(*CellTagsOutline
CellTagsIndex->{}
*)

(*CellTagsIndex
CellTagsIndex->{}
*)

(*NotebookFileOutline
Notebook[{
Cell[1754, 51, 89, 1, 50, "Input"],
Cell[1846, 54, 1309, 21, 210, "Input"],
Cell[3158, 77, 135, 2, 50, "Input"],
Cell[3296, 81, 88, 1, 50, "Input"],
Cell[3387, 84, 46, 1, 50, "Input"],
Cell[3436, 87, 820, 14, 150, "Input"],
Cell[4259, 103, 54, 1, 50, "Input"],
Cell[4316, 106, 916, 15, 170, "Input"],
Cell[5235, 123, 60, 1, 50, "Input"],
Cell[5298, 126, 63, 1, 50, "Input"],
Cell[5364, 129, 69, 1, 50, "Input"],
Cell[5436, 132, 63, 1, 50, "Input"],
Cell[5502, 135, 69, 1, 50, "Input"],
Cell[5574, 138, 84, 1, 50, "Input"],
Cell[5661, 141, 229, 4, 70, "Input"],
Cell[5893, 147, 184, 3, 50, "Input"],
Cell[6080, 152, 160, 3, 50, "Input"],
Cell[6243, 157, 259, 5, 70, "Input"],
Cell[6505, 164, 81, 1, 50, "Input"],
Cell[6589, 167, 86, 1, 50, "Input"],
Cell[6678, 170, 223, 4, 50, "Input"],
Cell[6904, 176, 236, 4, 70, "Input"],
Cell[7143, 182, 165, 3, 50, "Input"],
Cell[7311, 187, 136, 3, 50, "Input"],
Cell[7450, 192, 69, 1, 50, "Input"],
Cell[7522, 195, 221, 4, 70, "Input"],
Cell[7746, 201, 152, 3, 50, "Input"],
Cell[7901, 206, 119, 2, 50, "Input"],
Cell[8023, 210, 89, 1, 50, "Input"],
Cell[8115, 213, 91, 1, 50, "Input"],
Cell[8209, 216, 91, 1, 50, "Input"],

Cell[CellGroupData[{
Cell[8325, 221, 54, 1, 50, "Input"],
Cell[8382, 224, 1172, 17, 106, "Output"]
}, Open  ]],
Cell[9569, 244, 104, 2, 50, "Input"],
Cell[9676, 248, 106, 2, 50, "Input"],
Cell[9785, 252, 106, 2, 50, "Input"],

Cell[CellGroupData[{
Cell[9916, 258, 58, 1, 50, "Input"],
Cell[9977, 261, 1176, 17, 106, "Output"]
}, Open  ]],
Cell[11168, 281, 102, 2, 50, "Input"],
Cell[11273, 285, 104, 2, 50, "Input"],
Cell[11380, 289, 104, 2, 50, "Input"],

Cell[CellGroupData[{
Cell[11509, 295, 57, 1, 50, "Input"],
Cell[11569, 298, 1199, 17, 106, "Output"]
}, Open  ]],
Cell[12783, 318, 738, 12, 130, "Input"],
Cell[13524, 332, 50, 1, 50, "Input"],
Cell[13577, 335, 114, 2, 50, "Input"]
}
]
*)



(*******************************************************************
End of Mathematica Notebook file.
*******************************************************************)

