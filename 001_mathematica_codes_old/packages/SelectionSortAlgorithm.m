(* :Title: SelectionSortAlgorithm With Options. *)

(* :Authors: Paul Galiatsatos. *)

(* :Context: PaulsTools`SelectionSortAlgorithm` *)

(* :Package Version: 1.0 *)

(* :Copyright: No Copyright *)

(* :History: 
	Version 1.0 by Paul Galiatsatos, December 2004.
*)

(* :Keywords: *)

(* :Source: None. *)

(* :Warning: *)

(* :Mathematica Version: 5.1 *)

(* :Limitation: *)

                                           
BeginPackage["PaulsTools`SelectionSortAlgorithm`"]

                                                                  
      

SelectionSortAlgorithm::usage =
"SelectionSortAlgorithm[{n1,n2,...,nN},opts] 
\nsorts the list {n1,n2,...,nN}.
\nYou can give options.
\nThe options must have the form 
\nToPrint->Yes or No,GridArray->Yes or No,Steps->Yes or No.
\nYou can give any option you want or a combination of options!.
\nThe Order is arbitrary!."



ToPrint::usage =
"ToPrint is an option which can take the values Yes Or No.
\nThe Default value is No.
\nIf you choose Yes then all intermediates results are printed."


GridArray::usage =
"GridArray is an option which can take the values Yes or No. 
\nThe default value is No.
\nIf you choose Yes then all intermediates results
\nare printed in BoxedForm."



Steps::usage = "Steps gives the steps of SelectionSortAlgorithm."

Yes::usage = "Yes is one of the TWO possibles assignments for options."

No::usage = "No is one of the TWO possibles assignments for options."


SelectionSortAlgorithm::"BadElements AND/OR Bad Options" = 
"You Gave Bad Options AND/OR Bad ElementsList.
\nGive only options of the form ToPrint->Yes or No,
\nGridArray->Yes or No,Steps->Yes or No.
\nElements of the list must be only Numerics!.
\nFor now I Abort the evaluation!."



Begin["`Private`"]

Needs["PaulsTools`SwapFunction`"]

Unprotect[ToString]

Unprotect["PaulsTools`SelectionSortAlgorithm`*"]

SetAttributes[ToString, {Listable}]

Protect[ToString]

Options[SelectionSortAlgorithm] = {ToPrint -> "No", GridArray -> "No", 
    Steps -> "No"}


(* All Possible Options *) 

(*No Mistakes HERE *)               

(* a Area *)

SelectionSortAlgorithm[MyList_, opts___] :=
  
  (Module[{a1, a2, a3, a4, a5, a6, a7, a8, a9, a10,
        b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15, 
        b16,
        c1, c2, c3, c4, c5,
        f1, f2, f13, f4, f5, f6, f7, f8, f9, f10,
        e1, e2, e3,
        s, la, las, n, i, j,
        optionsList, optionsLength,
        optionsNames, optionsNamesLength, optionsValues,
        PossiblesValues, optionsValuesLength, optionsGurrent, 
        optionsGurrentLength,
        optionsLevel0, testNamesSymbol, onlyRules, optionsGurrentNames, 
        initialMinPosition,
        stepNumber, lengthOfList, TheOldPossibleMin, TheNewPossibleMin, 
        IndexOldPossibleMin, IndexNewPossibleMin, smax},
      
a1 = optionsList = Options[SelectionSortAlgorithm];

      a2 = optionsLength = Length[optionsList];

      a3 = optionsNames = Map[ToString, Table[Part[a1, n, 1], {n, 1, a2}]];

      a4 = Sort[a3];

      a5 = optionsNamesLength = Length[a4];

      a6 = optionsValues = Map[ToString, Table[Part[a1, n, 2], {n, 1, a2}]];

      a7 = Sort[a6];

      a71 = PossiblesValues = {"No", "Yes"};

      a8 = Union[a7, a7, a71];

      a9 = optionsValuesLength = Length[a7];
      
Do[a10[i] = Part[a7, i], {i, 1, Length[a7]}];
      
(*Gurrent Options*)    
      
(*The Possibility Of Mistakes Exists*)           

(* b Area *)
      
      
      b1 = opts;

      b2 = optionsGurrent = {b1};

      b3 = optionsGurrentLength = Length[b2];

      b4 = optionsLevel0 = Map[ToString, Table[Part[b2, n, 0], {n, 1, b3}]];

      b5 = Length[b4];

      b6 = Range[b5];

      b7 = Transpose[{b4, b6}];

      b8 = testNamesSymbol = Cases[b7, {ToString[Symbol], _}, {0, Infinity}];

      b9 = Cases[b8, _Integer, {0, Infinity}];

      b10 = Length[b9];

      b11 = onlyRules = Delete[b2, Map[List, b9]];

      b12 = Length[b11];

      b13 = 
        optionsGurrentNames = 
          Map[ToString, Table[Part[b11, n, 1], {n, 1, b12}]];

      b14 = Sort[Union[b13, b13]];

      b15 = 
        optionsGurrentValues = 
          Map[ToString, Table[Part[b11, n, 2], {n, 1, b12}]];

      b16 = Sort[Union[b15, b15]];
      
(* Check For Errors In List *)
                    
(* c area *)
      
      
c1 = las[0] = MyList;

      c2 = Sort[c1];

      c3 = Head[c2];

      c4 = Cases[c2, _?NumericQ];

      c5 = Complement[c2, c4];

(* cases *)          

(* We Check For Mistakes Here *)           

(* f area *)
      
(* if options names are ok *)
      
      f1 = Union[a4, b14];

      f2 = Union[f1, f1];

      f3 = (f2 == a4) || (b1 === Sequence[]);

      f4 = Flatten[Table[Cases[b16, Part[a8, i]], {i, 1, Length[a8]}]];
      
f5 = Complement[b16, f4];
      
      f6 = (f5 == {});
      
(*if options values are ok*)

f7 = (b1 === Sequence[]) || f6;           
  
(*if options symbols are NOT existed*)
      
f8 = (b8 == {});                    

(*if head of list Is List*)      

f9 = (c3 == List);                       

(*if there are only Numerics inside List*)      

f10 = (c5 == {});                       
   

(*Final Tests*)           

(*e area*)
      
(*test for bad options*) 
   
e1 = (f3 && f7 && f8);              
      
(*test for bad List*)

e2 = (f9 && f10);             
      
(*check if everything is OK*)

e3 = e2 && e1;           
      
Which[e3 == False,{Message[SelectionSortAlgorithm::"BadElements AND/OR Bad \
Options"],2;,Abort[]}];
        
(*The Algorithm*)

i = initialMinPosition = 1;               

      
s = stepNumber =0;                            

      la = MyList;

      n = lengthOfList = Length[la];

      Do[
        s = s + 1;
        TheOldPossibleMin = Part[la, i];
        
        IndexOldPossibleMin = IndexNewPossibleMin = i;
        
        Do[
          If[
            Part[la, j] < TheOldPossibleMin,
            TheNewPossibleMin = Part[la, j];
            TheOldPossibleMin = TheNewPossibleMin;
            IndexNewPossibleMin = j;
            ], {j, i + 1, n}
          ];
        SwapFunction[la,
          IndexOldPossibleMin,
          IndexNewPossibleMin];
        las[s] = la;
        smax = s;, {i, 1, n - 1}
        ];
      
                                                  
                                                                       
 (*Here I Deside What To do*)
 
      
        testPrint = {ToString[(ToPrint /. {opts} /. 
                Options[SelectionSortAlgorithm])], 
          Hold[Do[Print[las[s]], {s, 1, smax}]]};

      testBox = {ToString[(GridArray /. {opts} /. 
                Options[SelectionSortAlgorithm])], 
          Hold[DisplayForm[
              GridBox[Table[las[s], {s, 0, smax}], GridFrame -> 2, 
                RowLines -> 2, ColumnLines -> 2]]]};

      testStep = {ToString[(Steps /. {opts} /. 
                Options[SelectionSortAlgorithm])], 
          Hold[Do[Print[{las[s], {s}}], {s, 1, smax}]]};
      
                                                                
(*Possibles Result*)
      
      
Which[Part[testPrint, 1] == "Yes", ReleaseHold[Part[testPrint, 2]],True,1];
        Which[Part[testStep, 1] == "Yes", ReleaseHold[Part[testStep, \
2]],True,1];
        
        Print["AllSteps=" <> ToString[smax]] ;
      
      Which[Part[testBox, 1] == "Yes", ReleaseHold[Part[testBox, \
2]],True,{la, {smax}}]
      ]
    )

End[]

Protect["PaulsTools`SelectionSortAlgorithm`*"]

EndPackage[]






