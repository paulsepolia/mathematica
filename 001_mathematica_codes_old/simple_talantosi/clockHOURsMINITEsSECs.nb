(************** Content-type: application/mathematica **************
                     CreatedBy='Mathematica 4.2'

                    Mathematica-Compatible Notebook

This notebook can be used with any Mathematica-compatible
application, such as Mathematica, MathReader or Publicon. The data
for the notebook starts with the line containing stars above.

To get the notebook into a Mathematica-compatible application, do
one of the following:

* Save the data starting with the line of stars above into a file
  with a name ending in .nb, then open the file inside the
  application;

* Copy the data starting with the line of stars above to the
  clipboard, then use the Paste menu command inside the application.

Data for notebooks contains only printable 7-bit ASCII and can be
sent directly in email or through ftp in text mode.  Newlines can be
CR, LF or CRLF (Unix, Macintosh or MS-DOS style).

NOTE: If you modify the data for this notebook not in a Mathematica-
compatible application, you must delete the line below containing
the word CacheID, otherwise Mathematica-compatible applications may
try to use invalid cache data.

For more information on notebooks and Mathematica-compatible 
applications, contact Wolfram Research:
  web: http://www.wolfram.com
  email: info@wolfram.com
  phone: +1-217-398-0700 (U.S.)

Notebook reader applications are available free of charge from 
Wolfram Research.
*******************************************************************)

(*CacheID: 232*)


(*NotebookFileLineBreakTest
NotebookFileLineBreakTest*)
(*NotebookOptionsPosition[     30903,        501]*)
(*NotebookOutlinePosition[     31586,        524]*)
(*  CellTagsIndexPosition[     31542,        520]*)
(*WindowFrame->Normal*)



Notebook[{
Cell[BoxData[
    \(p[t_] := 
      Show[Graphics[{Line[{{0, 0}, {0.55*Sin[2*Pi*t/12], \ 
                  0.55*Cos[\(-2\)*Pi*t/12]}}], 
            Circle[{0, 0}, 0.8], {PointSize[ .08], RGBColor[1, 0, 0], 
              Point[0.8\ {0, 1}]}, {PointSize[ .08], RGBColor[1, 0, 0], 
              Point[0.8\ \ {1\/2, \@3\/2}]}, {PointSize[ .08], 
              RGBColor[1, 0, 0], 
              Point[0.8*{\@3\/2, 1\/2}]}, {PointSize[ .08], 
              RGBColor[1, 0, 0], Point[0.8*{1, 0}]}, {PointSize[ .08], 
              RGBColor[1, 0, 0], 
              Point[0.8*{\@3\/2, \(-\(1\/2\)\)}]}, {PointSize[ .08], 
              RGBColor[1, 0, 0], 
              Point[0.8*{1\/2, \(-\(\@3\/2\)\)}]}, {PointSize[ .08], 
              RGBColor[1, 0, 0], Point[0.8*{0, \(-1\)}]}, {PointSize[ .08], 
              RGBColor[1, 0, 0], 
              Point[0.8*{\(-\(1\/2\)\), \(-\(\@3\/2\)\)}]}, {PointSize[ .08], 
              RGBColor[1, 0, 0], 
              Point[0.8*{\(-\(\@3\/2\)\), \(-\(1\/2\)\)}]}, {PointSize[ .08], 
              RGBColor[1, 0, 0], Point[0.8*{\(-1\), 0}]}, {PointSize[ .08], 
              RGBColor[1, 0, 0], 
              Point[0.8*{\(-\(\@3\/2\)\), 1\/2}]}, {PointSize[ .08], 
              RGBColor[1, 0, 0], Point[0.8*{\(-\(1\/2\)\), \@3\/2}]}, 
            Line[{{0, 0}, {0.8*Sin[2*Pi*t], \ 
                  0.8*Cos[\(-2\)*Pi*t]}}], {PointSize[ .15], 
              RGBColor[0.5, 0, 1], Point[{0, 0}]}, 
            Line[{{\(-0.2\)*Sin[2*Pi*t*60], \ \(-0.2\)*
                    Cos[\(-2\)*Pi*t*60]}, {1.1*Sin[2*Pi*t*60], \ 
                  1.1*Cos[\(-2\)*Pi*t*60]}}], {PointSize[ .07], 
              RGBColor[1, 1, 0], Point[{0, 0}]}}, 
          PlotRange \[Rule] {{\(-1.1\), 1.1}, {\(-1.1\), 1.1}}, 
          AspectRatio \[Rule] Automatic, 
          Prolog \[Rule] {Thickness[0.02], RGBColor[0.5, 0.8, 0.4]}, 
          Axes \[Rule] False, ImageSize \[Rule] {500, 500}]]\)], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
    \(p[0.5]\)], "Input"],

Cell[GraphicsData["PostScript", "\<\
%!
%%Creator: Mathematica
%%AspectRatio: 1 
%%ImageSize: 500 500 
MathPictureStart
/Mabs {
Mgmatrix idtransform
Mtmatrix dtransform
} bind def
/Mabsadd { Mabs
3 -1 roll add
3 1 roll add
exch } bind def
%% Graphics
%%IncludeResource: font Courier
%%IncludeFont: Courier
/Courier findfont 10  scalefont  setfont
% Scaling calculations
0.5 0.454545 0.5 0.454545 [
[ 0 0 0 0 ]
[ 1 1 0 0 ]
] MathScale
% Start of Graphics
1 setlinecap
1 setlinejoin
newpath
0 0 m
1 0 L
1 1 L
0 1 L
closepath
clip
newpath
.5 .8 .4 r
.02 w
[ ] 0 setdash
.5 .5 m
.5647 .74148 L
s
newpath
.5 .5 .36364 0 365.73 arc
s
1 0 0 r
.08 w
.5 .86364 Mdot
.68182 .81492 Mdot
.81492 .68182 Mdot
.86364 .5 Mdot
.81492 .31818 Mdot
.68182 .18508 Mdot
.5 .13636 Mdot
.31818 .18508 Mdot
.18508 .31818 Mdot
.13636 .5 Mdot
.18508 .68182 Mdot
.31818 .81492 Mdot
.5 .8 .4 r
.02 w
.5 .5 m
.5 .13636 L
s
.5 0 1 r
.15 w
.5 .5 Mdot
.5 .8 .4 r
.02 w
.5 .40909 m
.5 1 L
s
1 1 0 r
.07 w
.5 .5 Mdot
% End of Graphics
MathPictureEnd
\
\>"], "Graphics",
  ImageSize->{500, 500},
  ImageCache->GraphicsData["Bitmap", "\<\
CF5dJ6E]HGAYHf4PAg9QL6QYHg<PAVmbKF5d0`40007d0001m2000`400?l00000o`00003oo`3ooooe
0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe
0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe
0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe
0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe
0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe
0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe
0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe
0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe
0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe
0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00o`3ooooe0?ooo`00m`3oool5
0?l00?P0oooo003b0?ooo`l0o`00l`3oool00?40oooo4@3o003b0?ooo`00k`3ooolE0?l00?00oooo
003^0?oooaL0o`00k`3oool00>`0oooo6`3o003]0?ooo`00j`3ooolM0?l00>`0oooo003[0?oooad0
o`00k03oool00>X0oooo7`3o003[0?ooo`00j@3ooolQ0?l00>X0oooo003Y0?ooob40o`00jP3oool0
0>P0oooo8`3o003Y0?ooo`00i`3ooolU0?l00>P0oooo003W0?ooobD0o`00j03oool00>L0oooo9@3o
003X0?ooo`00iP3oool00`1oc6H0o`000?l0000<0?l000T0OlaV3P3o003X0?ooo`00g`3oool807o<
IPh0o`002@1oc6H>0?l000L0OlaVh@3oool00=T0oooo3@1oc6H?0?l000T0OlaV3`3o000=07o<I]X0
oooo003D0?oooa80OlaV3`3o000907o<IPl0o`004@1oc6KF0?ooo`00d03ooolF07o<IPl0o`002@1o
c6H?0?l001D0OlaVdP3oool00</0oooo6`1oc6H?0?l000T0OlaV3`3o000J07o<I/d0oooo00370?oo
oal0OlaV3`3o000907o<IPl0o`007P1oc6K90?ooo`00a@3ooolR07o<IPh0o`002@1oc6H>0?l00240
OlaVa`3oool00<00oooo9`1oc6H>0?l000T0OlaV3P3o000V07o<I/80oooo002L0?ooo`D0o`007@3o
oolX07o<IP030?ooo`3o0000o`0000`0o`002@1oc6H>0?l002P0OlaV7@3oool50?l009h0oooo002G
0?ooo`l0o`00503ooolU07o<IPP0oooo3P3o000907o<IPh0o`001`3ooolS07o<IQH0oooo3`3o002I
0?ooo`00UP3ooolA0?l00140oooo8@1oc6H>0?ooo`h0o`002@1oc6H>0?l000h0oooo801oc6HA0?oo
oa40o`00V03oool009@0oooo5@3o000=0?oooah0OlaV503oool=0?l000T0OlaV3@3o000C0?oooah0
OlaV3@3ooolE0?l009H0oooo002C0?oooaL0o`002P3ooolL07o<IQT0oooo303o000907o<IP`0o`00
6@3ooolK07o<IPX0oooo5`3o002E0?ooo`00T@3ooolK0?l000H0oooo6@1oc6HN0?ooo``0o`002@1o
c6H<0?l001d0oooo6P1oc6H50?oooa/0o`00T`3oool00900oooo7@3o00020?oooaP0OlaV8`3oool;
0?l000T0OlaV2`3o000R0?oooaP0OlaV0P3ooolM0?l00980oooo002@0?oooad0o`00601oc6HV0?oo
o`X0o`002@1oc6H:0?l002H0oooo5`1oc6HM0?l00980oooo002?0?oooal0o`004P1oc6H[0?ooo`X0
o`002@1oc6H:0?l002X0oooo4P1oc6HO0?l00940oooo002>0?ooob40o`003`1oc6H^0?ooo`T0o`00
2@1oc6H90?l002d0oooo3`1oc6HQ0?l00900oooo002>0?ooob40o`002`1oc6Hd0?ooo`L0o`002@1o
c6H70?l00340oooo3@1oc6HQ0?l00900oooo002=0?ooob<0o`00201oc6Hg0?ooo`H0o`002@1oc6H6
0?l003H0oooo201oc6HS0?l008l0oooo002<0?ooobD0o`001@1oc6Hk0?ooo`@0o`002@1oc6H40?l0
03X0oooo1@1oc6HU0?l008h0oooo002<0?ooobD0o`000`1oc6Hn0?ooo`<0o`002@1oc6H30?l003h0
oooo0P1oc6HU0?l008h0oooo002<0?ooobD0o`00A03oool907o<IT<0oooo9@3o002>0?ooo`00S03o
oolU0?l004@0oooo2@1oc6I30?ooobD0o`00SP3oool008`0oooo9@3o00140?ooo`T0OlaV@`3ooolU
0?l008h0oooo002;0?ooobL0o`00@`3oool907o<IT80oooo9`3o002=0?ooo`00R`3ooolW0?l004<0
oooo2@1oc6I20?ooobL0o`00S@3oool008/0oooo9`3o00130?ooo`T0OlaV@P3ooolW0?l008d0oooo
002;0?ooobL0o`00@`3oool907o<IT80oooo9`3o002=0?ooo`00R`3ooolW0?l004<0oooo2@1oc6I2
0?ooobL0o`00S@3oool008`0oooo9@3o00140?ooo`T0OlaV@`3ooolU0?l008h0oooo002<0?ooobD0
o`00A03oool907o<IT<0oooo9@3o002>0?ooo`00S03ooolU0?l004@0oooo2@1oc6I30?ooobD0o`00
SP3oool008`0oooo9@3o00140?ooo`T0OlaV@`3ooolU0?l008h0oooo002<0?ooobD0o`00A03oool9
07o<IT<0oooo9@3o002>0?ooo`00RP3oool307o<IR<0o`00A@3oool907o<IT@0oooo8`3o000307o<
IX`0oooo00280?ooo`H0OlaV8@3o00160?ooo`T0OlaVA@3ooolQ0?l000H0OlaVRP3oool008P0oooo
1P1oc6HQ0?l004H0oooo2@1oc6I50?ooob40o`001`1oc6J90?ooo`00Q`3oool807o<IQl0o`00A`3o
ool907o<ITH0oooo7`3o000807o<IXT0oooo00250?ooo`/0OlaV7@3o00180?ooo`T0OlaVA`3ooolM
0?l000/0OlaVQ`3oool008@0oooo301oc6HM0?l004P0oooo2@1oc6I70?oooad0o`00301oc6J60?oo
o`00P`3oool=07o<IP030?ooo`3o0000o`0001T0o`00B@3oool907o<ITP0oooo6`3o00000`3oool0
OlaV07o<IP0;07o<IXD0oooo00210?ooo`d0OlaV1@3ooolG0?l004/0oooo2@1oc6I:0?oooaL0o`00
1@3oool=07o<IX<0oooo00200?ooo`h0OlaV1P3ooolE0?l004`0oooo2@1oc6I;0?oooaD0o`001`3o
ool=07o<IX80oooo001o0?ooo`h0OlaV2@3ooolA0?l004h0oooo2@1oc6I=0?oooa40o`002@3oool>
07o<IX40oooo001n0?ooo`d0OlaV303oool?0?l004l0oooo2@1oc6I>0?ooo`l0o`00303oool=07o<
IX00oooo001l0?ooo`h0OlaV4P3oool50?l005@0oooo2@1oc6IC0?ooo`D0o`004P3oool>07o<IWh0
oooo001l0?ooo`d0OlaVK03oool907o<IV/0oooo3P1oc6Im0?ooo`00N`3oool<07o<IVh0oooo2@1o
c6I]0?ooo``0OlaVO@3oool007X0oooo301oc6I_0?ooo`T0OlaVKP3oool<07o<IW`0oooo001i0?oo
o``0OlaVL03oool907o<IVl0oooo301oc6Ik0?ooo`00M`3oool=07o<IW40oooo2@1oc6I`0?ooo`d0
OlaVN@3oool007L0oooo2`1oc6Ic0?ooo`T0OlaVLP3oool<07o<IWP0oooo001f0?ooo``0OlaVL`3o
ool907o<IW<0oooo2`1oc6Ih0?ooo`00M03oool=07o<IW@0oooo2@1oc6Ic0?ooo`d0OlaVMP3oool0
07@0oooo301oc6Ie0?ooo`T0OlaVM03oool=07o<IWD0oooo001c0?ooo``0OlaVMP3oool907o<IWD0
oooo301oc6Ie0?ooo`00LP3oool;07o<IWP0oooo2@1oc6Ig0?ooo`/0OlaVM03oool00740oooo301o
c6Ih0?ooo`T0OlaVN03oool;07o<IW<0oooo001_0?ooo`d0OlaVN@3oool907o<IWP0oooo3@1oc6Ia
0?ooo`00K`3oool;07o<IW/0oooo2@1oc6Ij0?ooo``0OlaVL03oool006h0oooo301oc6Ik0?ooo`T0
OlaVN`3oool;07o<IW00oooo001]0?ooo``0OlaVO03oool907o<IW/0oooo301oc6I_0?ooo`00K03o
ool<07o<IWd0oooo2@1oc6Il0?ooo``0OlaVKP3oool006/0oooo301oc6In0?ooo`T0OlaVO@3oool<
07o<IVd0oooo001[0?ooo`X0OlaVP03oool907o<IWl0oooo2`1oc6I/0?ooo`00JP3oool;07o<IX00
oooo2@1oc6J00?ooo`X0OlaVK03oool006T0oooo2`1oc6J10?ooo`T0OlaVP03oool;07o<IV/0oooo
001X0?ooo`/0OlaVPP3oool907o<IX40oooo2`1oc6IZ0?ooo`00J03oool:07o<IX<0oooo2@1oc6J2
0?ooo`/0OlaVJ@3oool006L0oooo2P1oc6J40?ooo`T0OlaVP`3oool:07o<IVT0oooo001U0?ooo``0
OlaVQ03oool907o<IX@0oooo2`1oc6IW0?ooo`00I@3oool;07o<IXD0oooo2@1oc6J40?ooo``0OlaV
IP3oool006@0oooo2`1oc6J60?ooo`T0OlaVQ@3oool;07o<IVH0oooo001T0?ooo`X0OlaVQ`3oool9
07o<IXH0oooo2`1oc6IU0?ooo`00F@3oool50?l000D0oooo2`1oc6J70?ooo`T0OlaVQ`3oool:07o<
IPD0oooo1@3o001K0?ooo`00E03oool?0?l000X0OlaVR03oool907o<IXL0oooo2P1oc6H?0?l005H0
oooo001C0?oooa40o`001`1oc6J:0?ooo`T0OlaVR@3oool707o<IQ40o`00E@3oool00540oooo5@3o
000507o<IXX0oooo2@1oc6J:0?ooo`@0OlaV5@3o001C0?ooo`00D03ooolG0?l000<0OlaVR`3oool9
07o<IXX0oooo0`1oc6HG0?l00580oooo001>0?oooa/0o`0000<0OlaV0?ooo`3oool0R@3oool907o<
IX/0oooo6`3o001@0?ooo`00C@3ooolM0?l008/0oooo2@1oc6J:0?oooad0o`00C`3oool004d0oooo
7@3o002;0?ooo`T0OlaVRP3ooolM0?l004l0oooo001<0?oooal0o`00RP3oool907o<IXT0oooo7`3o
001>0?ooo`00B`3ooolQ0?l008T0oooo2@1oc6J80?ooob40o`00C@3oool004/0oooo8@3o00290?oo
o`T0OlaVR03ooolQ0?l004d0oooo001:0?ooob<0o`00R03oool907o<IXL0oooo8`3o001<0?ooo`00
B@3ooolU0?l008L0oooo2@1oc6J60?ooobD0o`00B`3oool004T0oooo9@3o00270?ooo`T0OlaVQP3o
oolU0?l004/0oooo00190?ooobD0o`00Q`3oool907o<IXH0oooo9@3o001;0?ooo`00B@3ooolU0?l0
08L0oooo2@1oc6J60?ooobD0o`00B`3oool004T0oooo9@3o00270?ooo`T0OlaVQP3ooolU0?l004/0
oooo00180?ooobL0o`00QP3oool907o<IXD0oooo9`3o001:0?ooo`00B03ooolW0?l008H0oooo2@1o
c6J50?ooobL0o`00BP3oool004P0oooo9`3o00260?ooo`T0OlaVQ@3ooolW0?l004X0oooo00180?oo
obL0o`00QP3oool907o<IXD0oooo9`3o001:0?ooo`00B03ooolW0?l008H0oooo2@1oc6J50?ooobL0
o`00BP3oool004T0oooo9@3o00270?ooo`T0OlaVQP3ooolU0?l004/0oooo00190?ooobD0o`00Q`3o
ool907o<IXH0oooo9@3o001;0?ooo`00B@3ooolU0?l008L0oooo2@1oc6J60?ooobD0o`00B`3oool0
04T0oooo9@3o00270?ooo`T0OlaVQP3ooolU0?l004/0oooo00190?ooobD0o`00Q`3oool907o<IXH0
oooo9@3o001;0?ooo`00BP3ooolS0?l008P0oooo2@1oc6J70?ooob<0o`00C03oool004/0oooo8@3o
00290?ooo`T0OlaVR03ooolQ0?l004d0oooo001;0?ooob40o`00R@3oool907o<IXP0oooo8@3o001=
0?ooo`00C03ooolO0?l008X0oooo2@1oc6J90?oooal0o`00CP3oool004d0oooo7@3o002;0?ooo`T0
OlaVRP3ooolM0?l004l0oooo001=0?oooad0o`00R`3oool907o<IXX0oooo7@3o001?0?ooo`00CP3o
oolK0?l008`0oooo2@1oc6J;0?oooa/0o`00D03oool00500oooo5`3o002>0?ooo`T0OlaVS@3ooolG
0?l0000307o<IP3oool0oooo04l0oooo001?0?ooo`80OlaV5@3o002?0?ooo`T0OlaVSP3ooolE0?l0
0080OlaVD@3oool004l0oooo101oc6HA0?l00940oooo2@1oc6J@0?oooa40o`001@1oc6I@0?ooo`00
CP3oool607o<IPl0o`00TP3oool907o<IY40oooo3`3o000607o<IU00oooo001>0?ooo`T0OlaV0P3o
ool50?l009L0oooo2@1oc6JF0?ooo`D0o`000`3oool807o<IU00oooo001>0?ooo`P0OlaVW`3oool9
07o<IYh0oooo2@1oc6I?0?ooo`00C@3oool907o<IYl0oooo2@1oc6JN0?ooo`T0OlaVC`3oool004d0
oooo2@1oc6JO0?ooo`T0OlaVW`3oool907o<ITh0oooo001<0?ooo`T0OlaVX03oool907o<IYl0oooo
2@1oc6I>0?ooo`00C03oool907o<IZ00oooo2@1oc6JP0?ooo`T0OlaVC@3oool004/0oooo2@1oc6JQ
0?ooo`T0OlaVX03oool907o<ITd0oooo001;0?ooo`T0OlaVX@3oool907o<IZ40oooo2@1oc6I<0?oo
o`00BP3oool907o<IZ80oooo2@1oc6JQ0?ooo`T0OlaVC03oool004X0oooo2@1oc6JR0?ooo`T0OlaV
XP3oool907o<IT/0oooo001:0?ooo`T0OlaVXP3oool907o<IZ80oooo2@1oc6I;0?ooo`00BP3oool9
07o<IZ80oooo2@1oc6JR0?ooo`T0OlaVB`3oool004T0oooo2@1oc6JS0?ooo`T0OlaVXP3oool907o<
IT/0oooo00190?ooo`T0OlaVX`3oool907o<IZ<0oooo2@1oc6I:0?ooo`00B03oool907o<IZ@0oooo
2@1oc6JS0?ooo`T0OlaVBP3oool004P0oooo2@1oc6JT0?ooo`T0OlaVY03oool907o<ITT0oooo0018
0?ooo`T0OlaVY03oool907o<IZ@0oooo2@1oc6I90?ooo`00B03oool907o<IZ@0oooo2@1oc6JT0?oo
o`T0OlaVB@3oool004P0oooo2@1oc6JT0?ooo`T0OlaVY03oool907o<ITT0oooo00170?ooo`T0OlaV
Y@3oool907o<IZ@0oooo2@1oc6I90?ooo`00A`3oool907o<IZD0oooo2@1oc6JU0?ooo`T0OlaVB03o
ool004H0oooo2@1oc6JV0?ooo`T0OlaVY@3oool907o<ITP0oooo00160?ooo`T0OlaVYP3oool907o<
IZH0oooo2@1oc6I70?ooo`00AP3oool907o<IZH0oooo2@1oc6JV0?ooo`T0OlaVA`3oool004H0oooo
2@1oc6JV0?ooo`T0OlaVYP3oool907o<ITL0oooo00150?ooo`T0OlaVY`3oool907o<IZH0oooo2@1o
c6I70?ooo`00A@3oool907o<IZL0oooo2@1oc6JW0?ooo`T0OlaVAP3oool004D0oooo2@1oc6JW0?oo
o`T0OlaVY`3oool907o<ITH0oooo00150?ooo`T0OlaVY`3oool907o<IZL0oooo2@1oc6I60?ooo`00
A@3oool907o<IZL0oooo2@1oc6JW0?ooo`T0OlaVAP3oool004@0oooo2@1oc6JX0?ooo`T0OlaVZ03o
ool907o<ITD0oooo00140?ooo`T0OlaVZ03oool907o<IZP0oooo2@1oc6I50?ooo`00A03oool907o<
IZP0oooo2@1oc6JX0?ooo`T0OlaVA@3oool004@0oooo2@1oc6JX0?ooo`T0OlaVZ03oool907o<ITD0
oooo00130?ooo`T0OlaVZ@3oool907o<IZP0oooo2@1oc6I50?ooo`00@`3oool907o<IZP0oooo00<0
O`3o07o<IP1oc6H01`1oc6JY0?ooo`T0OlaVA03oool004<0oooo2@1oc6JS0?ooo`H0O`3o2@1oc6H5
07l0oj@0oooo2@1oc6I40?ooo`00@`3oool907o<IZ00oooo2@1o0?l907o<IPP0O`3oX@3oool907o<
IT@0oooo00120?ooo`T0OlaVW`3oool;07l0o`T0OlaV2P1o0?nO0?ooo`T0OlaVA03oool00480oooo
2@1oc6JM0?ooo`d0O`3o2@1oc6H<07l0oih0oooo2@1oc6I30?ooo`00@P3oool907o<IY/0oooo3`1o
0?l907o<IPh0O`3oW03oool907o<IT<0oooo00120?ooo`T0OlaVV@3ooolA07l0o`T0OlaV401o0?nJ
0?ooo`T0OlaV@`3oool00480oooo2@1oc6JH0?oooa80O`3o2@1oc6HA07l0oiT0oooo2@1oc6I30?oo
o`00@P3oool907o<IYL0oooo4`1o0?l907o<IQ80O`3oV03oool907o<IT<0oooo00120?ooo`T0OlaV
UP3ooolD07l0o`T0OlaV4`1o0?nG0?ooo`T0OlaV@`3oool00440oooo2@1oc6JE0?oooaH0O`3o2@1o
c6HE07l0oiD0oooo2@1oc6I30?ooo`00@@3oool907o<IY@0oooo5`1o0?l907o<IQH0O`3oU@3oool9
07o<IT80oooo00110?ooo`T0OlaVU03ooolG07l0o`T0OlaV5P1o0?nE0?ooo`T0OlaV@P3oool00440
oooo2@1oc6JC0?oooaP0O`3o2@1oc6HG07l0oi@0oooo2@1oc6I20?ooo`00@@3oool907o<IY80oooo
6@1o0?l907o<IQP0O`3oT`3oool907o<IT80oooo00110?ooo`T0OlaVT@3ooolJ07l0o`T0OlaV6@1o
0?nB0?ooo`T0OlaV@P3oool00440oooo2@1oc6J@0?oooa/0O`3o2@1oc6HJ07l0oi40oooo2@1oc6I2
0?ooo`00@03oool00`1oc6H0o`000?l000030?l000<0OlaVT@3ooolK07l0o`T0OlaV6P1o0?nA0?oo
o`<0OlaV1@3o00000`1oc6H0oooo0?ooo`100?ooo`00?03oool?0?l008h0oooo701o0?l907o<IQ/0
O`3oSP3oool?0?l003h0oooo000k0?oooa40o`00S@3ooolL07l0o`T0OlaV6`1o0?n=0?oooa40o`00
?@3oool003T0oooo5@3o002:0?oooad0O`3o0P1oc6H40?oo00<0OlaV701o0?n:0?oooaD0o`00>`3o
ool003P0oooo5`3o00290?oooaX0O`3o3P3oo`0J07l0ohT0oooo5`3o000j0?ooo`00=P3ooolK0?l0
08H0oooo6P1o0?l@0?oo01X0O`3oQP3ooolK0?l003P0oooo000e0?oooad0o`00Q@3ooolH07l0oa@0
ool0601o0?n50?oooad0o`00=`3oool003D0oooo7@3o00240?oooaP0O`3o5P3oo`0H07l0oh@0oooo
7@3o000g0?ooo`00=03ooolO0?l008<0oooo5`1o0?lH0?oo01L0O`3oP`3ooolO0?l003H0oooo000c
0?ooob40o`00PP3ooolF07l0oaX0ool05P1o0?n20?ooob40o`00=@3oool003<0oooo8@3o00210?oo
oaH0O`3o703oo`0F07l0oh40oooo8@3o000e0?ooo`00<P3ooolS0?l00800oooo5P1o0?lL0?oo01H0
O`3oP03ooolS0?l003@0oooo000a0?ooobD0o`00O`3ooolE07l0oah0ool05@1o0?mo0?ooobD0o`00
<`3oool00340oooo9@3o001o0?oooa@0O`3o803oo`0D07l0ogl0oooo9@3o000c0?ooo`00<@3ooolU
0?l007l0oooo501o0?lP0?oo01@0O`3oO`3ooolU0?l003<0oooo000a0?ooobD0o`00OP3ooolE07l0
ob00ool05@1o0?mn0?ooobD0o`00<`3oool00340oooo9@3o001n0?oooaD0O`3o803oo`0E07l0ogh0
oooo9@3o000c0?ooo`00<03ooolW0?l007d0oooo5@1o0?lP0?oo01D0O`3oO@3ooolW0?l00380oooo
000`0?ooobL0o`00O@3ooolD07l0ob80ool0501o0?mm0?ooobL0o`00<P3oool00300oooo9`3o001m
0?oooa@0O`3o8P3oo`0D07l0ogd0oooo9`3o000b0?ooo`00<03ooolW0?l007d0oooo501o0?lR0?oo
01@0O`3oO@3ooolW0?l00380oooo000`0?ooobL0o`00O@3ooolD07l0ob80ool0501o0?mm0?ooobL0
o`00<P3oool00340oooo9@3o001n0?oooaD0O`3o803oo`0E07l0ogh0oooo9@3o000c0?ooo`00<@3o
oolU0?l007h0oooo5@1o0?lP0?oo01D0O`3oOP3ooolU0?l003<0oooo000a0?ooobD0o`00OP3ooolE
07l0ob00ool05@1o0?mn0?ooobD0o`00<`3oool00340oooo9@3o001o0?oooa@0O`3o803oo`0D07l0
ogl0oooo9@3o000c0?ooo`00<@3ooolU0?l007l0oooo501o0?lP0?oo01@0O`3oO`3ooolU0?l003<0
oooo000b0?ooob<0o`00P03ooolE07l0oah0ool05@1o0?n00?ooob<0o`00=03oool003<0oooo8@3o
00210?oooaH0O`3o703oo`0F07l0oh40oooo8@3o000e0?ooo`00<`3ooolQ0?l00840oooo5P1o0?lL
0?oo01H0O`3oP@3ooolQ0?l003D0oooo000d0?oooal0o`00P`3ooolF07l0oaX0ool05P1o0?n30?oo
oal0o`00=P3oool003D0oooo7@3o00240?oooaL0O`3o603oo`0G07l0oh@0oooo7@3o000g0?ooo`00
=@3ooolM0?l008@0oooo601o0?lF0?oo01P0O`3oQ03ooolM0?l003L0oooo000f0?oooa/0o`00QP3o
oolH07l0oa@0ool0601o0?n60?oooa/0o`00>03oool003P0oooo5`3o00280?oooaX0O`3o403oo`0J
07l0ohP0oooo5`3o000j0?ooo`00>@3ooolE0?l008X0oooo6P1o0?l>0?oo01X0O`3oRP3ooolE0?l0
03/0oooo000k0?oooa40o`00S03ooolM07l0o`80OlaV103oo`0307o<IQ`0O`3oS03ooolA0?l003d0
oooo000l0?ooo`l0o`00SP3ooolL07l0o`T0OlaV6`1o0?n>0?ooo`l0o`00?P3oool00400oooo00<0
OlaV0?l0003o00000`3o000307o<IY00oooo701o0?l907o<IQ/0O`3oT03oool307o<IPD0o`000P1o
c6I10?ooo`00@@3oool907o<IY00oooo6`1o0?l907o<IQX0O`3oT@3oool:07o<IT40oooo00110?oo
o`T0OlaVT03ooolK07l0o`T0OlaV6P1o0?nA0?ooo`X0OlaV@@3oool00440oooo2@1oc6JA0?oooaX0
O`3o2@1oc6HI07l0oi80oooo2P1oc6I10?ooo`00@@3oool907o<IY80oooo6@1o0?l907o<IQP0O`3o
T`3oool907o<IT80oooo00110?ooo`T0OlaVT`3ooolH07l0o`T0OlaV5`1o0?nD0?ooo`T0OlaV@P3o
ool00440oooo2@1oc6JD0?oooaL0O`3o2@1oc6HF07l0oiD0oooo2@1oc6I20?ooo`00@@3oool907o<
IY@0oooo5`1o0?l907o<IQH0O`3oU03oool907o<IT<0oooo00120?ooo`T0OlaVU03ooolF07l0o`T0
OlaV5@1o0?nE0?ooo`T0OlaV@`3oool00480oooo2@1oc6JF0?oooa@0O`3o2@1oc6HC07l0oiL0oooo
2@1oc6I30?ooo`00@P3oool907o<IYL0oooo4`1o0?l907o<IQ80O`3oV03oool907o<IT<0oooo0012
0?ooo`T0OlaVV03ooolB07l0o`T0OlaV4@1o0?nI0?ooo`T0OlaV@`3oool00480oooo2@1oc6JI0?oo
oa40O`3o2@1oc6H@07l0oiX0oooo2@1oc6I30?ooo`00@P3oool907o<IY/0oooo3`1o0?l907o<IPh0
O`3oW03oool907o<IT<0oooo00130?ooo`T0OlaVW03oool=07l0o`T0OlaV301o0?nM0?ooo`T0OlaV
A03oool004<0oooo2@1oc6JN0?ooo`/0O`3o2@1oc6H:07l0oil0oooo2@1oc6I40?ooo`00@`3oool9
07o<IZ00oooo2@1o0?l907o<IPP0O`3o00<0OlaV0?ooo`3oool0WP3oool907o<IT@0oooo00130?oo
o`T0OlaVX`3oool607l0o`T0OlaV1@1o0?l407o<IZ00oooo2@1oc6I40?ooo`00@`3oool907o<IZP0
oooo00<0O`3o07o<IP1oc6H01`1oc6H00`3oool0OlaV07o<IP0707o<IYh0oooo2@1oc6I50?ooo`00
A03oool907o<IZP0oooo2@1oc6H00`3oool0OlaV07o<IP0707o<IYh0oooo2@1oc6I50?ooo`00A03o
ool907o<IZP0oooo2@1oc6H20?ooo`P0OlaVWP3oool907o<ITD0oooo00140?ooo`T0OlaVZ03oool9
07o<IP80oooo201oc6JN0?ooo`T0OlaVA@3oool004@0oooo2@1oc6JX0?ooo`T0OlaV0P3oool907o<
IYd0oooo2@1oc6I50?ooo`00A@3oool907o<IZL0oooo2@1oc6H20?ooo`T0OlaVW03oool907o<ITH0
oooo00150?ooo`T0OlaVY`3oool907o<IP<0oooo201oc6JL0?ooo`T0OlaVAP3oool004D0oooo2@1o
c6JW0?ooo`T0OlaV0`3oool807o<IY`0oooo2@1oc6I60?ooo`00A@3oool907o<IZL0oooo2@1oc6H3
0?ooo`T0OlaVV`3oool907o<ITH0oooo00150?ooo`T0OlaVY`3oool907o<IP<0oooo2@1oc6JJ0?oo
o`T0OlaVA`3oool004H0oooo2@1oc6JV0?ooo`T0OlaV103oool807o<IYX0oooo2@1oc6I70?ooo`00
AP3oool907o<IZH0oooo2@1oc6H40?ooo`T0OlaVV@3oool907o<ITL0oooo00160?ooo`T0OlaVYP3o
ool907o<IP@0oooo2@1oc6JI0?ooo`T0OlaVA`3oool004H0oooo2@1oc6JV0?ooo`T0OlaV1@3oool8
07o<IYP0oooo2@1oc6I80?ooo`00A`3oool907o<IZD0oooo2@1oc6H50?ooo`P0OlaVV03oool907o<
ITP0oooo00170?ooo`T0OlaVY@3oool907o<IPD0oooo2@1oc6JF0?ooo`T0OlaVB@3oool004P0oooo
2@1oc6JT0?ooo`T0OlaV1@3oool907o<IYH0oooo2@1oc6I90?ooo`00B03oool907o<IZ@0oooo2@1o
c6H60?ooo`P0OlaVUP3oool907o<ITT0oooo00180?ooo`T0OlaVY03oool907o<IPH0oooo201oc6JF
0?ooo`T0OlaVB@3oool004P0oooo2@1oc6JT0?ooo`T0OlaV1P3oool907o<IYD0oooo2@1oc6I90?oo
o`00B03oool907o<IZ@0oooo2@1oc6H60?ooo`T0OlaVU03oool907o<ITX0oooo00190?ooo`T0OlaV
X`3oool907o<IPL0oooo201oc6JD0?ooo`T0OlaVBP3oool004T0oooo2@1oc6JS0?ooo`T0OlaV1`3o
ool807o<IY<0oooo2@1oc6I;0?ooo`00BP3oool907o<IZ80oooo2@1oc6H70?ooo`T0OlaVTP3oool9
07o<IT/0oooo001:0?ooo`T0OlaVXP3oool907o<IPL0oooo2@1oc6JB0?ooo`T0OlaVB`3oool004X0
oooo2@1oc6JR0?ooo`T0OlaV203oool807o<IY80oooo2@1oc6I;0?ooo`00BP3oool907o<IZ80oooo
2@1oc6H80?ooo`T0OlaVT03oool907o<IT`0oooo001;0?ooo`T0OlaVX@3oool907o<IPP0oooo2@1o
c6J@0?ooo`T0OlaVC03oool004/0oooo2@1oc6JQ0?ooo`T0OlaV2@3oool807o<IXl0oooo2@1oc6I=
0?ooo`00C03oool907o<IZ00oooo2@1oc6H90?ooo`P0OlaVS`3oool907o<ITd0oooo001<0?ooo`T0
OlaVX03oool907o<IPT0oooo2@1oc6J=0?ooo`T0OlaVCP3oool004d0oooo2@1oc6JO0?ooo`T0OlaV
2@3oool907o<IXd0oooo2@1oc6I>0?ooo`00C@3oool907o<IYl0oooo2@1oc6H:0?ooo`P0OlaVS03o
ool907o<ITl0oooo001>0?ooo`T0OlaVWP3oool907o<IPX0oooo201oc6J<0?ooo`T0OlaVC`3oool0
04h0oooo2@1oc6JN0?ooo`T0OlaV2P3oool907o<IX/0oooo201oc6I@0?ooo`00C`3oool807o<IP80
oooo1@3o002G0?ooo`T0OlaV2P3oool907o<IX<0oooo1@3o00020?ooo`T0OlaVD03oool004l0oooo
1@1oc6H?0?l00980oooo2@1oc6H;0?ooo`P0OlaVOP3oool?0?l000H0OlaVD03oool004l0oooo101o
c6HA0?l00940oooo2@1oc6H;0?ooo`T0OlaVO03ooolA0?l000@0OlaVD@3oool00500oooo00<0OlaV
0?l0003o00004`3o002?0?ooo`T0OlaV2`3oool907o<IWX0oooo5@3o000207o<IU40oooo001@0?oo
oaL0o`00SP3oool907o<IP`0oooo201oc6Ii0?oooaL0o`00DP3oool004h0oooo6`3o002<0?ooo`T0
OlaV303oool807o<IWL0oooo6`3o001@0?ooo`00C@3ooolM0?l008/0oooo2@1oc6H<0?ooo`T0OlaV
M@3ooolM0?l004l0oooo001=0?oooad0o`00R`3oool907o<IP`0oooo2@1oc6Ie0?oooad0o`00C`3o
ool004`0oooo7`3o002:0?ooo`T0OlaV3@3oool807o<IW@0oooo7`3o001>0?ooo`00B`3ooolQ0?l0
08T0oooo2@1oc6H=0?ooo`P0OlaVL`3ooolQ0?l004d0oooo001;0?ooob40o`00R@3oool907o<IPd0
oooo2@1oc6Ib0?ooob40o`00C@3oool004X0oooo8`3o00280?ooo`T0OlaV3@3oool907o<IW40oooo
8`3o001<0?ooo`00B@3ooolU0?l008L0oooo2@1oc6H>0?ooo`P0OlaVL03ooolU0?l004/0oooo0019
0?ooobD0o`00Q`3oool907o<IPh0oooo201oc6I`0?ooobD0o`00B`3oool004T0oooo9@3o00270?oo
o`T0OlaV3P3oool907o<IVl0oooo9@3o001;0?ooo`00B@3ooolU0?l008L0oooo2@1oc6H>0?ooo`T0
OlaVK`3ooolU0?l004/0oooo00190?ooobD0o`00Q`3oool907o<IPl0oooo201oc6I_0?ooobD0o`00
B`3oool004P0oooo9`3o00260?ooo`T0OlaV3`3oool907o<IVd0oooo9`3o001:0?ooo`00B03ooolW
0?l008H0oooo2@1oc6H?0?ooo`T0OlaVK@3ooolW0?l004X0oooo00180?ooobL0o`00QP3oool907o<
IQ00oooo201oc6I]0?ooobL0o`00BP3oool004P0oooo9`3o00260?ooo`T0OlaV403oool807o<IVd0
oooo9`3o001:0?ooo`00B03ooolW0?l008H0oooo2@1oc6H@0?ooo`T0OlaVK03ooolW0?l004X0oooo
00190?ooobD0o`00Q`3oool907o<IQ00oooo2@1oc6I]0?ooobD0o`00B`3oool004T0oooo9@3o0027
0?ooo`T0OlaV4@3oool807o<IVd0oooo9@3o001;0?ooo`00B@3ooolU0?l008L0oooo2@1oc6HA0?oo
o`P0OlaVK@3ooolU0?l004/0oooo00190?ooobD0o`00Q`3oool907o<IQ40oooo2@1oc6I/0?ooobD0
o`00B`3oool004T0oooo9@3o00270?ooo`T0OlaV4@3oool907o<IV`0oooo9@3o001;0?ooo`00BP3o
oolS0?l008P0oooo2@1oc6HB0?ooo`P0OlaVK@3ooolS0?l004`0oooo001;0?ooob40o`00R@3oool9
07o<IQ80oooo201oc6I^0?ooob40o`00C@3oool004/0oooo8@3o00290?ooo`T0OlaV4P3oool907o<
IVd0oooo8@3o001=0?ooo`00C03ooolO0?l008X0oooo2@1oc6HB0?ooo`T0OlaVKP3ooolO0?l004h0
oooo001=0?oooad0o`00R`3oool907o<IQ<0oooo201oc6I_0?oooad0o`00C`3oool004d0oooo7@3o
002;0?ooo`T0OlaV4`3oool907o<IVh0oooo7@3o001?0?ooo`00CP3ooolK0?l0000307o<IP3oool0
oooo08T0oooo2@1oc6HC0?ooo`T0OlaVKP3oool00`1oc6H0o`000?l0000I0?l00500oooo001@0?oo
oaL0o`00101oc6J:0?ooo`T0OlaV503oool807o<IVh0oooo0`1oc6HG0?l00580oooo001A0?oooaD0
o`001@1oc6J:0?ooo`T0OlaV503oool807o<IVd0oooo1@1oc6HE0?l005<0oooo001C0?oooa40o`00
2@1oc6J80?ooo`T0OlaV503oool907o<IVX0oooo2@1oc6HA0?l005D0oooo001D0?ooo`l0o`002`1o
c6J70?ooo`T0OlaV503oool907o<IVX0oooo2P1oc6H?0?l005H0oooo001I0?ooo`D0o`001P3oool:
07o<IXL0oooo2@1oc6HE0?ooo`P0OlaVJ@3oool;07o<IPD0oooo1@3o001K0?ooo`00I03oool;07o<
IXH0oooo2@1oc6HE0?ooo`P0OlaVJ03oool;07o<IVH0oooo001U0?ooo`/0OlaVQ@3oool907o<IQD0
oooo2@1oc6IV0?ooo``0OlaVIP3oool006D0oooo301oc6J40?ooo`T0OlaV5@3oool907o<IVH0oooo
2`1oc6IW0?ooo`00I`3oool:07o<IX@0oooo2@1oc6HF0?ooo`P0OlaVI@3oool:07o<IVT0oooo001X
0?ooo`X0OlaVP`3oool907o<IQH0oooo201oc6IT0?ooo`/0OlaVJ@3oool006P0oooo2`1oc6J20?oo
o`T0OlaV5P3oool907o<IV80oooo2`1oc6IZ0?ooo`00J@3oool;07o<IX40oooo2@1oc6HF0?ooo`T0
OlaVH@3oool;07o<IV/0oooo001Z0?ooo`/0OlaVP03oool907o<IQL0oooo201oc6IQ0?ooo`X0OlaV
K03oool006/0oooo2P1oc6J00?ooo`T0OlaV5`3oool907o<IUl0oooo2`1oc6I/0?ooo`00J`3oool<
07o<IWh0oooo2@1oc6HG0?ooo`T0OlaVG@3oool<07o<IVd0oooo001/0?ooo``0OlaVO@3oool907o<
IQP0oooo201oc6IL0?ooo``0OlaVKP3oool006d0oooo301oc6Il0?ooo`T0OlaV603oool807o<IU/0
oooo301oc6I_0?ooo`00KP3oool<07o<IW/0oooo2@1oc6HH0?ooo`T0OlaVFP3oool;07o<IW00oooo
001_0?ooo`/0OlaVN`3oool907o<IQP0oooo1P1oc6IL0?ooo``0OlaVL03oool006l0oooo3@1oc6Ii
0?ooo`T0OlaV6@3oool00`1oc6H0oooo0?ooo`1L0?ooo`d0OlaVL@3oool00740oooo301oc6Ih0?oo
o`T0OlaVN03oool;07o<IW<0oooo001b0?ooo`/0OlaVN03oool907o<IWL0oooo2`1oc6Id0?ooo`00
L`3oool<07o<IWH0oooo2@1oc6Ie0?ooo``0OlaVM@3oool007@0oooo301oc6Ie0?ooo`T0OlaVM03o
ool=07o<IWD0oooo001d0?ooo`d0OlaVM03oool907o<IW<0oooo3@1oc6If0?ooo`00MP3oool<07o<
IW<0oooo2@1oc6Ic0?ooo`/0OlaVN03oool007L0oooo2`1oc6Ic0?ooo`T0OlaVLP3oool<07o<IWP0
oooo001g0?ooo`d0OlaVL@3oool907o<IW00oooo3@1oc6Ii0?ooo`00N@3oool<07o<IW00oooo2@1o
c6I_0?ooo``0OlaVN`3oool007X0oooo301oc6I_0?ooo`T0OlaVKP3oool<07o<IW`0oooo001k0?oo
o``0OlaVKP3oool907o<IVd0oooo301oc6Im0?ooo`00O03oool=07o<IV`0oooo2@1oc6I[0?ooo`h0
OlaVO@3oool007`0oooo3P1oc6I[0?ooo`T0OlaVJP3oool>07o<IWh0oooo001n0?ooo`d0OlaV4@3o
ool50?l005@0oooo2@1oc6IC0?ooo`D0o`004@3oool=07o<IX00oooo001o0?ooo`h0OlaV2P3oool?
0?l004l0oooo2@1oc6I>0?ooo`l0o`002P3oool>07o<IX40oooo00200?ooo`h0OlaV203ooolA0?l0
04h0oooo2@1oc6I=0?oooa40o`002@3oool=07o<IX80oooo00210?ooo`d0OlaV1P3ooolE0?l004`0
oooo2@1oc6I;0?oooaD0o`001P3oool=07o<IX<0oooo00230?ooo`d0OlaV0`3ooolG0?l004/0oooo
2@1oc6I:0?oooaL0o`000`3oool=07o<IXD0oooo00240?ooo`d0OlaV6`3o00190?ooo`T0OlaVB03o
oolK0?l000d0OlaVQP3oool008D0oooo2`1oc6HM0?l004P0oooo2@1oc6I70?oooad0o`002`1oc6J7
0?ooo`00Q`3oool907o<IQd0o`00B03oool907o<ITL0oooo7@3o000907o<IXT0oooo00280?ooo`L0
OlaV7`3o00170?ooo`T0OlaVAP3ooolO0?l000P0OlaVR@3oool008P0oooo1P1oc6HQ0?l004H0oooo
2@1oc6I50?ooob40o`001P1oc6J:0?ooo`00RP3oool407o<IR40o`00AP3oool907o<ITD0oooo8@3o
000407o<IX`0oooo002<0?ooo`0307o<IP3o0000o`000240o`00A@3oool907o<IT@0oooo8`3o0000
0`1oc6H0oooo0?ooo`2<0?ooo`00S03ooolU0?l004@0oooo2@1oc6I30?ooobD0o`00SP3oool008`0
oooo9@3o00140?ooo`T0OlaV@`3ooolU0?l008h0oooo002<0?ooobD0o`00A03oool907o<IT<0oooo
9@3o002>0?ooo`00S03ooolU0?l004@0oooo2@1oc6I30?ooobD0o`00SP3oool008`0oooo9@3o0014
0?ooo`T0OlaV@`3ooolU0?l008h0oooo002;0?ooobL0o`00@`3oool907o<IT80oooo9`3o002=0?oo
o`00R`3ooolW0?l004<0oooo2@1oc6I20?ooobL0o`00S@3oool008/0oooo9`3o00130?ooo`T0OlaV
@P3ooolW0?l008d0oooo002;0?ooobL0o`00@`3oool907o<IT80oooo9`3o002=0?ooo`00R`3ooolW
0?l004<0oooo2@1oc6I20?ooobL0o`00S@3oool008`0oooo9@3o00140?ooo`T0OlaV@`3ooolU0?l0
08h0oooo002<0?ooobD0o`00A03oool907o<IT<0oooo9@3o002>0?ooo`00S03ooolU0?l000<0OlaV
@@3oool907o<IT00oooo0`1oc6HU0?l008h0oooo002<0?ooobD0o`001@1oc6Hl0?ooo`<0o`002@1o
c6H30?l003/0oooo1@1oc6HU0?l008h0oooo002<0?ooobD0o`001`1oc6Hi0?ooo`@0o`002@1oc6H4
0?l003P0oooo1`1oc6HU0?l008h0oooo002=0?ooob<0o`002P1oc6He0?ooo`H0o`002@1oc6H60?l0
03@0oooo2P1oc6HS0?l008l0oooo002>0?ooob40o`003`1oc6H`0?ooo`L0o`002@1oc6H70?l002l0
oooo3`1oc6HQ0?l00900oooo002>0?ooob40o`004@1oc6H/0?ooo`T0o`002@1oc6H90?l002/0oooo
4@1oc6HQ0?l00900oooo002?0?oooal0o`005`1oc6HV0?ooo`X0o`002@1oc6H:0?l002D0oooo5`1o
c6HO0?l00940oooo002@0?oooad0o`000P3ooolH07o<IR@0oooo2P3o000907o<IPX0o`008`3ooolH
07o<IP80oooo7@3o002B0?ooo`00T03ooolM0?l000D0oooo6@1oc6HO0?ooo`/0o`002@1oc6H;0?l0
01h0oooo6@1oc6H50?oooad0o`00TP3oool00940oooo6`3o00080?oooa/0OlaV6P3oool<0?l000T0
OlaV303o000I0?oooa/0OlaV203ooolK0?l009<0oooo002C0?oooaL0o`00303ooolN07o<IQD0oooo
303o000907o<IP`0o`00503ooolN07o<IP`0oooo5`3o002E0?ooo`00U03ooolE0?l000l0oooo801o
c6H@0?ooo`d0o`002@1oc6H=0?l000l0oooo801oc6H?0?oooaD0o`00UP3oool009H0oooo4@3o000C
0?ooobD0OlaV203oool>0?l000T0OlaV3P3o00070?ooobD0OlaV4`3ooolA0?l009P0oooo002G0?oo
o`l0o`00603ooolX07o<IP030?ooo`3o0000o`0000`0o`002@1oc6H>0?l002P0OlaV603oool?0?l0
09T0oooo002L0?ooo`D0o`007`3ooolW07o<IPh0o`002@1oc6H>0?l002H0OlaV7`3oool50?l009h0
oooo00350?ooob80OlaV3P3o000907o<IPh0o`008@1oc6K70?ooo`00a`3ooolP07o<IPh0o`002@1o
c6H>0?l001l0OlaVb@3oool00</0oooo6`1oc6H?0?l000T0OlaV3`3o000J07o<I/d0oooo003@0?oo
oaH0OlaV3`3o000907o<IPl0o`005P1oc6KA0?ooo`00e03ooolB07o<IPl0o`002@1oc6H?0?l00140
OlaVeP3oool00=P0oooo3P1oc6H?0?l000T0OlaV3`3o000=07o<I]X0oooo003O0?ooo`L0OlaV3`3o
000907o<IPl0o`001P1oc6KQ0?ooo`00iP3oool00`1oc6H0o`000?l0000<0?l000T0OlaV3P3o003X
0?ooo`00i`3oool>0?l000T0OlaV3P3o003X0?ooo`00i`3oool>0?l000T0OlaV3P3o003X0?ooo`00
i`3oool>0?l000T0OlaV3P3o003X0?ooo`00i`3oool>0?l000T0OlaV3P3o003X0?ooo`00j03oool=
0?l000T0OlaV3@3o003Y0?ooo`00j@3oool<0?l000T0OlaV303o003Z0?ooo`00j@3oool<0?l000T0
OlaV303o003Z0?ooo`00jP3oool;0?l000T0OlaV2`3o003[0?ooo`00j`3oool:0?l000T0OlaV2P3o
003/0?ooo`00j`3oool:0?l000T0OlaV2P3o003/0?ooo`00k03oool90?l000T0OlaV2@3o003]0?oo
o`00kP3oool70?l000T0OlaV1`3o003_0?ooo`00k`3oool60?l000T0OlaV1P3o003`0?ooo`00l@3o
ool40?l000T0OlaV103o003b0?ooo`00lP3oool30?l000T0OlaV0`3o003c0?ooo`00m@3oool907o<
I_H0oooo003e0?ooo`T0OlaVmP3oool00?D0oooo2@1oc6Kf0?ooo`00m@3oool907o<I_H0oooo003e
0?ooo`T0OlaVmP3oool00?D0oooo2@1oc6Kf0?ooo`00m@3oool907o<I_H0oooo003e0?ooo`T0OlaV
mP3oool00?D0oooo2@1oc6Kf0?ooo`00m@3oool907o<I_H0oooo003e0?ooo`T0OlaVmP3oool00?D0
oooo2@1oc6Kf0?ooo`00m@3oool907o<I_H0oooo003e0?ooo`T0OlaVmP3oool00?D0oooo2@1oc6Kf
0?ooo`00m@3oool907o<I_H0oooo003e0?ooo`T0OlaVmP3oool00?D0oooo2@1oc6Kf0?ooo`00m@3o
ool907o<I_H0oooo003e0?ooo`T0OlaVmP3oool00?D0oooo2@1oc6Kf0?ooo`00m@3oool907o<I_H0
oooo003e0?ooo`T0OlaVmP3oool00?D0oooo2@1oc6Kf0?ooo`00m@3oool907o<I_H0oooo003e0?oo
o`T0OlaVmP3oool00?D0oooo2@1oc6Kf0?ooo`00m@3oool907o<I_H0oooo003e0?ooo`T0OlaVmP3o
ool00?D0oooo2@1oc6Kf0?ooo`00m@3oool907o<I_H0oooo003e0?ooo`T0OlaVmP3oool00?D0oooo
2@1oc6Kf0?ooo`00m@3oool907o<I_H0oooo003e0?ooo`T0OlaVmP3oool00?D0oooo2@1oc6Kf0?oo
o`00m@3oool907o<I_H0oooo003e0?ooo`T0OlaVmP3oool00?D0oooo2@1oc6Kf0?ooo`00m@3oool9
07o<I_H0oooo003e0?ooo`T0OlaVmP3oool00?D0oooo2@1oc6Kf0?ooo`00m@3oool907o<I_H0oooo
003e0?ooo`T0OlaVmP3oool00?D0oooo2@1oc6Kf0?ooo`00m@3oool907o<I_H0oooo003e0?ooo`T0
OlaVmP3oool00?D0oooo2@1oc6Kf0?ooo`00m@3oool907o<I_H0oooo0000\
\>"],
  ImageRangeCache->{{{0, 499}, {499, 0}} -> {-1.10001, -1.10001, 0.00440887, \
0.00440887}}],

Cell[BoxData[
    TagBox[\(\[SkeletonIndicator]  Graphics  \[SkeletonIndicator]\),
      False,
      Editable->False]], "Output"]
}, Open  ]],

Cell[BoxData[
    \(Table[p[t], {t, 0, 12, 1/3600*5}]\)], "Input"]
},
FrontEndVersion->"4.2 for Microsoft Windows",
ScreenRectangle->{{0, 1024}, {0, 690}},
WindowSize->{1016, 659},
WindowMargins->{{-10, Automatic}, {Automatic, -1}},
StyleDefinitions -> "Classroom.nb"
]

(*******************************************************************
Cached data follows.  If you edit this Notebook file directly, not
using Mathematica, you must remove the line containing CacheID at
the top of  the file.  The cache data will then be recreated when
you save this file from within Mathematica.
*******************************************************************)

(*CellTagsOutline
CellTagsIndex->{}
*)

(*CellTagsIndex
CellTagsIndex->{}
*)

(*NotebookFileOutline
Notebook[{
Cell[1754, 51, 1925, 33, 399, "Input"],

Cell[CellGroupData[{
Cell[3704, 88, 39, 1, 50, "Input"],
Cell[3746, 91, 26939, 399, 508, 1034, 76, "GraphicsData", "PostScript", \
"Graphics"],
Cell[30688, 492, 130, 3, 49, "Output"]
}, Open  ]],
Cell[30833, 498, 66, 1, 50, "Input"]
}
]
*)



(*******************************************************************
End of Mathematica Notebook file.
*******************************************************************)

