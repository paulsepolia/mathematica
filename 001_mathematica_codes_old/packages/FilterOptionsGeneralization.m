
(* : Title : FilterOptionsGeneralization.*)

(* : Authors : Paul Galiatsatos.*)

(* : Context : PaulsTools`FilterOptionsGeneralization`*)

(* : Package Version : 1.0*)

(* : Copyright : ... ... ...*)

(* : History : Version 1.0 by Paul Galiatsatos, December 2004.*)

(* : Keywords :*)

(* : Source : None.*)

(* : Warning :*)

(* : Mathematica Version : 5.1*)

(* : Limitation :*)

BeginPackage["PaulsTools`FilterOptionsGeneralization`"]


FilterOptionsGeneralization::usage = "
\nFilterOptionsGeneralization[listOfCommands_List,opts___?OptionQ]
\nis a generalization for the build-in Mathematica function
\nFilterOptions ,
\nwhich can filter simultaneously the Wrong options for
several Mathematica's commands."

Begin["`Private`"]

Unprotect["PaulsTools`FilterOptionsGeneralization`*"]

Needs["Utilities`FilterOptions`"]

ClearAll[FilterOptionsGeneralization];

FilterOptionsGeneralization[listOfCommands_, opts___?OptionQ] := 
  Module[{a, b}, a = Flatten[Map[Options, listOfCommands]];
    b = Union[Table[Part[a, i, 1], {i, 1, Length[a]}]];
    FilterOptions[b, opts]]

End[]

Protect["PaulsTools`FilterOptionsGeneralization`*"]

EndPackage[]


