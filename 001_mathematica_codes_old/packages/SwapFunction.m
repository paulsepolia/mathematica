(* :Title: SwapFunction. *)

(* :Authors: Paul Galiatsatos. *)

(* :Context: PaulsTools`SwapFunction` *)

(* :Package Version: 1.0 *)

(* :Copyright: No Copyright *)

(* :History: 
	Version 1.0 by Paul Galiatsatos, December 2004.
*)

(* :Keywords: *)

(* :Source: None. *)

(* :Warning: *)

(* :Mathematica Version: 5.1 *)

(* :Limitation: *)

BeginPackage["PaulsTools`SwapFunction`"]

SwapFunction::usage = 
"SwapFunction[object,i,j] exchanges elements i and j of the value of object."

Begin["`Private`"]

Unprotect["PaulsTools`SwapFunction`*"]

SetAttributes[SwapFunction, HoldFirst]

SwapFunction[object_, i_, j_] :=
  (
    {Part[object, i], Part[object, j]} =
      {Part[object, j], Part[object, i]};
    object
    )

End[]

Protect["PaulsTools`SwapFunction`*"]

EndPackage[]



