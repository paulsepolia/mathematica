(* Content-type: application/mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 7.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       145,          7]
NotebookDataLength[      8919,        310]
NotebookOptionsPosition[      8069,        279]
NotebookOutlinePosition[      8519,        296]
CellTagsIndexPosition[      8476,        293]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["\<\
                                                                  Newton \
Fractal\
\>", "Section"],

Cell[BoxData[
 RowBox[{"ClearAll", "[", "\"\<Global`*\>\"", "]"}]], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"realPart", "[", "n_", "]"}], "=", 
  RowBox[{"Together", "[", 
   RowBox[{"ComplexExpand", "[", 
    RowBox[{"Re", "[", 
     RowBox[{
      RowBox[{"x", "[", "n", "]"}], "+", 
      RowBox[{"I", " ", 
       RowBox[{"y", "[", "n", "]"}]}], "-", 
      FractionBox[
       RowBox[{
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{
           RowBox[{"x", "[", "n", "]"}], "+", 
           RowBox[{"I", " ", 
            RowBox[{"y", "[", "n", "]"}]}]}], ")"}], "3"], "-", "1"}], 
       RowBox[{"3", " ", 
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{
           RowBox[{"x", "[", "n", "]"}], "+", 
           RowBox[{"I", " ", 
            RowBox[{"y", "[", "n", "]"}]}]}], ")"}], "2"]}]]}], "]"}], "]"}], 
   "]"}]}]], "Input"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SuperscriptBox[
    RowBox[{"x", "[", "n", "]"}], "2"], "+", 
   RowBox[{"2", " ", 
    SuperscriptBox[
     RowBox[{"x", "[", "n", "]"}], "5"]}], "-", 
   SuperscriptBox[
    RowBox[{"y", "[", "n", "]"}], "2"], "+", 
   RowBox[{"4", " ", 
    SuperscriptBox[
     RowBox[{"x", "[", "n", "]"}], "3"], " ", 
    SuperscriptBox[
     RowBox[{"y", "[", "n", "]"}], "2"]}], "+", 
   RowBox[{"2", " ", 
    RowBox[{"x", "[", "n", "]"}], " ", 
    SuperscriptBox[
     RowBox[{"y", "[", "n", "]"}], "4"]}]}], 
  RowBox[{"3", " ", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{
      SuperscriptBox[
       RowBox[{"x", "[", "n", "]"}], "2"], "+", 
      SuperscriptBox[
       RowBox[{"y", "[", "n", "]"}], "2"]}], ")"}], "2"]}]]], "Output",
 CellChangeTimes->{3.4501981259913545`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"imaginaryPart", "[", "n_", "]"}], "=", 
  RowBox[{"Together", "[", 
   RowBox[{"ComplexExpand", "[", 
    RowBox[{"Im", "[", 
     RowBox[{
      RowBox[{"x", "[", "n", "]"}], "+", 
      RowBox[{"I", " ", 
       RowBox[{"y", "[", "n", "]"}]}], "-", 
      FractionBox[
       RowBox[{
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{
           RowBox[{"x", "[", "n", "]"}], "+", 
           RowBox[{"I", " ", 
            RowBox[{"y", "[", "n", "]"}]}]}], ")"}], "3"], "-", "1"}], 
       RowBox[{"3", " ", 
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{
           RowBox[{"x", "[", "n", "]"}], "+", 
           RowBox[{"I", " ", 
            RowBox[{"y", "[", "n", "]"}]}]}], ")"}], "2"]}]]}], "]"}], "]"}], 
   "]"}]}]], "Input"],

Cell[BoxData[
 FractionBox[
  RowBox[{"2", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      RowBox[{"-", 
       RowBox[{"x", "[", "n", "]"}]}], " ", 
      RowBox[{"y", "[", "n", "]"}]}], "+", 
     RowBox[{
      SuperscriptBox[
       RowBox[{"x", "[", "n", "]"}], "4"], " ", 
      RowBox[{"y", "[", "n", "]"}]}], "+", 
     RowBox[{"2", " ", 
      SuperscriptBox[
       RowBox[{"x", "[", "n", "]"}], "2"], " ", 
      SuperscriptBox[
       RowBox[{"y", "[", "n", "]"}], "3"]}], "+", 
     SuperscriptBox[
      RowBox[{"y", "[", "n", "]"}], "5"]}], ")"}]}], 
  RowBox[{"3", " ", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{
      SuperscriptBox[
       RowBox[{"x", "[", "n", "]"}], "2"], "+", 
      SuperscriptBox[
       RowBox[{"y", "[", "n", "]"}], "2"]}], ")"}], "2"]}]]], "Output",
 CellChangeTimes->{3.4501981361001544`*^9}]
}, Open  ]],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"ClearAll", "[", 
    RowBox[{"f", ",", "kk", ",", "a"}], "]"}], ";"}], "\[IndentingNewLine]", 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"f", "[", 
   RowBox[{"kk_", ",", 
    RowBox[{"{", 
     RowBox[{"x0_", ",", "y0_"}], "}"}]}], "]"}], ":=", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"ClearAll", "[", 
     RowBox[{"x", ",", "y"}], "]"}], ";", 
    RowBox[{
     RowBox[{"x", "[", "0", "]"}], "=", "x0"}], ";", 
    RowBox[{
     RowBox[{"y", "[", "0", "]"}], "=", "y0"}], ";", 
    RowBox[{
     RowBox[{"x", "[", "n_", "]"}], ":=", 
     RowBox[{
      RowBox[{"x", "[", "n", "]"}], "=", 
      RowBox[{"N", "[", 
       RowBox[{"realPart", "[", 
        RowBox[{"n", "-", "1"}], "]"}], "]"}]}]}], ";", 
    RowBox[{
     RowBox[{"y", "[", "n_", "]"}], ":=", 
     RowBox[{
      RowBox[{"y", "[", "n", "]"}], "=", 
      RowBox[{"N", "[", 
       RowBox[{"imaginaryPart", "[", 
        RowBox[{"n", "-", "1"}], "]"}], "]"}]}]}]}], ")"}]}]}], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"RootPointFun", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"x0_", ",", "y0_"}], "}"}], ",", "n_"}], "]"}], ":=", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{"f", "[", 
      RowBox[{"1", ",", 
       RowBox[{"{", 
        RowBox[{"x0", ",", "y0"}], "}"}]}], "]"}], ";", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"x0", ",", "y0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"x", "[", "n", "]"}], ",", 
         RowBox[{"y", "[", "n", "]"}]}], "}"}]}], "}"}]}], ")"}], "//", 
   "Timing"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"                                                                    \
", 
  RowBox[{"(*", " ", "roots", " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"x1", "=", 
     RowBox[{"{", 
      RowBox[{"1.", ",", "0."}], "}"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"x2", "=", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"-", "0.5"}], ",", 
       RowBox[{"-", "0.866025"}]}], "}"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"x3", "=", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"-", "0.5"}], ",", "0.866025"}], "}"}]}], ";"}]}]}]], "Input"],

Cell[BoxData[
 RowBox[{"ContourPlot", "[", 
  RowBox[{
   RowBox[{"Part", "[", 
    RowBox[{
     RowBox[{"RootPointFun", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"x0", ",", "y0"}], "}"}], ",", "10"}], "]"}], ",", "2", ",", 
     "2", ",", "2"}], "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"x0", ",", 
     RowBox[{"-", "1.5"}], ",", "1.5"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"y0", ",", 
     RowBox[{"-", "1"}], ",", "1"}], "}"}], ",", 
   RowBox[{"Frame", "\[Rule]", "False"}], ",", 
   RowBox[{"PlotPoints", "\[Rule]", "20"}], ",", 
   RowBox[{"ColorFunction", "\[Rule]", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"GrayLevel", "[", 
       RowBox[{"Abs", "[", "#", "]"}], "]"}], "&"}], ")"}]}], ",", 
   RowBox[{"ContourLines", "\[Rule]", "False"}], ",", 
   RowBox[{"Contours", "\[Rule]", "20"}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"DensityPlot", "[", 
  RowBox[{
   RowBox[{"Part", "[", 
    RowBox[{
     RowBox[{"RootPointFun", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"x0", ",", "y0"}], "}"}], ",", "100"}], "]"}], ",", "2", ",", 
     "2", ",", "2"}], "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"x0", ",", 
     RowBox[{"-", "1.5"}], ",", "1.5"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"y0", ",", 
     RowBox[{"-", "1"}], ",", "1"}], "}"}], ",", 
   RowBox[{"Frame", "\[Rule]", "False"}], ",", 
   RowBox[{"PlotPoints", "\[Rule]", "300"}], ",", 
   RowBox[{"ColorFunction", "\[Rule]", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"GrayLevel", "[", 
       RowBox[{"Abs", "[", "#", "]"}], "]"}], "&"}], ")"}]}], ",", 
   RowBox[{"Mesh", "\[Rule]", "False"}]}], "]"}]], "Input"]
}, Open  ]]
},
WindowSize->{1672, 910},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
Magnification:>FEPrivate`If[
  FEPrivate`Equal[FEPrivate`$VersionNumber, 6.], 2., 2. Inherited],
FrontEndVersion->"7.0 for Microsoft Windows (64-bit) (November 10, 2008)",
StyleDefinitions->"Classroom800X600.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[567, 22, 109, 3, 140, "Section"],
Cell[679, 27, 76, 1, 57, "Input"],
Cell[CellGroupData[{
Cell[780, 32, 801, 25, 100, "Input"],
Cell[1584, 59, 826, 27, 98, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2447, 91, 806, 25, 100, "Input"],
Cell[3256, 118, 854, 28, 101, "Output"]
}, Open  ]],
Cell[4125, 149, 1033, 32, 194, "Input"],
Cell[5161, 183, 607, 21, 57, "Input"],
Cell[5771, 206, 632, 19, 160, "Input"],
Cell[6406, 227, 849, 24, 92, "Input"],
Cell[7258, 253, 795, 23, 92, "Input"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
