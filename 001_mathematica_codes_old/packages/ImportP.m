
(* :Title: ImportP. *)

(* :Authors: Paul Galiatsatos. *)

(* :Context: PaulsTools`ImportP` *)

(* :Package Version: 1.0 *)

(* :Copyright: No Copyright *)

(* :History: 
	Version 1.0 by Paul Galiatsatos, December 2004.
*)

(* :Keywords: *)

(* :Source: None. *)

(* :Warning: *)

(* :Mathematica Version: 5.1 *)

(* :Limitation: *)


BeginPackage["PaulsTools`ImportP`"]




ImportP::usage = "ImportP is the head of a new function
\nthat generalizes the Import build-in Mathematica function.
\nThe possibles forms of the ImportP function are the following:
\n1....
\nImportP[{{firstLetter_},{\[Alpha]_,\[Beta]_,\[Gamma]_,\[Delta]_,\
\[CurlyEpsilon]_,\[Zeta]_,\[Eta]_},{format_},{path_String}},opts___?OptionQ],
\n2....
\nImportP[{{nameOfFrame_},{format_},{path_String}},opts___?OptionQ],
\n3....
\nImportP[{specialNumber_?IntegerQ},
\n{{firstLetter_},{frameNumber_},{format_},{path_String}},opts___?OptionQ],
\n4....
\nImportP[{{firstLetter_},{secondLetter_},{format_}, \
{path_String}},opts___?OptionQ]
\n5....
\nImportP[path_String,opts___?OptionQ].
\n
\nExplanations:
\n
\n1....The following usage: 
\nImportP[{{Photo},{\[Alpha],\[Beta], ,0,1,r,0},{Png},{examplePath}},opts]
\nis for the frame with name Photo\[Alpha]\[Beta]01r0 and 
\nformat Png at the path examplePath.
\n
\n2....The following usage:
\nImportP[{{Photo2},{Gif},{examplePath}},opts]
\nis for the frame with name Photo2 and
\nformat Gif at the path examplePath.
\n
\n3....The following usage:
\nImportP[{4},{{ph},{403},{Bmp},{examplePath}},opts]
\nis for the frame ph00403 with format Bmp at the path examplePath.
\n
\n4....The following usage:
\nImportP[{{a},{11},{Bmp}, {examplePath}},opts]
\nis for the frame a11 with format Bmp at the path examplePath.
\n
\n5....The following usage:
\nImportP[path,opts]
\nis exactly the Import[path,opts].
\n
\nAn important aspect is that you can set options 
\nexactly the same as for the original build-in function 
\nImport[path_String,opts___?OptionQ]."

Unprotect[PathImport]

ClearAll[PathImport]


PathImport::usage = "PathImport is a String.
\nIt has the special value:
\nF:\\Documents and Settings\\paulsepolia\\Desktop\\New Folder."


Begin["`Private`"]



Unprotect["PaulsTools`ImportP`*"]


ClearAll[helpImportFunction]


helpImportFunction[{{\[Theta]_}, {\[Alpha]_, \[Beta]_, \[Gamma]_, \[Delta]_, \
\
\
\
\
\
\
\
\
\
\
\
\[CurlyEpsilon]_, \[Zeta]_, \[Eta]_}, {\[CurlyPhi]_}, {\[Iota]_}}] := 
  StringReplace[
    "\[Iota]\\\[Theta]\[Alpha]\[Beta]\[Gamma]\[Delta]\[CurlyEpsilon]\[Zeta]\
\[Eta].\[CurlyPhi]",\
 {"\[Alpha]" -> ToString[\[Alpha]], "\[Beta]" -> ToString[\[Beta]], 
      "\[Gamma]" -> ToString[\[Gamma]], "\[Delta]" -> ToString[\[Delta]], 
      "\[CurlyEpsilon]" -> ToString[\[CurlyEpsilon]], 
      "\[Zeta]" -> ToString[\[Zeta]], "\[Eta]" -> ToString[\[Eta]], 
      "\[Theta]" -> ToString[\[Theta]], 
      "\[CurlyPhi]" -> ToString[\[CurlyPhi]], 
      "\[Iota]" -> ToString[\[Iota]]}]


helpImportFunction[{{\[Alpha]_}, {\[CurlyPhi]_}, {\[Iota]_}}] := 
  StringReplace[
    "\[Iota]\\\[Alpha].\[CurlyPhi]", {"\[Alpha]" -> ToString[\[Alpha]], 
      "\[CurlyPhi]" -> ToString[\[CurlyPhi]], 
      "\[Iota]" -> ToString[\[Iota]]}]


helpImportFunction[{{\[Alpha]_},{\[Zeta]_},{\[CurlyPhi]_}, {\[Iota]_}}] := 
  StringReplace[
    "\[Iota]\\\[Alpha]\[Zeta].\[CurlyPhi]", {"\[Alpha]" -> \
ToString[\[Alpha]], 
      "\[CurlyPhi]" -> ToString[\[CurlyPhi]], 
      "\[Iota]" -> ToString[\[Iota]],"\[Zeta]" -> ToString[\[Zeta]]}]



PathImport = "F:\\Documents and Settings\\paulsepolia\\Desktop\\New Folder"



ImportP[{{fl_}, {\[Alpha]_, \[Beta]_, \[Gamma]_, \[Delta]_, \[CurlyEpsilon]_, \
\
\
\
\
\
\
\
\
\
\
\
\[Zeta]_, \[Eta]_}, {f_}, {p_}},opts___?OptionQ] := 
  Import[helpImportFunction[{{fl}, {\[Alpha], \[Beta], \[Gamma], \[Delta], \
\[CurlyEpsilon], \[Zeta], \[Eta]}, {f}, {p}}],opts]


ImportP[{{fl_}, {f_}, {p_}},opts___?OptionQ] := \
Import[helpImportFunction[{{fl}, {f}, {p}}],opts]


ImportP[{{fl_},{secLe_},{f_}, {p_}},opts___?OptionQ] := 
Import[helpImportFunction[{{fl},{secLe},{f},{p}}],opts]

ImportP[path_,opts___?OptionQ] := Import[path,opts]

ClearAll[mmod]

mmod[a_, b_] := If[a < b/10, 0, Mod[Floor[a], b]]


ImportP[{myN_?IntegerQ}, {{fl_}, {frNu_}, {f_}, {p_}},opts___?OptionQ] := 
    Which[myN > 6, 
      Print["There is a limitation here.The first argument must be between 
{0,1,2,3,4,5,6}."], myN==6, 
      ImportP[{{fl}, {mmod[frNu/1000000,10], mmod[frNu/100000,10], 
            mmod[frNu/10000,10], mmod[frNu/1000,10], mmod[frNu/100, 10], 
            mmod[frNu/10,10], mmod[frNu,10]},{f},{p}},opts], myN==5, 
      ImportP[{{fl}, {"",mmod[frNu/100000,10], mmod[frNu/10000,10], 
            mmod[frNu/1000,10], mmod[frNu/100,10], mmod[frNu/10,10], 
            mmod[frNu,10]},{f},{p}},opts], myN==4, 
      ImportP[{{fl}, {"", "", mmod[frNu/10000,10], 
            mmod[frNu/1000,10], mmod[frNu/100,10], mmod[frNu/10, 10], 
            mmod[frNu,10]},{f},{p}},opts], myN==3, 
      ImportP[{{fl}, {"", "", "", mmod[frNu/1000,10], 
            mmod[frNu/100,10],mmod[frNu/10,10], 
            mmod[frNu,10]},{f},{p}},opts], myN==2, 
      ImportP[{{fl}, {"", "", "", "", mmod[frNu/100,10], 
            mmod[frNu/10,10], mmod[frNu,10]},{f},{p}},opts], myN==1, 
      ImportP[{{fl}, {"", "", "", "", "", 
            mmod[frNu/10,10], mmod[frNu,10]},{f},{p}},opts], myN==0, 
      ImportP[{{fl}, {"", "", "", "", "", "", 
            mmod[frNu,10]},{f},{p}},opts]]

End[]



Protect["PaulsTools`ImportP`*"]



EndPackage[]

(*
PaulsTools`ImportP`
*)
(*
ImportP is the head of a new function 
that generalizes the Import build-in Mathematica function. 
The possibles forms of the ImportP function are the following: 
1.... 
ImportP[{{firstLetter_},{\[Alpha]_,\[Beta]_,\[Gamma]_,\[Delta]_,\
\[CurlyEpsilon]_,\[Zeta]_,\[Eta]_},{format_},{path_}}], 
2.... 
ImportP[{{nameOfFrame_},{format_},{path_}}], 
3.... 
ImportP[{specialNumber_?         \
IntegerQ},{{firstLetter_},{frameNumber_},{format_},{path_}}], 
4.... 
ImportP[path_]. 
Explanations: 
1....The following usage:  
ImportP[{{Photo},{\[Alpha],\[Beta], ,0,1,r,0},{Png},{examplePath}}] 
is for the frame with name Photo\[Alpha]\[Beta]01r0 and  
format Png at the path examplePath. 
 
2....The following usage: 
ImportP[{{Photo2},{Gif},{examplePath}}] 
is for the frame with name Photo2 and 
format Gif at the path examplePath. 
 
3....The following usage: 
ImportP[{4},{{ph},{403},{Bmp},{examplePath}}] 
is for the frame ph00403 with format Bmp at the path examplePath. 
 
4....The following usage: 
ImportP[path] 
is exactly the Import[path].
*)
(*
\!\(\*
  RowBox[{\(ClearAll::"wrsym"\), \(\(:\)\(\ \)\), "\<\"Symbol \\!\\(pathP\\) \
\
\
\
\
\
\
\
\
\
\
is Protected. \\!\\(\\*ButtonBox[\\\"More\[Ellipsis]\\\", \
ButtonStyle->\\\"RefGuideLinkText\\\", ButtonFrame->None, \
ButtonData:>\\\"General::wrsym\\\"]\\)\"\>"}]\)
*)
(*
pathP has a specific value. 
It is a default path. 
The value is the string path 
F:\Documents and Settings\paulsepolia\Desktop\New Folder 
We can understand that inside New Folder  
we must search for frames!
*)
(*
PaulsTools`ImportP`Private`
*)
(*
{ImportP, pathP}
*)
(*
F:\Documents and Settings\paulsepolia\Desktop\New Folder
*)
(*
PaulsTools`ImportP`Private`
*)
(*
{ImportP, pathP}
*)










