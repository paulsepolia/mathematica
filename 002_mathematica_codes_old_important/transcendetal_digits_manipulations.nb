(************** Content-type: application/mathematica **************
                     CreatedBy='Mathematica 4.2'

                    Mathematica-Compatible Notebook

This notebook can be used with any Mathematica-compatible
application, such as Mathematica, MathReader or Publicon. The data
for the notebook starts with the line containing stars above.

To get the notebook into a Mathematica-compatible application, do
one of the following:

* Save the data starting with the line of stars above into a file
  with a name ending in .nb, then open the file inside the
  application;

* Copy the data starting with the line of stars above to the
  clipboard, then use the Paste menu command inside the application.

Data for notebooks contains only printable 7-bit ASCII and can be
sent directly in email or through ftp in text mode.  Newlines can be
CR, LF or CRLF (Unix, Macintosh or MS-DOS style).

NOTE: If you modify the data for this notebook not in a Mathematica-
compatible application, you must delete the line below containing
the word CacheID, otherwise Mathematica-compatible applications may
try to use invalid cache data.

For more information on notebooks and Mathematica-compatible 
applications, contact Wolfram Research:
  web: http://www.wolfram.com
  email: info@wolfram.com
  phone: +1-217-398-0700 (U.S.)

Notebook reader applications are available free of charge from 
Wolfram Research.
*******************************************************************)

(*CacheID: 232*)


(*NotebookFileLineBreakTest
NotebookFileLineBreakTest*)
(*NotebookOptionsPosition[     19402,        468]*)
(*NotebookOutlinePosition[     20085,        491]*)
(*  CellTagsIndexPosition[     20041,        487]*)
(*WindowFrame->Normal*)



Notebook[{
Cell["\<\
The following program gives the function with name :  final[a,k,nmin,nmax]  .
\"a\" is the  transcendetal number which its digits i consider about ,
\"k\" is the length  of  FractionPart of transcendental number ,
\"nmin\" , \"nmax\" is minimum and maximum value of digits .
The result is at the form {{transcendental , number of fractionalDigits , \
Averege , Value},{transcendental , number of fractionalDigits , Apoklisi , \
value },{transcendental , number of fractionalDigits , AveregeTheoritical , \
value},{transcendental , number of fractionalDigits , ApoklisiTheritical , \
value }}\
\>", "Text"],

Cell[BoxData[
    \(\($MaxPrecision = Infinity;\)\)], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
    \(Clear[n, s, k, a]; 
    AvTHEORITICAL[nmin_, nmax_] := 
      Sum[n, {n, nmin, nmax}]/\((nmax - nmin + 1)\) // N; 
    apoklisiTHEORITICAL[nmin_, 
        nmax_] := \((Sum[
                  n^2, {n, nmin, nmax}]/\((nmax + 1 - 
                    nmin)\) - \((Sum[
                      n, {n, nmin, nmax}]/\((nmax + 1 - nmin)\))\)^2)\)^0.5 // 
        N; Av[s_] := Apply[Plus, s]/Length[s] // N; 
    apoklisi[s_] := \((Av[s^2] - Av[s]^2)\)^0.5; 
    pi[a_, k_] := FractionalPart[N[a, k + 10]]; 
    piINTEGERpart[a_, k_] := IntegerPart[pi[a, k + 10]*10^k]; 
    piINTEGERdigits[a_, k_] := IntegerDigits[piINTEGERpart[a, k]]; 
    length[a_, k_] := Length[piINTEGERdigits[a, k]]; 
    final[a_, k_, nmin_, 
        nmax_] := {{a, length[a, k], Averege, Av[piINTEGERdigits[a, k]]}, {a, 
          length[a, k], Apoklisi, apoklisi[piINTEGERdigits[a, k]]}, {a, 
          nmax + 1 - nmin, AveregeTHEORITICAL, 
          AvTHEORITICAL[nmin, nmax]}, {a, nmax + 1 - nmin, 
          ApoklisiTHEORITICAL, apoklisiTHEORITICAL[nmin, nmax]}}; 
    final[Pi, 30000, 0, 9]\)], "Input"],

Cell[BoxData[
    \({{\[Pi], 30000, Averege, 4.501633333333333`}, {\[Pi], 30000, Apoklisi, 
        2.870527941956942`}, {\[Pi], 10, AveregeTHEORITICAL, 4.5`}, {\[Pi], 
        10, ApoklisiTHEORITICAL, 2.872281323269014`}}\)], "Output"]
}, Open  ]],

Cell["\<\
The following program gives the List  : t .
\" t \" is the list of numbers shows the positions in which i met a certain \
sequence of digits in the fractionalPart of a certain         transcendental  \
number , in  the following example i found that at position 2831 begins a \
sequence of 4 digits 3 at the fractionalpart of E*Pi .       \
\>", "Text"],

Cell[CellGroupData[{

Cell[BoxData[
    \(Clear[s, i, m, r, j, t, a, k, s, n]; 
    Clear[s1]; {s1 = {3, 3, 3, 3}, jj = 10000, a = E*Pi}; Remove[y]; 
    Evaluate[Table[y[i], {i, 1, Length[s1]}]] = 
      Evaluate[Table[Part[s1, i], {i, 1, Length[s1]}]]; 
    pi[a_, k_] := FractionalPart[N[a, k + 10]]; 
    piINTEGERpart[a_, k_] := IntegerPart[pi[a, k + 10]*10^k]; 
    piINTEGERdigits[a_, k_] := IntegerDigits[piINTEGERpart[a, k]]; 
    length[a_, k_] := Length[piINTEGERdigits[a, k]]; Remove[n]; t = {}; 
    f[transcendetal_, 
        digits_] := \((s = piINTEGERdigits[transcendetal, digits]; 
        Do[If[Evaluate[
                Table[Part[s, i + m], {m, 0, Length[s1] - 1}]] \[Equal] 
              Evaluate[Table[y[e], {e, Length[s1]}]], t = Append[t, i], 
            t = t], {i, 1, Length[s] - Length[s1] + 1}])\); f[a, jj]; 
    t\)], "Input"],

Cell[BoxData[
    \({2831}\)], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(Clear[s, i, m, r, j, t, a, k, s, n]; 
    Clear[s1]; {s1 = {1, 2, 1, 2, 1, 2}, jj = 10000000, a = Pi}; Remove[y]; 
    Evaluate[Table[y[i], {i, 1, Length[s1]}]] = 
      Evaluate[Table[Part[s1, i], {i, 1, Length[s1]}]]; 
    pi[a_, k_] := FractionalPart[N[a, k + 10]]; 
    piINTEGERpart[a_, k_] := IntegerPart[pi[a, k + 10]*10^k]; 
    piINTEGERdigits[a_, k_] := IntegerDigits[piINTEGERpart[a, k]]; 
    length[a_, k_] := Length[piINTEGERdigits[a, k]]; Remove[n]; t = {}; 
    f[transcendetal_, 
        digits_] := \((s = piINTEGERdigits[transcendetal, digits]; 
        Do[If[Evaluate[
                Table[Part[s, i + m], {m, 0, Length[s1] - 1}]] \[Equal] 
              Evaluate[Table[y[e], {e, Length[s1]}]], t = Append[t, i], 
            t = t], {i, 1, Length[s] - Length[s1] + 1}])\); f[a, jj]; 
    t\)], "Input"],

Cell[BoxData[
    \({241987, 1065022, 1246310, 1588384, 1894534, 3380964, 5149956, 5876147, 
      6540947, 7906366, 8406537, 9066112, 9904921, 9995212}\)], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(Clear[s, i, m, r, j, t, a, k, s, n]; 
    Clear[s1]; {s1 = {3, 2, 3, 2, 3, 2}, jj = 10000000, a = Pi}; Remove[y]; 
    Evaluate[Table[y[i], {i, 1, Length[s1]}]] = 
      Evaluate[Table[Part[s1, i], {i, 1, Length[s1]}]]; 
    pi[a_, k_] := FractionalPart[N[a, k + 10]]; 
    piINTEGERpart[a_, k_] := IntegerPart[pi[a, k + 10]*10^k]; 
    piINTEGERdigits[a_, k_] := IntegerDigits[piINTEGERpart[a, k]]; 
    length[a_, k_] := Length[piINTEGERdigits[a, k]]; Remove[n]; t = {}; 
    f[transcendetal_, 
        digits_] := \((s = piINTEGERdigits[transcendetal, digits]; 
        Do[If[Evaluate[
                Table[Part[s, i + m], {m, 0, Length[s1] - 1}]] \[Equal] 
              Evaluate[Table[y[e], {e, Length[s1]}]], t = Append[t, i], 
            t = t], {i, 1, Length[s] - Length[s1] + 1}])\); f[a, jj]; 
    t\)], "Input"],

Cell[BoxData[
    \({238699, 910963, 1448540, 2006745, 3279829, 4483452, 5314942, 5461017, 
      6192990, 6269486, 8267401}\)], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(Clear[s, i, m, r, j, t, a, k, s, n]; 
    Clear[s1]; {s1 = {1, 1, 1, 1, 1, 1, 1}, jj = 15000000, a = Pi}; 
    Remove[y]; 
    Evaluate[Table[y[i], {i, 1, Length[s1]}]] = 
      Evaluate[Table[Part[s1, i], {i, 1, Length[s1]}]]; 
    pi[a_, k_] := FractionalPart[N[a, k + 10]]; 
    piINTEGERpart[a_, k_] := IntegerPart[pi[a, k + 10]*10^k]; 
    piINTEGERdigits[a_, k_] := IntegerDigits[piINTEGERpart[a, k]]; 
    length[a_, k_] := Length[piINTEGERdigits[a, k]]; Remove[n]; t = {}; 
    f[transcendetal_, 
        digits_] := \((s = piINTEGERdigits[transcendetal, digits]; 
        Do[If[Evaluate[
                Table[Part[s, i + m], {m, 0, Length[s1] - 1}]] \[Equal] 
              Evaluate[Table[y[e], {e, Length[s1]}]], t = Append[t, i], 
            t = t], {i, 1, Length[s] - Length[s1] + 1}])\); f[a, jj]; 
    t\)], "Input"],

Cell[BoxData[
    \({4657555}\)], "Output"]
}, Open  ]],

Cell["\<\
The final purpose is to find a list in form :
{ { position of certain digit#1 on the TranscendetalfractionPart  , digit#1 \
at this position } , { position of certain digit#2 on the \
TranscendetalfractionPart  , digit#2 at this position  } , ............... , \
{position of certain digit#n on the TranscendetalfractionPart  , digit#n at \
this position } } 
The program most close to the final purpose is the following
where : (1) \" s1 \"  is the certain sequence of digits i look for , in form \
List .
               (2) \" jj \"     is  number of   fractionalPart digits of the \
transcendetal .
               (3) \" a \"     is  the Transcendetal .
             \
\>", "Text"],

Cell[CellGroupData[{

Cell[BoxData[
    \(Clear[s, i, m, r, j, t, a, k, s, n]; 
    Clear[s1]; {s1 = {2, 9, 1, 1, 7, 3}, jj = 15000000, a = Pi}; Remove[y]; 
    Evaluate[Table[y[i], {i, 1, Length[s1]}]] = 
      Evaluate[Table[Part[s1, i], {i, 1, Length[s1]}]]; 
    pi[a_, k_] := FractionalPart[N[a, k + 10]]; 
    piINTEGERpart[a_, k_] := IntegerPart[pi[a, k + 10]*10^k]; 
    piINTEGERdigits[a_, k_] := IntegerDigits[piINTEGERpart[a, k]]; 
    length[a_, k_] := Length[piINTEGERdigits[a, k]]; Remove[n]; t = {}; 
    f[transcendetal_, 
        digits_] := \((s = piINTEGERdigits[transcendetal, digits]; 
        Do[If[Evaluate[
                Table[Part[s, i + m], {m, 0, Length[s1] - 1}]] \[Equal] 
              Evaluate[Table[y[e], {e, Length[s1]}]], t = Append[t, i], 
            t = t], {i, 1, Length[s] - Length[s1] + 1}])\); f[a, jj]; 
    Partition[
      Flatten[Table[
          Part[Partition[
              Flatten[Table[
                  Part[Partition[
                      Flatten[{Table[t + i, {i, 0, Length[s1] - 1}], 
                          Evaluate[
                            Table[Part[s, t + i], {i, 0, Length[s1] - 1}]]}], 
                      Length[t]], {i, i + Length[s1]}], {i, 1, Length[s1]}]], 
              2*Length[t]], j, {i, i + Length[t]}], {j, 1, Length[s1]}, {i, 
            1, Length[t]}]], 2]\)], "Input"],

Cell[BoxData[
    \({{278554, 2}, {641848, 2}, {3259385, 2}, {3312774, 2}, {3688006, 
        2}, {7950265, 2}, {8344202, 2}, {9377962, 2}, {9419159, 2}, {9616346, 
        2}, {12348599, 2}, {13787791, 2}, {278555, 9}, {641849, 9}, {3259386, 
        9}, {3312775, 9}, {3688007, 9}, {7950266, 9}, {8344203, 9}, {9377963, 
        9}, {9419160, 9}, {9616347, 9}, {12348600, 9}, {13787792, 
        9}, {278556, 1}, {641850, 1}, {3259387, 1}, {3312776, 1}, {3688008, 
        1}, {7950267, 1}, {8344204, 1}, {9377964, 1}, {9419161, 1}, {9616348, 
        1}, {12348601, 1}, {13787793, 1}, {278557, 1}, {641851, 1}, {3259388, 
        1}, {3312777, 1}, {3688009, 1}, {7950268, 1}, {8344205, 1}, {9377965, 
        1}, {9419162, 1}, {9616349, 1}, {12348602, 1}, {13787794, 
        1}, {278558, 7}, {641852, 7}, {3259389, 7}, {3312778, 7}, {3688010, 
        7}, {7950269, 7}, {8344206, 7}, {9377966, 7}, {9419163, 7}, {9616350, 
        7}, {12348603, 7}, {13787795, 7}, {278559, 3}, {641853, 3}, {3259390, 
        3}, {3312779, 3}, {3688011, 3}, {7950270, 3}, {8344207, 3}, {9377967, 
        3}, {9419164, 3}, {9616351, 3}, {12348604, 3}, {13787796, 
        3}}\)], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(Clear[s, i, m, r, j, t, a, k, s, n]; 
    Clear[s1]; {s1 = {2, 9, 1}, jj = 10000, a = Pi}; Remove[y]; 
    Evaluate[Table[y[i], {i, 1, Length[s1]}]] = 
      Evaluate[Table[Part[s1, i], {i, 1, Length[s1]}]]; 
    pi[a_, k_] := FractionalPart[N[a, k + 10]]; 
    piINTEGERpart[a_, k_] := IntegerPart[pi[a, k + 10]*10^k]; 
    piINTEGERdigits[a_, k_] := IntegerDigits[piINTEGERpart[a, k]]; 
    length[a_, k_] := Length[piINTEGERdigits[a, k]]; Remove[n]; t = {}; 
    f[transcendetal_, 
        digits_] := \((s = piINTEGERdigits[transcendetal, digits]; 
        Do[If[Evaluate[
                Table[Part[s, i + m], {m, 0, Length[s1] - 1}]] \[Equal] 
              Evaluate[Table[y[e], {e, Length[s1]}]], t = Append[t, i], 
            t = t], {i, 1, Length[s] - Length[s1] + 1}])\); f[a, jj]; 
    Partition[
      Flatten[Table[
          Part[Partition[
              Flatten[Table[
                  Part[Partition[
                      Flatten[{Table[t + i, {i, 0, Length[s1] - 1}], 
                          Evaluate[
                            Table[Part[s, t + i], {i, 0, Length[s1] - 1}]]}], 
                      Length[t]], {i, i + Length[s1]}], {i, 1, Length[s1]}]], 
              2*Length[t]], j, {i, i + Length[t]}], {j, 1, Length[s1]}, {i, 
            1, Length[t]}]], 2]\)], "Input"],

Cell[BoxData[
    \({{3368, 2}, {3403, 2}, {3792, 2}, {6033, 2}, {6086, 2}, {6229, 
        2}, {7600, 2}, {7786, 2}, {7877, 2}, {7946, 2}, {8198, 2}, {3369, 
        9}, {3404, 9}, {3793, 9}, {6034, 9}, {6087, 9}, {6230, 9}, {7601, 
        9}, {7787, 9}, {7878, 9}, {7947, 9}, {8199, 9}, {3370, 1}, {3405, 
        1}, {3794, 1}, {6035, 1}, {6088, 1}, {6231, 1}, {7602, 1}, {7788, 
        1}, {7879, 1}, {7948, 1}, {8200, 1}}\)], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(Clear[n, s, k, a]; 
    AvTHEORITICAL[nmin_, nmax_] := 
      Sum[n, {n, nmin, nmax}]/\((nmax - nmin + 1)\) // N; 
    apoklisiTHEORITICAL[nmin_, 
        nmax_] := \((Sum[
                  n^2, {n, nmin, nmax}]/\((nmax + 1 - 
                    nmin)\) - \((Sum[
                      n, {n, nmin, nmax}]/\((nmax + 1 - nmin)\))\)^2)\)^0.5 // 
        N; Av[s_] := Apply[Plus, s]/Length[s] // N; 
    apoklisi[s_] := \((Av[s^2] - Av[s]^2)\)^0.5; 
    pi[a_, k_] := FractionalPart[N[a, k + 10]]; 
    piINTEGERpart[a_, k_] := IntegerPart[pi[a, k + 10]*10^k]; 
    piINTEGERdigits[a_, k_] := IntegerDigits[piINTEGERpart[a, k]]; 
    length[a_, k_] := Length[piINTEGERdigits[a, k]]; 
    final[a_, k_, nmin_, 
        nmax_] := {{a, length[a, k], Averege, Av[piINTEGERdigits[a, k]]}, {a, 
          length[a, k], Apoklisi, apoklisi[piINTEGERdigits[a, k]]}, {a, 
          nmax + 1 - nmin, AveregeTHEORITICAL, 
          AvTHEORITICAL[nmin, nmax]}, {a, nmax + 1 - nmin, 
          ApoklisiTHEORITICAL, apoklisiTHEORITICAL[nmin, nmax]}}; 
    final[Pi, 30000, 0, 9]\)], "Input"],

Cell[BoxData[
    \({{\[Pi], 30000, Averege, 4.501633333333333`}, {\[Pi], 30000, Apoklisi, 
        2.870527941956942`}, {\[Pi], 10, AveregeTHEORITICAL, 4.5`}, {\[Pi], 
        10, ApoklisiTHEORITICAL, 2.872281323269014`}}\)], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(aaa = 
      Table[{i, Count[piINTEGERdigits[Pi, 100000], i]}, {i, 0, 9}]\)], "Input"],

Cell[BoxData[
    \({{0, 9999}, {1, 10137}, {2, 9908}, {3, 10025}, {4, 9971}, {5, 
        10026}, {6, 10029}, {7, 10025}, {8, 9978}, {9, 9902}}\)], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(Clear[a, d]; 
    f[a_, d_] := 
      Part[Flatten[Table[{i, Count[piINTEGERdigits[a, d], i]}, {i, 0, 9}]], 
            Table[i, {i, 2, 20, 2}]]/d // N; f[Pi, 100000]\)], "Input"],

Cell[BoxData[
    \({0.09999`, 0.10137`, 0.09908`, 0.10025`, 0.09971`, 0.10026`, 0.10029`, 
      0.10025`, 0.09978`, 0.09902`}\)], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(lll = 
      Table[{i, 
            1/10000*Abs[10000 - Count[piINTEGERdigits[Pi, 100000], i]]}, {i, 
            0, 9}] // N\)], "Input"],

Cell[BoxData[
    \({{0.`, 0.0001`}, {1.`, 0.0137`}, {2.`, 0.0092`}, {3.`, 0.0025`}, {4.`, 
        0.0029`}, {5.`, 0.0026`}, {6.`, 0.0029`}, {7.`, 0.0025`}, {8.`, 
        0.0022`}, {9.`, 0.0098`}}\)], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(lll1 = 
      Table[{i, Abs[10000 - Count[piINTEGERdigits[Pi, 100000], i]]}, {i, 0, 
            9}] // N\)], "Input"],

Cell[BoxData[
    \({{0.`, 1.`}, {1.`, 137.`}, {2.`, 92.`}, {3.`, 25.`}, {4.`, 29.`}, {5.`, 
        26.`}, {6.`, 29.`}, {7.`, 25.`}, {8.`, 22.`}, {9.`, 
        98.`}}\)], "Output"]
}, Open  ]],

Cell[BoxData[
    \(listplot[
        k_] := \((baa = 
          Evaluate[Table[{i, Count[piINTEGERdigits[E, k], i]}, {i, 0, 9}]]; 
        ListPlot[baa, 
          PlotStyle \[Rule] {RGBColor[0, 0.5, 1], PointSize[0.05], 
              Thickness[0.01]}, PlotJoined \[Rule] True, 
          Background \[Rule] RGBColor[0, 0, 0], 
          AxesStyle \[Rule] {RGBColor[1, 0.5, 0], Thickness[0.006]}, 
          ImageSize \[Rule] {600, 400}, 
          PlotRange \[Rule] {11*k/100, 9*k/100}, Axes \[Rule] False, 
          DisplayFunction \[Rule] Identity])\); 
    graph1[k_] := 
      Plot[{k/10}, {x, 0, 9}, PlotRange \[Rule] {11*k/100, 9*k/100}, 
        PlotStyle \[Rule] {RGBColor[1, 0, 0], Thickness[0.04]}, 
        Epilog \[Rule] {Evaluate[
              Table[{Thickness[0.01], RGBColor[1, 0, 0], 
                  Line[{{i, 10.8*k/100}, {i, 9.1*k/100}}]}, {i, 0, 
                  9}]], {Thickness[0.01], RGBColor[0, 1, 0], 
              Line[{{0, k/10}, {9, k/10}}]}, 
            Evaluate[
              Table[Text[
                  StyleForm[i, FontSize \[Rule] 18, 
                    FontColor \[Rule] RGBColor[0, 1, 1]], {i, 9. *k/100}, {0, 
                    0}, TextStyle \[Rule] {FontWeight -> "\<Bold\>"}], {i, 0, 
                  9}]], Text[
              StyleForm[k, FontSize \[Rule] 18, 
                FontColor \[Rule] RGBColor[0, 1, 1]], {7.7, 
                10.95*k/100}, {\(-1\), 0}, 
              TextStyle \[Rule] {FontWeight -> "\<Bold\>"}], 
            Text[StyleForm["\<FractionPartDigits =\>", FontSize \[Rule] 20, 
                FontColor \[Rule] RGBColor[0, 1, 1]], {5.8, 10.95*k/100}, {0, 
                0}, TextStyle \[Rule] {FontWeight -> "\<Bold\>"}], 
            Text[StyleForm["\<Transcendetal = E\>", FontSize \[Rule] 20, 
                FontColor \[Rule] RGBColor[0, 1, 1]], {1.8, 10.95*k/100}, {0, 
                0}, TextStyle \[Rule] {FontWeight -> "\<Bold\>"}]}, 
        Background \[Rule] RGBColor[0, 0, 0], Axes -> False, 
        ImageSize \[Rule] {600, 400}];\)], "Input"],

Cell[BoxData[
    \(Clear[q]; 
    Evaluate[Table[q[IntegerPart[i]], {i, 1000, 500000, 1000}]] = 
      Evaluate[Table[
          listplot[IntegerPart[i]], {i, 1000, 500000, 1000}]];\)], "Input"],

Cell[BoxData[
    \(Clear[qq]; 
    Evaluate[Table[qq[IntegerPart[i]], {i, 1000, 500000, 1000}]] = 
      Evaluate[Table[
          graph1[IntegerPart[i]], {i, 1000, 500000, 1000}]];\)], "Input"],

Cell[BoxData[
    \(Table[
      Show[{qq[IntegerPart[i]], q[IntegerPart[i]]}], {i, 1000, 500000, 
        1000}]\)], "Input"],

Cell[BoxData[
    \(Clear[qq]; 
    Evaluate[Table[qq[IntegerPart[1.5^i]], {i, 10, 34, 0.1}]] = 
      Evaluate[Table[
          graph1[IntegerPart[1.5^i]], {i, 10, 34, 0.1}]];\)], "Input"],

Cell[BoxData[
    \(Table[
      Show[{qq[IntegerPart[1.5^i]], q[IntegerPart[1.5^i]]}], {i, 10, 34, 
        0.1}]\)], "Input"],

Cell[BoxData[
    \(Clear[q]; 
    Evaluate[Table[q[IntegerPart[i]], {i, 1000, 500000, 1000}]] = 
      Evaluate[Table[
          listplot[IntegerPart[i]], {i, 1000, 500000, 1000}]];\)], "Input"],

Cell[BoxData[
    \(Clear[qq]; 
    Evaluate[Table[qq[IntegerPart[i]], {i, 1000, 500000, 1000}]] = 
      Evaluate[Table[
          graph1[IntegerPart[i]], {i, 1000, 500000, 1000}]];\)], "Input"],

Cell[BoxData[
    \(Table[
      Show[{qq[IntegerPart[i]], q[IntegerPart[i]]}], {i, 1000, 500000, 
        1000}]\)], "Input"]
},
FrontEndVersion->"4.2 for Microsoft Windows",
ScreenRectangle->{{0, 1024}, {0, 688}},
WindowSize->{1016, 656},
WindowMargins->{{-8, Automatic}, {Automatic, 42}},
StyleDefinitions -> "Classroom2.nb"
]

(*******************************************************************
Cached data follows.  If you edit this Notebook file directly, not
using Mathematica, you must remove the line containing CacheID at
the top of  the file.  The cache data will then be recreated when
you save this file from within Mathematica.
*******************************************************************)

(*CellTagsOutline
CellTagsIndex->{}
*)

(*CellTagsIndex
CellTagsIndex->{}
*)

(*NotebookFileOutline
Notebook[{
Cell[1754, 51, 615, 10, 235, "Text"],
Cell[2372, 63, 62, 1, 50, "Input"],

Cell[CellGroupData[{
Cell[2459, 68, 1095, 21, 210, "Input"],
Cell[3557, 91, 236, 3, 49, "Output"]
}, Open  ]],
Cell[3808, 97, 363, 6, 130, "Text"],

Cell[CellGroupData[{
Cell[4196, 107, 837, 15, 210, "Input"],
Cell[5036, 124, 40, 1, 49, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[5113, 130, 844, 15, 210, "Input"],
Cell[5960, 147, 166, 2, 49, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[6163, 154, 844, 15, 210, "Input"],
Cell[7010, 171, 138, 2, 49, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[7185, 178, 852, 16, 210, "Input"],
Cell[8040, 196, 43, 1, 49, "Output"]
}, Open  ]],
Cell[8098, 200, 694, 14, 305, "Text"],

Cell[CellGroupData[{
Cell[8817, 218, 1336, 25, 370, "Input"],
Cell[10156, 245, 1177, 16, 182, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[11370, 266, 1324, 25, 370, "Input"],
Cell[12697, 293, 442, 6, 87, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[13176, 304, 1095, 21, 210, "Input"],
Cell[14274, 327, 236, 3, 49, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[14547, 335, 106, 2, 50, "Input"],
Cell[14656, 339, 158, 2, 49, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[14851, 346, 200, 4, 70, "Input"],
Cell[15054, 352, 141, 2, 49, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[15232, 359, 158, 4, 50, "Input"],
Cell[15393, 365, 212, 3, 68, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[15642, 373, 138, 3, 50, "Input"],
Cell[15783, 378, 182, 3, 49, "Output"]
}, Open  ]],
Cell[15980, 384, 2046, 37, 310, "Input"],
Cell[18029, 423, 195, 4, 70, "Input"],
Cell[18227, 429, 195, 4, 70, "Input"],
Cell[18425, 435, 126, 3, 50, "Input"],
Cell[18554, 440, 189, 4, 50, "Input"],
Cell[18746, 446, 127, 3, 50, "Input"],
Cell[18876, 451, 195, 4, 70, "Input"],
Cell[19074, 457, 195, 4, 70, "Input"],
Cell[19272, 463, 126, 3, 50, "Input"]
}
]
*)



(*******************************************************************
End of Mathematica Notebook file.
*******************************************************************)

