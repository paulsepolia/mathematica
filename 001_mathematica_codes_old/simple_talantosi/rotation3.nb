(************** Content-type: application/mathematica **************
                     CreatedBy='Mathematica 4.2'

                    Mathematica-Compatible Notebook

This notebook can be used with any Mathematica-compatible
application, such as Mathematica, MathReader or Publicon. The data
for the notebook starts with the line containing stars above.

To get the notebook into a Mathematica-compatible application, do
one of the following:

* Save the data starting with the line of stars above into a file
  with a name ending in .nb, then open the file inside the
  application;

* Copy the data starting with the line of stars above to the
  clipboard, then use the Paste menu command inside the application.

Data for notebooks contains only printable 7-bit ASCII and can be
sent directly in email or through ftp in text mode.  Newlines can be
CR, LF or CRLF (Unix, Macintosh or MS-DOS style).

NOTE: If you modify the data for this notebook not in a Mathematica-
compatible application, you must delete the line below containing
the word CacheID, otherwise Mathematica-compatible applications may
try to use invalid cache data.

For more information on notebooks and Mathematica-compatible 
applications, contact Wolfram Research:
  web: http://www.wolfram.com
  email: info@wolfram.com
  phone: +1-217-398-0700 (U.S.)

Notebook reader applications are available free of charge from 
Wolfram Research.
*******************************************************************)

(*CacheID: 232*)


(*NotebookFileLineBreakTest
NotebookFileLineBreakTest*)
(*NotebookOptionsPosition[     13360,        252]*)
(*NotebookOutlinePosition[     14043,        275]*)
(*  CellTagsIndexPosition[     13999,        271]*)
(*WindowFrame->Normal*)



Notebook[{
Cell[BoxData[
    \(p1[n_, m_] := 
      Show[Graphics[{{PointSize[ .07], RGBColor[0, 0, 1], 
              Point[{Sin[\((2*Pi + m)\)*n], 0}]}}, 
          AspectRatio \[Rule] Automatic, 
          PlotRange \[Rule] {{\(-1.08\), 1.08}, {\(-1.08\), 1.08}}, 
          ImageSize \[Rule] {800, 500}]]\)], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
    \(p1[0, 100]\)], "Input"],

Cell[GraphicsData["PostScript", "\<\
%!
%%Creator: Mathematica
%%AspectRatio: 1 
%%ImageSize: 800 500 
MathPictureStart
/Mabs {
Mgmatrix idtransform
Mtmatrix dtransform
} bind def
/Mabsadd { Mabs
3 -1 roll add
3 1 roll add
exch } bind def
%% Graphics
%%IncludeResource: font Courier
%%IncludeFont: Courier
/Courier findfont 10  scalefont  setfont
% Scaling calculations
0.5 0.462963 0.5 0.462963 [
[ 0 0 0 0 ]
[ 1 1 0 0 ]
] MathScale
% Start of Graphics
1 setlinecap
1 setlinejoin
newpath
0 0 m
1 0 L
1 1 L
0 1 L
closepath
clip
newpath
0 0 1 r
.07 w
.5 .03704 Mdot
1 0 0 r
.35694 .0597 Mdot
% End of Graphics
MathPictureEnd
\
\>"], "Graphics",
  ImageSize->{800, 500},
  ImageMargins->{{0, 0}, {0, 0}},
  ImageRegion->{{0, 1}, {0, 1}},
  ImageCache->GraphicsData["Bitmap", "\<\
CF5dJ6E]HGAYHf4PAg9QL6QYHg<PAVmbKF5d0`4000<P0001m1000`40O003h00Oogooogooogoo8goo
003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooSWoo100OogooT7oo003oOon9Ool>01ooOon;
Ool00?moohQooa007omoohYoo`00ogooQWoo500OogooR7oo003oOon5OolF01ooOon7Ool00?moohAo
oaP07omoohIoo`00ogooPgoo6P0OogooQGoo003oOon2OolL01ooOon4Ool00?mooh9ooa`07omoohAo
o`00ogooPGoo7P0OogooPgoo003oOon0OolP01ooOon2Ool00?moodMoo`Al03Eoob007omooh9oo`00
ogoo@Woo3W`0<7oo800OogooPWoo003oOom1Ool@O00_OolP01ooOon2Ool00?moocmooaAl02eoob00
7omooh9oo`00ogoo?Woo5W`0:goo8P0OogooPGoo003oOolmOolHO00ZOolR01ooOon1Ool00?moocao
oaYl02Uoob807omooh5oo`00ogoo>goo77`0:7oo8P0OogooPGoo003oOolkOolLO00YOolP01ooOon2
Ool00?moocYooail02Qoob007omooh9oo`00ogoo>Goo87`09goo800OogooPWoo003oOoliOolPO00W
OolP01ooOon2Ool00?moocUoob1l02Moob007omooh9oo`00ogoo>Goo87`0:7oo7P0OogooPgoo003o
OoliOolPO00YOolL01ooOon4Ool00?moocQoob9l02Qooa`07omoohAoo`00ogoo>7oo8W`0:Goo6P0O
ogooQGoo003oOolhOolRO00ZOolH01ooOon6Ool00?moocQoob9l02]ooaH07omoohMoo`00ogoo>Goo
87`0;Goo500OogooR7oo003oOoliOolPO00_Ool@01ooOon:Ool00?moocUoob1l031oo`h07omooh]o
o`00ogoo>Goo87`0=Goo100OogooT7oo003oOoliOolPO03oOoo9Ool00?moocYooail0?moolYoo`00
ogoo>goo77`0ogoobgoo003oOolkOolLO03oOoo;Ool00?moocaooaYl0?moolaoo`00ogoo?Goo67`0
ogoocGoo003oOolnOolFO03oOoo>Ool00?moocmooaAl0?moolmoo`00ogoo@Goo47`0ogoodGoo003o
Oom2Ool>O03oOooBOol00?moodMoo`Al0?moomMoo`00ogooogooogoo8goo003oOoooOoooOolSOol0
0?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogoo
ogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOooo
OolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=o
o`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003o
OoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomo
oomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo
8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol0
0?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogoo
ogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOooo
OolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=o
o`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003o
OoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomo
oomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo
8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol0
0?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogoo
ogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOooo
OolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=o
o`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003o
OoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomo
oomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo
8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol0
0?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogoo
ogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOooo
OolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=o
o`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003o
OoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomo
oomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo
8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol0
0?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogoo
ogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOooo
OolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=o
o`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003o
OoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomo
oomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo
8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol0
0?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogoo
ogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOooo
OolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=o
o`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003o
OoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomo
oomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo
8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol0
0?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogoo
ogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOooo
OolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=o
o`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003o
OoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomo
oomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo
8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol0
0?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogoo
ogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOooo
OolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=o
o`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003o
OoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomo
oomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo
8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol0
0?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogoo
ogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOooo
OolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=o
o`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003o
OoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomo
oomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo
8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol0
0?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogoo
ogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOooo
OolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=o
o`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003o
OoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomo
oomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo
8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol0
0?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogoo
ogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOooo
OolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=o
o`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003o
OoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomo
oomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo
8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol0
0?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogoo
ogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOooo
OolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=o
o`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003o
OoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomo
oomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo
8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol0
0?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogoo
ogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOooo
OolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=o
o`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003o
OoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomo
oomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo
8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol0
0?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogoo
ogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOooo
OolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=o
o`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003o
OoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomo
oomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo
8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol0
0?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogoo
ogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOooo
OolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=o
o`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003o
OoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomo
oomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo
8goo003oOoooOoooOolSOol00?mooomooomoob=oo`00ogooogooogoo8goo003oOoooOoooOolSOol0
0001\
\>"],
  ImageRangeCache->{{{0, 799}, {499, 0}} -> {-1.72932, -1.08001, 0.0043287, \
0.0043287}}],

Cell[OutputFormData["\<\
Graphics[{{PointSize[0.07], RGBColor[0, 0, 1], Point[{0, -1}]}, \
{PointSize[0.07], RGBColor[1, 0, 0], 
   Point[{(1 - Sqrt[5])/4, -Sqrt[(5 + Sqrt[5])/2]/2}]}}, {AspectRatio -> \
Automatic, PlotRange -> {{-1.08, 1.08}, {-1.08, 1.08}}, 
  Prolog -> {Thickness[0.02], RGBColor[1, 1, 1]}, ImageSize -> {800, 500}}]\
\>", "\<\
-Graphics-\
\>"], "Output"]
}, Open  ]],

Cell[BoxData[
    \(Table[p1[y, 0], {y, 0, 1, 1/100}]\)], "Input"]
},
FrontEndVersion->"4.2 for Microsoft Windows",
ScreenRectangle->{{0, 1024}, {0, 690}},
WindowSize->{1016, 658},
WindowMargins->{{-2, Automatic}, {-19, Automatic}},
StyleDefinitions -> "Classroom.nb"
]

(*******************************************************************
Cached data follows.  If you edit this Notebook file directly, not
using Mathematica, you must remove the line containing CacheID at
the top of  the file.  The cache data will then be recreated when
you save this file from within Mathematica.
*******************************************************************)

(*CellTagsOutline
CellTagsIndex->{}
*)

(*CellTagsIndex
CellTagsIndex->{}
*)

(*NotebookFileOutline
Notebook[{
Cell[1754, 51, 310, 6, 90, "Input"],

Cell[CellGroupData[{
Cell[2089, 61, 43, 1, 50, "Input"],
Cell[2135, 64, 10762, 172, 508, 643, 42, "GraphicsData", "PostScript", \
"Graphics"],
Cell[12900, 238, 375, 8, 47, "Output"]
}, Open  ]],
Cell[13290, 249, 66, 1, 50, "Input"]
}
]
*)



(*******************************************************************
End of Mathematica Notebook file.
*******************************************************************)

