(* : Title : InsertionSortAlgorithm.*)

(* : Authors : Paul Galiatsatos.*)

(* : Context : PaulsTools`InsertionSortAlgorithm`*)

(* : Package Version : 1.0*)

(* : Copyright : No Copyright*)

(* : History : Version 1.0 by Paul Galiatsatos, December 2004.*)

(* : Keywords :*)

(* : Source : None.*)

(* : Warning :*)

(* : Mathematica Version : 5.1*)

(* : Limitation :*)

BeginPackage["PaulsTools`InsertionSortAlgorithm`"]

InsertionSortAlgorithm::usage =
"InsertionSortAlgorithm[MyList_,opts___] sorts the MyList list.
\nThe MyList is consisted of ONLY NUMERICS.
\nIt takes the option ToPrint, which has possibles assignments the 
\nvalues Yes or No."

ToPrint::usage =
"ToPrint is an option for InsertionSortAlgorithm which can 
\nhave the possibles assignments Yes or No.
\nThe Default is Yes.
\nWith the assignment Yes you can see the intermediates steps 
\nof sorting the MyList.
\nYou can see how the algorithm works."

Yes::usage =
"Yes is one of the 2 possibles assignments for option ToPrint."

No::usage =
"No is one of the 2 possibles assignments for option To Print."

InsertionSortAlgorithm::"BadOption" =
"You gave bad option.Try ONLY the rules ToPrint->Yes or ToPrint->No
\nor Sequence[] ...nothing to say."

InsertionSortAlgorithm::"BadElements" =
"The list's elements MUST BE ONLY NUMERICS! Try Again!"

Begin["`Private`"]

Unprotect["PaulsTools`InsertionSortAlgorithm`*"];

Needs["PaulsTools`SwapFunction`"];

Options[InsertionSortAlgorithm] = {ToPrint -> "Yes"};

InsertionSortAlgorithm[list_List, opts___?OptionQ] :=
  Module[
    {i,
      j,
      l = list,
      n = Length[list],
      aa,
      testPrint,
      testOption,
      s = 1},
    
    If[Cases[l, _Symbol] =!= {}, 
      Message[InsertionSortAlgorithm::"BadElements"],
      aa = l;
      
      testOption = ToString[Part[opts, 1]];
      
      testPrint =
        ToString[
          (ToPrint /. {opts} /. Options[InsertionSortAlgorithm])
          ];
      
      Do[
        i = j - 1;
        While[
          i >= 1 && (Part[l, i] > Part[l, i + 1]),
          SwapFunction[l, i, i + 1];
          Which[
            (testPrint == "Yes" && testOption == "ToPrint") || (testOption == \
\
\

                  "1"), If[j == 2, Print[aa]; Print[l], Print[{l, {s}}]],
            testPrint == "No" && testOption == "ToPrint", 2;,
            testPrint =!= ("Yes" || "No") || testOption =!= "ToPrint",
            Message[
              InsertionSortAlgorithm::"BadOption", ToPrint, testPrint
              ];
            Break[]]; s = s + 1; i--
          ],
        {j, 2, n}
        ];
     {l,{s-1}}]
    ]

End[]

Protect["PaulsTools`InsertionSortAlgorithm`*"];

EndPackage[]



