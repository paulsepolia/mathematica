(************** Content-type: application/mathematica **************
                     CreatedBy='Mathematica 4.2'

                    Mathematica-Compatible Notebook

This notebook can be used with any Mathematica-compatible
application, such as Mathematica, MathReader or Publicon. The data
for the notebook starts with the line containing stars above.

To get the notebook into a Mathematica-compatible application, do
one of the following:

* Save the data starting with the line of stars above into a file
  with a name ending in .nb, then open the file inside the
  application;

* Copy the data starting with the line of stars above to the
  clipboard, then use the Paste menu command inside the application.

Data for notebooks contains only printable 7-bit ASCII and can be
sent directly in email or through ftp in text mode.  Newlines can be
CR, LF or CRLF (Unix, Macintosh or MS-DOS style).

NOTE: If you modify the data for this notebook not in a Mathematica-
compatible application, you must delete the line below containing
the word CacheID, otherwise Mathematica-compatible applications may
try to use invalid cache data.

For more information on notebooks and Mathematica-compatible 
applications, contact Wolfram Research:
  web: http://www.wolfram.com
  email: info@wolfram.com
  phone: +1-217-398-0700 (U.S.)

Notebook reader applications are available free of charge from 
Wolfram Research.
*******************************************************************)

(*CacheID: 232*)


(*NotebookFileLineBreakTest
NotebookFileLineBreakTest*)
(*NotebookOptionsPosition[     13793,        339]*)
(*NotebookOutlinePosition[     14473,        362]*)
(*  CellTagsIndexPosition[     14429,        358]*)
(*WindowFrame->Normal*)



Notebook[{
Cell[BoxData[
    \(\(Clear[m, g, kx, ky, l, vx, vy, f, Tinitial, Tfinal];\)\)], "Input"],

Cell[BoxData[
    \(f1[m_, g_, kx_, ky_, l_, vx_, vy_, Tinitial_, Tfinal_] := 
      NDSolve[{m*\(x''\)[t] \[Equal] 
            ky*Abs[\((\((l - x[t])\)^2 + \((l + y[t])\)^2)\)^0.5 - 
                    l]*\((\(-1\))\)*
                Sign[\((\((l - x[t])\)^2 + \((l + y[t])\)^2)\)^0.5 - 
                    l]*\((x[t] - 
                      l)\)/\((\((l - x[t])\)^2 + \((l + y[t])\)^2)\)^0.5 + 
              kx*Abs[\((\((x[t])\)^2 + \((y[t])\)^2)\)^0.5 - l]*
                Sign[\((\((x[t])\)^2 + \((y[t])\)^2)\)^0.5 - l]*\((\(-1\))\)*
                x[t]/\((\((x[t])\)^2 + \((y[t])\)^2)\)^0.5 + m*g, 
          m*\(y''\)[t] \[Equal] 
            ky*Abs[\((\((l - x[t])\)^2 + \((l + y[t])\)^2)\)^0.5 - 
                    l]*\((\(-1\))\)*
                Sign[\((\((l - x[t])\)^2 + \((l + y[t])\)^2)\)^0.5 - 
                    l]*\((y[t] + 
                      l)\)/\((\((l - x[t])\)^2 + \((l + y[t])\)^2)\)^0.5 + 
              kx*Abs[\((\((x[t])\)^2 + \((y[t])\)^2)\)^0.5 - l]*\((\(-1\))\)*
                Sign[\((\((x[t])\)^2 + \((y[t])\)^2)\)^0.5 - l]*
                y[t]/\((\((x[t])\)^2 + \((y[t])\)^2)\)^0.5, x[0] \[Equal] l, 
          y[0] \[Equal] 0, \(x'\)[0] \[Equal] vx, \(y'\)[0] \[Equal] vy}, {x, 
          y}, {t, Tinitial, Tfinal}, MaxSteps \[Rule] 50000000000]\)], "Input"],

Cell[BoxData[
    \(m = 5; g = 0; kx = 1000; ky = 0; l = 0.1; vx = 0; vy = 2; f = 2; 
    Tinitial = 30000; Tfinal = 35000;\)], "Input"],

Cell[BoxData[
    \(\(s1 = f1[m, g, kx, ky, l, vx, vy, Tinitial, Tfinal];\)\)], "Input"],

Cell[BoxData[
    \(Clear[ti, dt]\)], "Input"],

Cell[BoxData[
    \(f2[ti_, dt_] := 
      ParametricPlot[{{x[t], y[t]} /. s1, 
          l*{Cos[2*Pi*f*t], Sin[2*Pi*f*t]}}, {t, ti, ti + dt}, 
        Compiled \[Rule] False, PlotPoints \[Rule] 500, MaxBend \[Rule] 0, 
        PlotStyle \[Rule] {{RGBColor[1, 0, 0], 
              Thickness[0.005]}, {RGBColor[0, 1, 0], Thickness[0.01]}}, 
        PlotRange \[Rule] All, Axes \[Rule] True, 
        AxesLabel \[Rule] {StyleForm["\<x(metre)\>", FontSize \[Rule] 15, 
              FontWeight \[Rule] "\<Bold\>"], 
            StyleForm["\<y(metre)\>", FontSize \[Rule] 15, 
              FontWeight -> "\<Bold\>"]}, AspectRatio \[Rule] Automatic, 
        AxesStyle \[Rule] {{RGBColor[0, 0, 0], 
              Thickness[0.004]}, {RGBColor[0, 0, 0], Thickness[0.004]}}, 
        ImageSize \[Rule] {500, 500}]\)], "Input"],

Cell[BoxData[
    \(Clear[sz, ti, tf, dt]\)], "Input"],

Cell[BoxData[
    \(f3[sz_, ti_, tf_, dt_] := 
      Table[ParametricPlot[{{x[t], y[t]} /. s1, 
            l*{Cos[2*Pi*f*t], Sin[2*Pi*f*t]}}, {t, ti, ti + dt}, 
          Compiled \[Rule] False, PlotPoints \[Rule] 50, MaxBend \[Rule] 0, 
          PlotStyle \[Rule] {{RGBColor[1, 0, 0], 
                Thickness[0.005]}, {RGBColor[0, 1, 0], Thickness[0.01]}}, 
          PlotRange \[Rule] {{\(-sz\), sz}, {\(-sz\), sz}}, 
          Axes \[Rule] False, 
          AxesLabel \[Rule] {StyleForm["\<x(metre)\>", FontSize \[Rule] 15, 
                FontWeight \[Rule] "\<Bold\>"], 
              StyleForm["\<y(metre)\>", FontSize \[Rule] 15, 
                FontWeight -> "\<Bold\>"]}, AspectRatio \[Rule] Automatic, 
          AxesStyle \[Rule] {{RGBColor[0, 0, 0], 
                Thickness[0.004]}, {RGBColor[0, 0, 0], Thickness[0.004]}}, 
          ImageSize \[Rule] {500, 500}], {n, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(\(Clear[X, Y, vX, vY, t];\)\)], "Input"],

Cell[BoxData[
    \(\(X[t_] = x[t] /. First[s1];\)\)], "Input"],

Cell[BoxData[
    \(\(vX[t_] = \(x'\)[t] /. First[s1];\)\)], "Input"],

Cell[BoxData[
    \(\(Y[t_] = y[t] /. First[s1];\)\)], "Input"],

Cell[BoxData[
    \(\(vY[t_] = \(y'\)[t] /. First[s1];\)\)], "Input"],

Cell[BoxData[
    \(\(Clear[s, dt, dS, En, meanEn, ti, dt, V, L, tf];\)\)], "Input"],

Cell[BoxData[
    \(dS[ti_, dt_] := 
      NIntegrate[\((\((vX[t])\)^2 + \((vY[t])\)^2)\)^0.5, {t, ti, ti + dt}, 
        MaxRecursion \[Rule] 50, WorkingPrecision \[Rule] 30, 
        Method \[Rule] QuasiMonteCarlo]\)], "Input"],

Cell[BoxData[
    \(dS1[ti_, dt_] := 
      NIntegrate[\((\((vX[t])\)^2 + \((vY[t])\)^2)\)^0.5, {t, ti, ti + dt}, 
        MinRecursion \[Rule] 5, MaxRecursion \[Rule] 50]\)], "Input"],

Cell[BoxData[
    \(dS2[ti_, dt_] := 
      NIntegrate[\((\((vX[t])\)^2 + \((vY[t])\)^2)\)^0.5, {t, ti, ti + dt}, 
        MaxRecursion \[Rule] 50]\)], "Input"],

Cell[BoxData[
    \(En[t_] := 
      1/2*kx*\((Abs[\((\((X[t])\)^2 + \((Y[t])\)^2)\)^0.5 - l])\)^2*
          Sign[\((\((\((X[t])\)^2 + \((Y[t])\)^2)\)^0.5 - 
                l)\)]*\((\(-1\))\) + 
        1/2*m*\((\((vX[t])\)^2 + \((vY[t])\)^2)\)\)], "Input"],

Cell[BoxData[
    \(V[t_] := \((\((vX[t])\)^2 + \((vY[t])\)^2)\)^0.5\)], "Input"],

Cell[BoxData[
    \(Ekin[t_] := 1/2*m*\((\((vX[t])\)^2 + \((vY[t])\)^2)\)\)], "Input"],

Cell[BoxData[
    \(Edyn[t_, kx_, ky_] := 
      1/2*kx*\((Abs[\((\((X[t])\)^2 + \((Y[t])\)^2)\)^0.5 - l])\)^2 + 
        1/2*ky*\((Abs[\((\((l - X[t])\)^2 + \((l + Y[t])\)^2)\)^0.5 - 
                  l])\)^2\)], "Input"],

Cell[BoxData[
    \(meanEn[ti_, dt_] := 
      NIntegrate[En[t], {t, ti, ti + dt}, MinRecursion \[Rule] 5, 
          MaxRecursion \[Rule] 50, WorkingPrecision \[Rule] 30, 
          Method \[Rule] QuasiMonteCarlo]/\((dt)\)\)], "Input"],

Cell[BoxData[
    \(meanEn1[ti_, dt_] := 
      NIntegrate[En[t], {t, ti, ti + dt}, MinRecursion \[Rule] 5, 
          MaxRecursion \[Rule] 20]/\((dt)\)\)], "Input"],

Cell[BoxData[
    \(meanEn2[ti_, dt_] := 
      NIntegrate[En[t], {t, ti, ti + dt}, 
          MaxRecursion -> 50]/\((dt)\)\)], "Input"],

Cell[BoxData[
    \(L[t_] := X[t]*m*vY[t] - Y[t]*m*vX[t]\)], "Input"],

Cell[BoxData[
    \(meanL[t_, dt_] := 
      NIntegrate[L[n]/dt, {n, t, t + dt}, MinRecursion \[Rule] 5, 
        MaxRecursion \[Rule] 50, WorkingPrecision \[Rule] 30, 
        Method \[Rule] QuasiMonteCarlo]\)], "Input"],

Cell[BoxData[
    \(meanL1[t_, dt_] := 
      NIntegrate[L[n]/dt, {n, t, t + dt}, MinRecursion \[Rule] 5, 
        MaxRecursion \[Rule] 20]\)], "Input"],

Cell[BoxData[
    \(meanL2[t_, dt_] := 
      NIntegrate[L[n]/dt, {n, t, t + dt}, MaxRecursion \[Rule] 50]\)], "Input"],

Cell[BoxData[
    \(f4dS[ti_, tf_, dt_] := Table[dS[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
    \(f4dS[30000, 34900, 100]\)], "Input"],

Cell[BoxData[
    \({27.198873053972218`, 27.18948242495052`, 27.1782369326462`, 
      27.166516060297827`, 27.154779621773486`, 27.143162422553566`, 
      27.133389750676795`, 27.124653938835863`, 27.114180100205683`, 
      27.103878727290944`, 27.093468107890608`, 27.08319026406037`, 
      27.072844735012996`, 27.062593776766004`, 27.052254796868898`, 
      27.041201924688536`, 27.029202659959406`, 27.017362173487157`, 
      27.005465457153274`, 26.995051423695323`, 26.985428246224917`, 
      26.97386927243309`, 26.962416069664652`, 26.950952738160986`, 
      26.941076325975242`, 26.93167837544999`, 26.922308445264424`, 
      26.91302461098857`, 26.902817777894203`, 26.89007211126562`, 
      26.8794716126717`, 26.8700627165178`, 26.859389596955356`, 
      26.848756761002296`, 26.83808824072442`, 26.82749919030386`, 
      26.81695437160343`, 26.806365242076517`, 26.795798436512612`, 
      26.78531969970315`, 26.774835038520276`, 26.764347563626476`, 
      26.75392475840858`, 26.743543233409486`, 26.73314783821124`, 
      26.722755342855848`, 26.71241575045615`, 26.702119751176063`, 
      26.691835456041183`, 26.681547983867556`}\)], "Output"]
}, Open  ]],

Cell[BoxData[
    \(f4dS1[ti_, tf_, dt_] := Table[dS1[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f4dS2[ti_, tf_, dt_] := Table[dS2[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f5meanEn[ti_, tf_, dt_] := 
      Table[meanEn[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
    \(f5meanEn[30000, 34900, 100]\)], "Input"],

Cell[BoxData[
    \({0.17856640416943384`, 0.1784469689656264`, 0.17830433275800261`, 
      0.17815586535193945`, 0.17800634476986604`, 0.17785981519673696`, 
      0.1777352670595088`, 0.1776257077577935`, 0.1774923436481678`, 
      0.17736278629257846`, 0.17723035708325724`, 0.17710114376775343`, 
      0.17696965276295493`, 0.1768408486966285`, 0.1767095473022098`, 
      0.1765706156825849`, 0.17641830017375582`, 0.17626938833783676`, 
      0.17611854979355912`, 0.17598761892266854`, 0.17586641938715578`, 
      0.17572027371764767`, 0.17557651380263056`, 0.17543120787001254`, 
      0.17530760826436592`, 0.1751892386399949`, 0.17507118438475697`, 
      0.17495514175562923`, 0.17482631388295153`, 0.1746658993140328`, 
      0.17453310345431142`, 0.17441447550256356`, 0.17428067600774744`, 
      0.1741475481713069`, 0.17401310678453008`, 0.17388041079166086`, 
      0.17374860831357622`, 0.17361544458093944`, 0.1734828629368484`, 
      0.173352079616229`, 0.17322075618606975`, 0.17308904115268994`, 
      0.1729587351228718`, 0.17282925417798697`, 0.17269916031844562`, 
      0.17256890088001897`, 0.1724397357977921`, 0.1723114458063775`, 
      0.17218310784951918`, 0.1720544580405858`}\)], "Output"]
}, Open  ]],

Cell[BoxData[
    \(f5meanEn1[ti_, tf_, dt_] := 
      Table[meanEn1[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f5meanEn2[ti_, tf_, dt_] := 
      Table[meanEn2[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f6meanL[ti_, tf_, dt_] := 
      Table[meanL[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
    \(f6meanL[30000, 34900, 100]\)], "Input"],

Cell[BoxData[
    \({0.14085040193048487`, 0.14079877464540314`, 0.14073660808085878`, 
      0.14067170386648684`, 0.14060705843665658`, 0.14054252971757214`, 
      0.14048898595238024`, 0.14044053945948964`, 0.1403830472736184`, 
      0.1403259026491225`, 0.14026874459419944`, 0.1402117531464793`, 
      0.14015495212294612`, 0.14009812292380291`, 0.14004135833561254`, 
      0.13998008939217169`, 0.13991407854143187`, 0.13984842112634993`, 
      0.13978293270089134`, 0.13972536393320334`, 0.1396723513307001`, 
      0.13960868382314875`, 0.13954520127853764`, 0.1394822273339109`, 
      0.1394275269783515`, 0.13937588045049698`, 0.13932440628122972`, 
      0.13927305412058794`, 0.13921697696890165`, 0.13914652886057952`, 
      0.1390879362649907`, 0.1390363758148687`, 0.1389774519427918`, 
      0.13891870762656733`, 0.1388601238537757`, 0.13880171292213606`, 
      0.13874343825128227`, 0.13868525343995378`, 0.13862709880167862`, 
      0.1385691839365175`, 0.13851143909991817`, 0.1384538397172829`, 
      0.13839638782869992`, 0.13833906657848927`, 0.13828186145707547`, 
      0.13822477320936805`, 0.13816783114615883`, 0.1381110234263648`, 
      0.13805437738106233`, 0.1379978413034612`}\)], "Output"]
}, Open  ]],

Cell[BoxData[
    \(f6meanL1[ti_, tf_, dt_] := 
      Table[meanL1[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f6meanL2[ti_, tf_, dt_] := 
      Table[meanL2[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f7graphic[t_, sz_] := 
      Graphics[{{Circle[{0, 0}, l], RGBColor[0, 1, 0]}, {PointSize[ .04], 
            RGBColor[1, 0, 0], Point[{X[t], Y[t]}]}}, Axes \[Rule] True, 
        AxesLabel \[Rule] {StyleForm["\<x(metre)\>", FontSize \[Rule] 15, 
              FontWeight \[Rule] "\<Bold\>"], 
            StyleForm["\<y(metre)\>", FontSize -> 15, 
              FontWeight -> "\<Bold\>"]}, AspectRatio \[Rule] Automatic, 
        AxesStyle \[Rule] {{RGBColor[1, 0.5, 0], 
              Thickness[0.01]}, {RGBColor[1, 0.5, 0], Thickness[0.01]}}, 
        PlotRange \[Rule] {{\(-sz\), sz}, {\(-sz\), sz}}, 
        ImageSize \[Rule] {500, 500}, 
        Prolog \[Rule] {RGBColor[0, 1, 0], Thickness[0.02]}]\)], "Input"],

Cell[BoxData[
    \(Clear[ti, tf, dt]\)], "Input"],

Cell[BoxData[
    \(f8table[ti_, tf_, dt_] := 
      Table[Show[f7graphic[t, 0.22]], {t, ti, tf, dt}]\)], "Input"]
},
FrontEndVersion->"4.2 for Microsoft Windows",
ScreenRectangle->{{0, 1024}, {0, 690}},
WindowSize->{1016, 659},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
StyleDefinitions -> "Classroom.nb"
]

(*******************************************************************
Cached data follows.  If you edit this Notebook file directly, not
using Mathematica, you must remove the line containing CacheID at
the top of  the file.  The cache data will then be recreated when
you save this file from within Mathematica.
*******************************************************************)

(*CellTagsOutline
CellTagsIndex->{}
*)

(*CellTagsIndex
CellTagsIndex->{}
*)

(*NotebookFileOutline
Notebook[{
Cell[1754, 51, 89, 1, 50, "Input"],
Cell[1846, 54, 1309, 21, 210, "Input"],
Cell[3158, 77, 136, 2, 50, "Input"],
Cell[3297, 81, 88, 1, 50, "Input"],
Cell[3388, 84, 46, 1, 50, "Input"],
Cell[3437, 87, 820, 14, 150, "Input"],
Cell[4260, 103, 54, 1, 50, "Input"],
Cell[4317, 106, 916, 15, 170, "Input"],
Cell[5236, 123, 60, 1, 50, "Input"],
Cell[5299, 126, 63, 1, 50, "Input"],
Cell[5365, 129, 69, 1, 50, "Input"],
Cell[5437, 132, 63, 1, 50, "Input"],
Cell[5503, 135, 69, 1, 50, "Input"],
Cell[5575, 138, 84, 1, 50, "Input"],
Cell[5662, 141, 229, 4, 70, "Input"],
Cell[5894, 147, 184, 3, 50, "Input"],
Cell[6081, 152, 160, 3, 50, "Input"],
Cell[6244, 157, 259, 5, 70, "Input"],
Cell[6506, 164, 81, 1, 50, "Input"],
Cell[6590, 167, 86, 1, 50, "Input"],
Cell[6679, 170, 223, 4, 50, "Input"],
Cell[6905, 176, 236, 4, 70, "Input"],
Cell[7144, 182, 165, 3, 50, "Input"],
Cell[7312, 187, 136, 3, 50, "Input"],
Cell[7451, 192, 69, 1, 50, "Input"],
Cell[7523, 195, 221, 4, 70, "Input"],
Cell[7747, 201, 152, 3, 50, "Input"],
Cell[7902, 206, 119, 2, 50, "Input"],
Cell[8024, 210, 89, 1, 50, "Input"],

Cell[CellGroupData[{
Cell[8138, 215, 56, 1, 50, "Input"],
Cell[8197, 218, 1177, 17, 106, "Output"]
}, Open  ]],
Cell[9389, 238, 91, 1, 50, "Input"],
Cell[9483, 241, 91, 1, 50, "Input"],
Cell[9577, 244, 104, 2, 50, "Input"],

Cell[CellGroupData[{
Cell[9706, 250, 60, 1, 50, "Input"],
Cell[9769, 253, 1229, 17, 106, "Output"]
}, Open  ]],
Cell[11013, 273, 106, 2, 50, "Input"],
Cell[11122, 277, 106, 2, 50, "Input"],
Cell[11231, 281, 102, 2, 50, "Input"],

Cell[CellGroupData[{
Cell[11358, 287, 59, 1, 50, "Input"],
Cell[11420, 290, 1232, 17, 106, "Output"]
}, Open  ]],
Cell[12667, 310, 104, 2, 50, "Input"],
Cell[12774, 314, 104, 2, 50, "Input"],
Cell[12881, 318, 738, 12, 130, "Input"],
Cell[13622, 332, 50, 1, 50, "Input"],
Cell[13675, 335, 114, 2, 50, "Input"]
}
]
*)



(*******************************************************************
End of Mathematica Notebook file.
*******************************************************************)

