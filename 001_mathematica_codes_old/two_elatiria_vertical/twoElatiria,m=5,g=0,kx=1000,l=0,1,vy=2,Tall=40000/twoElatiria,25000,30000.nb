(************** Content-type: application/mathematica **************
                     CreatedBy='Mathematica 4.2'

                    Mathematica-Compatible Notebook

This notebook can be used with any Mathematica-compatible
application, such as Mathematica, MathReader or Publicon. The data
for the notebook starts with the line containing stars above.

To get the notebook into a Mathematica-compatible application, do
one of the following:

* Save the data starting with the line of stars above into a file
  with a name ending in .nb, then open the file inside the
  application;

* Copy the data starting with the line of stars above to the
  clipboard, then use the Paste menu command inside the application.

Data for notebooks contains only printable 7-bit ASCII and can be
sent directly in email or through ftp in text mode.  Newlines can be
CR, LF or CRLF (Unix, Macintosh or MS-DOS style).

NOTE: If you modify the data for this notebook not in a Mathematica-
compatible application, you must delete the line below containing
the word CacheID, otherwise Mathematica-compatible applications may
try to use invalid cache data.

For more information on notebooks and Mathematica-compatible 
applications, contact Wolfram Research:
  web: http://www.wolfram.com
  email: info@wolfram.com
  phone: +1-217-398-0700 (U.S.)

Notebook reader applications are available free of charge from 
Wolfram Research.
*******************************************************************)

(*CacheID: 232*)


(*NotebookFileLineBreakTest
NotebookFileLineBreakTest*)
(*NotebookOptionsPosition[      9860,        264]*)
(*NotebookOutlinePosition[     10541,        287]*)
(*  CellTagsIndexPosition[     10497,        283]*)
(*WindowFrame->Normal*)



Notebook[{
Cell[BoxData[
    \(\(Clear[m, g, kx, ky, l, vx, vy, f, Tinitial, Tfinal];\)\)], "Input"],

Cell[BoxData[
    \(f1[m_, g_, kx_, ky_, l_, vx_, vy_, Tinitial_, Tfinal_] := 
      NDSolve[{m*\(x''\)[t] \[Equal] 
            ky*Abs[\((\((l - x[t])\)^2 + \((l + y[t])\)^2)\)^0.5 - 
                    l]*\((\(-1\))\)*
                Sign[\((\((l - x[t])\)^2 + \((l + y[t])\)^2)\)^0.5 - 
                    l]*\((x[t] - 
                      l)\)/\((\((l - x[t])\)^2 + \((l + y[t])\)^2)\)^0.5 + 
              kx*Abs[\((\((x[t])\)^2 + \((y[t])\)^2)\)^0.5 - l]*
                Sign[\((\((x[t])\)^2 + \((y[t])\)^2)\)^0.5 - l]*\((\(-1\))\)*
                x[t]/\((\((x[t])\)^2 + \((y[t])\)^2)\)^0.5 + m*g, 
          m*\(y''\)[t] \[Equal] 
            ky*Abs[\((\((l - x[t])\)^2 + \((l + y[t])\)^2)\)^0.5 - 
                    l]*\((\(-1\))\)*
                Sign[\((\((l - x[t])\)^2 + \((l + y[t])\)^2)\)^0.5 - 
                    l]*\((y[t] + 
                      l)\)/\((\((l - x[t])\)^2 + \((l + y[t])\)^2)\)^0.5 + 
              kx*Abs[\((\((x[t])\)^2 + \((y[t])\)^2)\)^0.5 - l]*\((\(-1\))\)*
                Sign[\((\((x[t])\)^2 + \((y[t])\)^2)\)^0.5 - l]*
                y[t]/\((\((x[t])\)^2 + \((y[t])\)^2)\)^0.5, x[0] \[Equal] l, 
          y[0] \[Equal] 0, \(x'\)[0] \[Equal] vx, \(y'\)[0] \[Equal] vy}, {x, 
          y}, {t, Tinitial, Tfinal}, MaxSteps \[Rule] 50000000000]\)], "Input"],

Cell[BoxData[
    \(m = 5; g = 0; kx = 1000; ky = 0; l = 0.1; vx = 0; vy = 2; f = 2; 
    Tinitial = 25000; Tfinal = 30000;\)], "Input"],

Cell[BoxData[
    \(\(s1 = f1[m, g, kx, ky, l, vx, vy, Tinitial, Tfinal];\)\)], "Input"],

Cell[BoxData[
    \(Clear[ti, dt]\)], "Input"],

Cell[BoxData[
    \(f2[ti_, dt_] := 
      ParametricPlot[{{x[t], y[t]} /. s1, 
          l*{Cos[2*Pi*f*t], Sin[2*Pi*f*t]}}, {t, ti, ti + dt}, 
        Compiled \[Rule] False, PlotPoints \[Rule] 500, MaxBend \[Rule] 0, 
        PlotStyle \[Rule] {{RGBColor[1, 0, 0], 
              Thickness[0.005]}, {RGBColor[0, 1, 0], Thickness[0.01]}}, 
        PlotRange \[Rule] All, Axes \[Rule] True, 
        AxesLabel \[Rule] {StyleForm["\<x(metre)\>", FontSize \[Rule] 15, 
              FontWeight \[Rule] "\<Bold\>"], 
            StyleForm["\<y(metre)\>", FontSize \[Rule] 15, 
              FontWeight -> "\<Bold\>"]}, AspectRatio \[Rule] Automatic, 
        AxesStyle \[Rule] {{RGBColor[0, 0, 0], 
              Thickness[0.004]}, {RGBColor[0, 0, 0], Thickness[0.004]}}, 
        ImageSize \[Rule] {500, 500}]\)], "Input"],

Cell[BoxData[
    \(Clear[sz, ti, tf, dt]\)], "Input"],

Cell[BoxData[
    \(f3[sz_, ti_, tf_, dt_] := 
      Table[ParametricPlot[{{x[t], y[t]} /. s1, 
            l*{Cos[2*Pi*f*t], Sin[2*Pi*f*t]}}, {t, ti, ti + dt}, 
          Compiled \[Rule] False, PlotPoints \[Rule] 50, MaxBend \[Rule] 0, 
          PlotStyle \[Rule] {{RGBColor[1, 0, 0], 
                Thickness[0.005]}, {RGBColor[0, 1, 0], Thickness[0.01]}}, 
          PlotRange \[Rule] {{\(-sz\), sz}, {\(-sz\), sz}}, 
          Axes \[Rule] False, 
          AxesLabel \[Rule] {StyleForm["\<x(metre)\>", FontSize \[Rule] 15, 
                FontWeight \[Rule] "\<Bold\>"], 
              StyleForm["\<y(metre)\>", FontSize \[Rule] 15, 
                FontWeight -> "\<Bold\>"]}, AspectRatio \[Rule] Automatic, 
          AxesStyle \[Rule] {{RGBColor[0, 0, 0], 
                Thickness[0.004]}, {RGBColor[0, 0, 0], Thickness[0.004]}}, 
          ImageSize \[Rule] {500, 500}], {n, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(\(Clear[X, Y, vX, vY, t];\)\)], "Input"],

Cell[BoxData[
    \(\(X[t_] = x[t] /. First[s1];\)\)], "Input"],

Cell[BoxData[
    \(\(vX[t_] = \(x'\)[t] /. First[s1];\)\)], "Input"],

Cell[BoxData[
    \(\(Y[t_] = y[t] /. First[s1];\)\)], "Input"],

Cell[BoxData[
    \(\(vY[t_] = \(y'\)[t] /. First[s1];\)\)], "Input"],

Cell[BoxData[
    \(\(Clear[s, dt, dS, En, meanEn, ti, dt, V, L, tf];\)\)], "Input"],

Cell[BoxData[
    \(dS[ti_, dt_] := 
      NIntegrate[\((\((vX[t])\)^2 + \((vY[t])\)^2)\)^0.5, {t, ti, ti + dt}, 
        MaxRecursion \[Rule] 50, WorkingPrecision \[Rule] 30, 
        Method \[Rule] QuasiMonteCarlo]\)], "Input"],

Cell[BoxData[
    \(dS1[ti_, dt_] := 
      NIntegrate[\((\((vX[t])\)^2 + \((vY[t])\)^2)\)^0.5, {t, ti, ti + dt}, 
        MinRecursion \[Rule] 5, MaxRecursion \[Rule] 50]\)], "Input"],

Cell[BoxData[
    \(dS2[ti_, dt_] := 
      NIntegrate[\((\((vX[t])\)^2 + \((vY[t])\)^2)\)^0.5, {t, ti, ti + dt}, 
        MaxRecursion \[Rule] 50]\)], "Input"],

Cell[BoxData[
    \(En[t_] := 
      1/2*kx*\((Abs[\((\((X[t])\)^2 + \((Y[t])\)^2)\)^0.5 - l])\)^2*
          Sign[\((\((\((X[t])\)^2 + \((Y[t])\)^2)\)^0.5 - 
                l)\)]*\((\(-1\))\) + 
        1/2*m*\((\((vX[t])\)^2 + \((vY[t])\)^2)\)\)], "Input"],

Cell[BoxData[
    \(V[t_] := \((\((vX[t])\)^2 + \((vY[t])\)^2)\)^0.5\)], "Input"],

Cell[BoxData[
    \(Ekin[t_] := 1/2*m*\((\((vX[t])\)^2 + \((vY[t])\)^2)\)\)], "Input"],

Cell[BoxData[
    \(Edyn[t_, kx_, ky_] := 
      1/2*kx*\((Abs[\((\((X[t])\)^2 + \((Y[t])\)^2)\)^0.5 - l])\)^2 + 
        1/2*ky*\((Abs[\((\((l - X[t])\)^2 + \((l + Y[t])\)^2)\)^0.5 - 
                  l])\)^2\)], "Input"],

Cell[BoxData[
    \(meanEn[ti_, dt_] := 
      NIntegrate[En[t], {t, ti, ti + dt}, MinRecursion \[Rule] 5, 
          MaxRecursion \[Rule] 50, WorkingPrecision \[Rule] 30, 
          Method \[Rule] QuasiMonteCarlo]/\((dt)\)\)], "Input"],

Cell[BoxData[
    \(meanEn1[ti_, dt_] := 
      NIntegrate[En[t], {t, ti, ti + dt}, MinRecursion \[Rule] 5, 
          MaxRecursion \[Rule] 20]/\((dt)\)\)], "Input"],

Cell[BoxData[
    \(meanEn2[ti_, dt_] := 
      NIntegrate[En[t], {t, ti, ti + dt}, 
          MaxRecursion -> 50]/\((dt)\)\)], "Input"],

Cell[BoxData[
    \(L[t_] := X[t]*m*vY[t] - Y[t]*m*vX[t]\)], "Input"],

Cell[BoxData[
    \(meanL[t_, dt_] := 
      NIntegrate[L[n]/dt, {n, t, t + dt}, MinRecursion \[Rule] 5, 
        MaxRecursion \[Rule] 50, WorkingPrecision \[Rule] 30, 
        Method \[Rule] QuasiMonteCarlo]\)], "Input"],

Cell[BoxData[
    \(meanL1[t_, dt_] := 
      NIntegrate[L[n]/dt, {n, t, t + dt}, MinRecursion \[Rule] 5, 
        MaxRecursion \[Rule] 20]\)], "Input"],

Cell[BoxData[
    \(meanL2[t_, dt_] := 
      NIntegrate[L[n]/dt, {n, t, t + dt}, MaxRecursion \[Rule] 50]\)], "Input"],

Cell[BoxData[
    \(f4dS[ti_, tf_, dt_] := Table[dS[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f4dS1[ti_, tf_, dt_] := Table[dS1[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f4dS2[ti_, tf_, dt_] := Table[dS2[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f5meanEn[ti_, tf_, dt_] := 
      Table[meanEn[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f5meanEn1[ti_, tf_, dt_] := 
      Table[meanEn1[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f5meanEn2[ti_, tf_, dt_] := 
      Table[meanEn2[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f6meanL[ti_, tf_, dt_] := 
      Table[meanL[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f6meanL1[ti_, tf_, dt_] := 
      Table[meanL1[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f6meanL2[ti_, tf_, dt_] := 
      Table[meanL2[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f7graphic[t_, sz_] := 
      Graphics[{{Circle[{0, 0}, l], RGBColor[0, 1, 0]}, {PointSize[ .04], 
            RGBColor[1, 0, 0], Point[{X[t], Y[t]}]}}, Axes \[Rule] True, 
        AxesLabel \[Rule] {StyleForm["\<x(metre)\>", FontSize \[Rule] 15, 
              FontWeight \[Rule] "\<Bold\>"], 
            StyleForm["\<y(metre)\>", FontSize -> 15, 
              FontWeight -> "\<Bold\>"]}, AspectRatio \[Rule] Automatic, 
        AxesStyle \[Rule] {{RGBColor[1, 0.5, 0], 
              Thickness[0.01]}, {RGBColor[1, 0.5, 0], Thickness[0.01]}}, 
        PlotRange \[Rule] {{\(-sz\), sz}, {\(-sz\), sz}}, 
        ImageSize \[Rule] {500, 500}, 
        Prolog \[Rule] {RGBColor[0, 1, 0], Thickness[0.02]}]\)], "Input"],

Cell[BoxData[
    \(Clear[ti, tf, dt]\)], "Input"],

Cell[BoxData[
    \(f8table[ti_, tf_, dt_] := 
      Table[Show[f7graphic[t, 0.22]], {t, ti, tf, dt}]\)], "Input"]
},
FrontEndVersion->"4.2 for Microsoft Windows",
ScreenRectangle->{{0, 1024}, {0, 690}},
WindowSize->{1016, 659},
WindowMargins->{{-6, Automatic}, {Automatic, 5}},
StyleDefinitions -> "Classroom.nb"
]

(*******************************************************************
Cached data follows.  If you edit this Notebook file directly, not
using Mathematica, you must remove the line containing CacheID at
the top of  the file.  The cache data will then be recreated when
you save this file from within Mathematica.
*******************************************************************)

(*CellTagsOutline
CellTagsIndex->{}
*)

(*CellTagsIndex
CellTagsIndex->{}
*)

(*NotebookFileOutline
Notebook[{
Cell[1754, 51, 89, 1, 50, "Input"],
Cell[1846, 54, 1309, 21, 210, "Input"],
Cell[3158, 77, 136, 2, 50, "Input"],
Cell[3297, 81, 88, 1, 50, "Input"],
Cell[3388, 84, 46, 1, 50, "Input"],
Cell[3437, 87, 820, 14, 150, "Input"],
Cell[4260, 103, 54, 1, 50, "Input"],
Cell[4317, 106, 916, 15, 170, "Input"],
Cell[5236, 123, 60, 1, 50, "Input"],
Cell[5299, 126, 63, 1, 50, "Input"],
Cell[5365, 129, 69, 1, 50, "Input"],
Cell[5437, 132, 63, 1, 50, "Input"],
Cell[5503, 135, 69, 1, 50, "Input"],
Cell[5575, 138, 84, 1, 50, "Input"],
Cell[5662, 141, 229, 4, 70, "Input"],
Cell[5894, 147, 184, 3, 50, "Input"],
Cell[6081, 152, 160, 3, 50, "Input"],
Cell[6244, 157, 259, 5, 70, "Input"],
Cell[6506, 164, 81, 1, 50, "Input"],
Cell[6590, 167, 86, 1, 50, "Input"],
Cell[6679, 170, 223, 4, 50, "Input"],
Cell[6905, 176, 236, 4, 70, "Input"],
Cell[7144, 182, 165, 3, 50, "Input"],
Cell[7312, 187, 136, 3, 50, "Input"],
Cell[7451, 192, 69, 1, 50, "Input"],
Cell[7523, 195, 221, 4, 70, "Input"],
Cell[7747, 201, 152, 3, 50, "Input"],
Cell[7902, 206, 119, 2, 50, "Input"],
Cell[8024, 210, 89, 1, 50, "Input"],
Cell[8116, 213, 91, 1, 50, "Input"],
Cell[8210, 216, 91, 1, 50, "Input"],
Cell[8304, 219, 104, 2, 50, "Input"],
Cell[8411, 223, 106, 2, 50, "Input"],
Cell[8520, 227, 106, 2, 50, "Input"],
Cell[8629, 231, 102, 2, 50, "Input"],
Cell[8734, 235, 104, 2, 50, "Input"],
Cell[8841, 239, 104, 2, 50, "Input"],
Cell[8948, 243, 738, 12, 130, "Input"],
Cell[9689, 257, 50, 1, 50, "Input"],
Cell[9742, 260, 114, 2, 50, "Input"]
}
]
*)



(*******************************************************************
End of Mathematica Notebook file.
*******************************************************************)

