(************** Content-type: application/mathematica **************
                     CreatedBy='Mathematica 4.2'

                    Mathematica-Compatible Notebook

This notebook can be used with any Mathematica-compatible
application, such as Mathematica, MathReader or Publicon. The data
for the notebook starts with the line containing stars above.

To get the notebook into a Mathematica-compatible application, do
one of the following:

* Save the data starting with the line of stars above into a file
  with a name ending in .nb, then open the file inside the
  application;

* Copy the data starting with the line of stars above to the
  clipboard, then use the Paste menu command inside the application.

Data for notebooks contains only printable 7-bit ASCII and can be
sent directly in email or through ftp in text mode.  Newlines can be
CR, LF or CRLF (Unix, Macintosh or MS-DOS style).

NOTE: If you modify the data for this notebook not in a Mathematica-
compatible application, you must delete the line below containing
the word CacheID, otherwise Mathematica-compatible applications may
try to use invalid cache data.

For more information on notebooks and Mathematica-compatible 
applications, contact Wolfram Research:
  web: http://www.wolfram.com
  email: info@wolfram.com
  phone: +1-217-398-0700 (U.S.)

Notebook reader applications are available free of charge from 
Wolfram Research.
*******************************************************************)

(*CacheID: 232*)


(*NotebookFileLineBreakTest
NotebookFileLineBreakTest*)
(*NotebookOptionsPosition[     19628,        577]*)
(*NotebookOutlinePosition[     20418,        604]*)
(*  CellTagsIndexPosition[     20374,        600]*)
(*WindowFrame->Normal*)



Notebook[{
Cell[BoxData[
    \(p[n_, fa_, fd_] := 
      Graphics[{{PointSize[ .04], RGBColor[0, 0, 1], 
            Point[{2 + Sin[\((2*Pi*fd)\)*n] + 
                  2*Sin[2*Pi/2*\((fa - fd)\)*n]*Cos[2*Pi/2*\((fa + fd)\)*n], 
                0}]}, {PointSize[ .04], RGBColor[0, 0, 0], 
            Point[{\(-2\) + Sin[\((\(-2\)*Pi*fa)\)*n] + 
                  2*Sin[2*Pi/2*\((fa - fd)\)*n]*Cos[2*Pi/2*\((fa + fd)\)*n], 
                0}]}}, Axes -> {True, False}, 
        AxesLabel \[Rule] {StyleForm["\<x(metre)\>", FontSize \[Rule] 15, 
              FontWeight \[Rule] "\<Bold\>"], StyleForm[q]}, 
        AspectRatio \[Rule] 1/9, 
        AxesStyle \[Rule] {{RGBColor[1, 0.5, 0], Thickness[0.01]}, {}}, 
        PlotRange \[Rule] {{\(-4\), 4}, {\(-5\), 5}}, 
        ImageSize \[Rule] {700, 100}]\)], "Input"],

Cell[BoxData[
    \(\(pc = 
        Graphics[{Line[{{0, \(-5\)}, {0, \(+5\)}}], 
            Line[{{2, \(+5\)}, {2, \(-5\)}}], 
            Line[{{\(-2\), \(+5\)}, {\(-2\), \(-5\)}}]}, 
          AspectRatio \[Rule] 1/9, 
          Prolog \[Rule] {Thickness[0.01], RGBColor[1, 0, 0]}];\)\)], "Input"],

Cell[BoxData[
    \(pa[n_, fa_, fd_] := 
      Graphics[{{PointSize[ .08], RGBColor[1, 1, 0], 
            Point[{2*Sin[2*Pi/2*\((fa - fd)\)*n]*Cos[2*Pi/2*\((fa + fd)\)*n], 
                0}]}, {PointSize[ .04], RGBColor[0, 1, 1], 
            Point[{2*Sin[2*Pi/2*\((fa - fd)\)*n]*Cos[2*Pi/2*\((fa + fd)\)*n], 
                0}]}, Line[{{2*Sin[2*Pi/2*\((fa - fd)\)*n]*
                  Cos[2*Pi/2*\((fa + fd)\)*n], \(-5\)}, {2*
                  Sin[2*Pi/2*\((fa - fd)\)*n]*
                  Cos[2*Pi/2*\((fa + fd)\)*n], \(+5\)}}]}, 
        Axes -> {True, False}, 
        AxesLabel \[Rule] {StyleForm["\<x(metre)\>", FontSize \[Rule] 15, 
              FontWeight \[Rule] "\<Bold\>"], StyleForm[q]}, 
        AspectRatio \[Rule] 1/9, 
        AxesStyle \[Rule] {{RGBColor[1, 0.5, 0], Thickness[0.01]}, {}}, 
        PlotRange \[Rule] {{\(-4\), 4}, {\(-5\), 5}}, 
        ImageSize \[Rule] {700, 100}, 
        Prolog \[Rule] {Thickness[0.01], RGBColor[1, 0, 0]}]\)], "Input"],

Cell[BoxData[
    \(pb[n_, fa_, fd_] := 
      Graphics[{PointSize[ .04], RGBColor[0, 1, 1], 
          Point[{2*Sin[2*Pi/2*\((fa - fd)\)*n]*Cos[2*Pi/2*\((fa + fd)\)*n], 
              0}]}, Axes -> {True, False}, 
        AxesLabel \[Rule] {StyleForm["\<x(metre)\>", FontSize \[Rule] 15, 
              FontWeight \[Rule] "\<Bold\>"], StyleForm[q]}, 
        AspectRatio \[Rule] 1/9, 
        AxesStyle \[Rule] {{RGBColor[1, 0.5, 0], Thickness[0.01]}, {}}, 
        PlotRange \[Rule] {{\(-4\), 4}, {\(-5\), 5}}, 
        ImageSize \[Rule] {700, 100}, 
        Prolog \[Rule] {Thickness[0.01], RGBColor[1, 0.5, 0]}]\)], "Input"],

Cell[BoxData[
    \(g1[n_, fa_, fd_] := 
      Graphics[{{Thickness[
              0.030 - Abs[\((2*Sin[2*Pi/2*\((fa - fd)\)*n]*
                          Cos[2*Pi/2*\((fa + fd)\)*n] - \((2 + 
                            Sin[\((2*Pi*fd)\)*n] + 
                            2*Sin[2*Pi/2*\((fa - fd)\)*n]*
                              Cos[2*Pi/2*\((fa + fd)\)*n])\))\)]/120], 
            RGBColor[0, 0.5, 0.9], 
            Line[{{2*Sin[2*Pi/2*\((fa - fd)\)*n]*Cos[2*Pi/2*\((fa + fd)\)*n], 
                  0}, {2 + Sin[\((2*Pi*fd)\)*n] + 
                    2*Sin[2*Pi/2*\((fa - fd)\)*n]*
                      Cos[2*Pi/2*\((fa + fd)\)*n], 0}}]}}, 
        Axes -> {True, False}, 
        AxesLabel \[Rule] {StyleForm["\<x(metre)\>", FontSize \[Rule] 15, 
              FontWeight \[Rule] "\<Bold\>"], StyleForm[q]}, 
        AspectRatio \[Rule] 1/9, 
        AxesStyle \[Rule] {{RGBColor[1, 0.5, 0], Thickness[0.01]}, {}}, 
        PlotRange \[Rule] {{\(-4\), 4}, {\(-5\), 5}}, 
        ImageSize \[Rule] {700, 100}, 
        Prolog \[Rule] {Thickness[0.01], RGBColor[1, 0, 0]}]\)], "Input"],

Cell[BoxData[
    \(ga1[n_, fa_, fd_] := 
      Graphics[{{Thickness[
              0.030 - Abs[\((2*Sin[2*Pi/2*\((fa - fd)\)*n]*
                          Cos[2*Pi/2*\((fa + fd)\)*n] - \((\(-2\) + 
                            Sin[\((\(-2\)*Pi*fa)\)*n] + 
                            2*Sin[2*Pi/2*\((fa - fd)\)*n]*
                              Cos[2*Pi/2*\((fa + fd)\)*n])\))\)]/120], 
            RGBColor[0, 1, 0], 
            Line[{{2*Sin[2*Pi/2*\((fa - fd)\)*n]*Cos[2*Pi/2*\((fa + fd)\)*n], 
                  0}, {\(-2\) + Sin[\((\(-2\)*Pi*fa)\)*n] + 
                    2*Sin[2*Pi/2*\((fa - fd)\)*n]*
                      Cos[2*Pi/2*\((fa + fd)\)*n], 0}}]}}, 
        Axes -> {True, False}, 
        AxesLabel \[Rule] {StyleForm["\<x(metre)\>", FontSize \[Rule] 15, 
              FontWeight \[Rule] "\<Bold\>"], StyleForm[q]}, 
        AspectRatio \[Rule] 1/9, 
        AxesStyle \[Rule] {{RGBColor[1, 0.5, 0], Thickness[0.01]}, {}}, 
        PlotRange \[Rule] {{\(-4\), 4}, {\(-5\), 5}}, 
        ImageSize \[Rule] {700, 100}, 
        Prolog \[Rule] {Thickness[0.01], RGBColor[1, 0, 0]}]\)], "Input"],

Cell[BoxData[
    \(fa = 900; fd = 800; k = 1/10;\)], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
    \(Show[{{pa[k, fa, fd]}, {g1[k, fa, fd]}, {ga1[k, fa, fd]}, {p[k, fa, 
            fd]}, {pb[k, fa, fd]}, {pc}}]\)], "Input"],

Cell[GraphicsData["PostScript", "\<\
%!
%%Creator: Mathematica
%%AspectRatio: .11111 
%%ImageSize: 700 100 
MathPictureStart
/Mabs {
Mgmatrix idtransform
Mtmatrix dtransform
} bind def
/Mabsadd { Mabs
3 -1 roll add
3 1 roll add
exch } bind def
%% Graphics
%%IncludeResource: font Courier
%%IncludeFont: Courier
/Courier findfont 10  scalefont  setfont
% Scaling calculations
0.5 0.125 0.0555556 0.0111111 [
[0 .04306 -6 -9 ]
[0 .04306 6 0 ]
[.125 .04306 -6 -9 ]
[.125 .04306 6 0 ]
[.25 .04306 -6 -9 ]
[.25 .04306 6 0 ]
[.375 .04306 -6 -9 ]
[.375 .04306 6 0 ]
[.5 .04306 -3 -9 ]
[.5 .04306 3 0 ]
[.625 .04306 -3 -9 ]
[.625 .04306 3 0 ]
[.75 .04306 -3 -9 ]
[.75 .04306 3 0 ]
[.875 .04306 -3 -9 ]
[.875 .04306 3 0 ]
[1 .04306 -3 -9 ]
[1 .04306 3 0 ]
[1.025 .05556 0 -7.0625 ]
[1.025 .05556 76.3125 7.0625 ]
[ -0.005 0 0 0 ]
[ 1.005 .11111 0 0 ]
] MathScale
% Start of Graphics
1 setlinecap
1 setlinejoin
newpath
0 g
.25 Mabswid
[ ] 0 setdash
0 .05556 m
0 .06181 L
s
[(-4)] 0 .04306 0 1 Mshowa
.125 .05556 m
.125 .06181 L
s
[(-3)] .125 .04306 0 1 Mshowa
.25 .05556 m
.25 .06181 L
s
[(-2)] .25 .04306 0 1 Mshowa
.375 .05556 m
.375 .06181 L
s
[(-1)] .375 .04306 0 1 Mshowa
.5 .05556 m
.5 .06181 L
s
[(0)] .5 .04306 0 1 Mshowa
.625 .05556 m
.625 .06181 L
s
[(1)] .625 .04306 0 1 Mshowa
.75 .05556 m
.75 .06181 L
s
[(2)] .75 .04306 0 1 Mshowa
.875 .05556 m
.875 .06181 L
s
[(3)] .875 .04306 0 1 Mshowa
1 .05556 m
1 .06181 L
s
[(4)] 1 .04306 0 1 Mshowa
.125 Mabswid
.025 .05556 m
.025 .05931 L
s
.05 .05556 m
.05 .05931 L
s
.075 .05556 m
.075 .05931 L
s
.1 .05556 m
.1 .05931 L
s
.15 .05556 m
.15 .05931 L
s
.175 .05556 m
.175 .05931 L
s
.2 .05556 m
.2 .05931 L
s
.225 .05556 m
.225 .05931 L
s
.275 .05556 m
.275 .05931 L
s
.3 .05556 m
.3 .05931 L
s
.325 .05556 m
.325 .05931 L
s
.35 .05556 m
.35 .05931 L
s
.4 .05556 m
.4 .05931 L
s
.425 .05556 m
.425 .05931 L
s
.45 .05556 m
.45 .05931 L
s
.475 .05556 m
.475 .05931 L
s
.525 .05556 m
.525 .05931 L
s
.55 .05556 m
.55 .05931 L
s
.575 .05556 m
.575 .05931 L
s
.6 .05556 m
.6 .05931 L
s
.65 .05556 m
.65 .05931 L
s
.675 .05556 m
.675 .05931 L
s
.7 .05556 m
.7 .05931 L
s
.725 .05556 m
.725 .05931 L
s
.775 .05556 m
.775 .05931 L
s
.8 .05556 m
.8 .05931 L
s
.825 .05556 m
.825 .05931 L
s
.85 .05556 m
.85 .05931 L
s
.9 .05556 m
.9 .05931 L
s
.925 .05556 m
.925 .05931 L
s
.95 .05556 m
.95 .05931 L
s
.975 .05556 m
.975 .05931 L
s
1 .5 0 r
.01 w
0 .05556 m
1 .05556 L
s
0 g
gsave
1.025 .05556 -61 -11.0625 Mabsadd m
1 1 Mabs scale
currentpoint translate
0 22.125 translate 1 -1 scale
/g { setgray} bind def
/k { setcmykcolor} bind def
/p { gsave} bind def
/r { setrgbcolor} bind def
/w { setlinewidth} bind def
/C { curveto} bind def
/F { fill} bind def
/L { lineto} bind def
/rL { rlineto} bind def
/P { grestore} bind def
/s { stroke} bind def
/S { show} bind def
/N {currentpoint 3 -1 roll show moveto} bind def
/Msf { findfont exch scalefont [1 0 0 -1 0 0 ] makefont setfont} bind def
/m { moveto} bind def
/Mr { rmoveto} bind def
/Mx {currentpoint exch pop moveto} bind def
/My {currentpoint pop exch moveto} bind def
/X {0 rmoveto} bind def
/Y {0 exch rmoveto} bind def
63.000 14.375 moveto
%%IncludeResource: font Courier
%%IncludeFont: Courier
/Courier findfont 15.000 scalefont
[1 0 0 -1 0 0 ] makefont setfont
0.000 0.000 0.000 setrgbcolor
0.000 0.000 rmoveto
63.000 14.375 moveto
%%IncludeResource: font Courier
%%IncludeFont: Courier
/Courier findfont 15.000 scalefont
[1 0 0 -1 0 0 ] makefont setfont
0.000 0.000 0.000 setrgbcolor
(x) show
%%IncludeResource: font Mathematica2Mono-Bold
%%IncludeFont: Mathematica2Mono-Bold
/Mathematica2Mono-Bold findfont 15.000 scalefont
[1 0 0 -1 0 0 ] makefont setfont
0.000 0.000 0.000 setrgbcolor
72.000 14.375 moveto
(H) show
81.313 14.375 moveto
%%IncludeResource: font Courier
%%IncludeFont: Courier
/Courier findfont 15.000 scalefont
[1 0 0 -1 0 0 ] makefont setfont
0.000 0.000 0.000 setrgbcolor
(metre) show
%%IncludeResource: font Mathematica2Mono-Bold
%%IncludeFont: Mathematica2Mono-Bold
/Mathematica2Mono-Bold findfont 15.000 scalefont
[1 0 0 -1 0 0 ] makefont setfont
0.000 0.000 0.000 setrgbcolor
126.313 14.375 moveto
(L) show
135.313 14.375 moveto
%%IncludeResource: font Courier
%%IncludeFont: Courier
/Courier findfont 15.000 scalefont
[1 0 0 -1 0 0 ] makefont setfont
0.000 0.000 0.000 setrgbcolor
0.000 0.000 rmoveto
1.000 setlinewidth
grestore
0 0 m
1 0 L
1 .11111 L
0 .11111 L
closepath
clip
newpath
1 1 0 r
.08 w
.5 .05556 Mdot
0 1 1 r
.04 w
.5 .05556 Mdot
1 0 0 r
.01 w
.5 0 m
.5 .11111 L
s
0 .5 .9 r
.01333 w
.5 .05556 m
.75 .05556 L
s
0 1 0 r
.5 .05556 m
.25 .05556 L
s
0 0 1 r
.04 w
.75 .05556 Mdot
0 0 0 r
.25 .05556 Mdot
0 1 1 r
.5 .05556 Mdot
1 0 0 r
.01 w
.5 0 m
.5 .11111 L
s
.75 .11111 m
.75 0 L
s
.25 .11111 m
.25 0 L
s
% End of Graphics
MathPictureEnd
\
\>"], "Graphics",
  ImageSize->{700, 100},
  ImageCache->GraphicsData["Bitmap", "\<\
CF5dJ6E]HGAYHf4PAg9QL6QYHg<PAVmbKF5d0`4000=[0000OA000`40O003h00OogooogooogooKWoo
003oOoooOoooOom^Ool00?mooomooomoofioo`00ogooogooogooKWoo003oOoooOoooOom^Ool00?mo
oomooomoofioo`00ogooogooogooKWoo003oOoooOoooOom^Ool00?mooomooomoofioo`00ogooogoo
ogooKWoo003oOoooOoooOom^Ool00?mooomooomoofioo`00ogooogooogooKWoo003oOoooOoooOom^
Ool00?mooomooomoofioo`00ogooogooogooKWoo003oOoooOoooOom^Ool00?mooomooomoofioo`00
`7oo1g`0]Goo1g`0]Goo1g`0ogoo;Goo0030Ool7O02eOol7O02eOol7O03oOol]Ool00<1oo`Ml0;Eo
o`Ml0;Eoo`Ml0?moobeoo`00`7oo1g`0]Goo1g`0]Goo1g`0ogoo;Goo0030Ool7O02eOol7O02eOol7
O03oOol]Ool00<1oo`Ml0;Eoo`Ml0;Eoo`Ml0?moobeoo`00`7oo1g`0]Goo1g`0]Goo1g`0ogoo;Goo
0030Ool7O02eOol7O02eOol7O03oOol]Ool00<1oo`Ml0;Eoo`Ml0;Eoo`Ml0?moobeoo`00`7oo1g`0
]Goo1g`0]Goo1g`0ogoo;Goo0030Ool7O02eOol7O02eOol7O03oOol]Ool00<1oo`Ml0;Eoo`Ml0;Eo
o`Ml0?moobeoo`00`7oo1g`0]Goo1g`0]Goo1g`0ogoo;Goo0030Ool7O02eOol7O02eOol7O03oOol]
Ool00<1oo`Ml0;Eoo`Ml0;Eoo`Ml0?moobeoo`00`7oo1g`0]7oo00=oh7`0O0001G`00WoP/goo1g`0
ogoo;Goo0030Ool7O02`Ool5On07O006On2_Ool7O03oOol]Ool00<1oo`Ml0:eoo`Qoh0Ml00Uoh:ao
o`Ml0?moobeoo`00`7oo1g`0Zgoo2WoP1g`02goPZWoo1g`0ogoo;Goo0030Ool7O02ZOol;On07O00<
On2YOol7O03oOol]Ool00<1oo`Ml0:Qoo`eoh0Ml00ioh:Moo`Ml0?moobeoo`00`7oo1g`0Ygoo3WoP
1g`03goPYWoo1g`0ogoo;Goo0030Ool7O02VOol?On07O00@On2UOol7O03oOol]Ool00<1oo`Ml0:Ao
oa5oh0Ml019oh:=oo`Ml0?moobeoo`002goo0`00FGoo0`00EWoo1g`00P00FWoo1@00@Woo4WoP1g`0
4goPAGoo1@00F7oo1g`0F7oo0`00GGoo0`00LGoo000<Ool00`00Oomoo`1GOol01@00Oomoogoo0000
EGoo1g`000=oo`00Ool0Fgoo00<007ooOol0@Woo4WoP1g`04goPAgoo00<007ooOol0F7oo1g`0Egoo
00D007ooOomoo`0005eoo`03001oogoo071oo`0037oo00<007ooOol0Fgoo00<007ooOol0Dgoo1g`0
GWoo00<007ooOol0@Goo4goP1g`057oPAWoo00<007ooOol0F7oo1g`0Fgoo00<007ooOol0Fgoo00<0
07ooOol0L7oo00060002Ool6001@Ool60006Ool00`00Oomoo`1?Ool40007O01COol60005Ool00`00
Oomoo`10OolDOn07O00EOn15Ool00`00Oomoo`1HOol7O01KOol00`00Oomoo`1GOol6001aOol000Uo
o`04001oogoo001KOol2001FOol7O0000`00Oomoo`1KOol00`00Oomoo`0oOolEOn07O00FOn14Ool0
0`00Oomoo`1HOol7O01IOol2001KOol01000Oomoo`00LWoo0009Ool01000Oomoo`00GGoo00<007oo
Ool0Dgoo1g`000=oo`00Ool0Fgoo00<007ooOol0?goo5GoP1g`05WoPA7oo00<007ooOol0F7oo1g`0
Fgoo00<007ooOol0F7oo00@007ooOol0079oo`002Woo00<007oo0000FGoo00D007ooOomoo`0005Eo
o`Ml0003Ool007oo05Uoo`<0041ooaIoh0Ml01Moh45oo`<005Yoo`Ml05Moo`05001oogooOol0001K
Ool00`00Ool0001bOol000]oo`8005Yoo`<005Aoo`8000Ml00<005]oo`03001oogoo03eooaEoh083
o`Ml00<3oaEoh49oo`03001oogoo05Ioo`807`Ml00<07eEoo`<005eoo`80079oo`00_7oo10001g`0
1@00VGoo4goP10?o1g`01@?o4goPVGoo100O1g`01@0Oogoo:7oo002kOol50007O006002GOolCOn05
0ol7O0060olCOn2GOol501l7O00601ooOolWOol00;Yoo`H000Ml00L009Iooa9oh0H3o`Ml00L3oa9o
h9Ioo`H07`Ml00L07omoobIoo`00^Goo1`001g`02000UGoo4GoP1`?o1g`020?o4GoPUGoo1`0O1g`0
200Oogoo9Goo002hOol80007O009002COolAOn080ol7O0090olAOn2COol801l7O00901ooOolTOol0
0;Moo`T000Ml00X0099ooa1oh0T3o`Ml00X3oa1oh99oo`T07`Ml00X07mIoo`03JeX004Ha03]oo`03
AS4006]J00]oo`00]goo2@001g`02P00TWoo47oP2@?o1g`02P?o47oPTWoo2@0O1g`02P0OeWoo00<a
S000Mkd0>goo00=g_@00<H`02goo002fOol:0007O00;002AOol?On0:0ol7O00;0ol?On2AOol:01l7
O00;01oDOol00ekg001BU00mOol00e:D001Nm`0:Ool00;Ioo`X000Ml00/0091ooa1oh0X3o`Ml00/3
oa1oh91oo`X07`Ml00/07mAoo`039BT007Nm03eoo`03Mkd002DY00Yoo`00]Woo2P001g`02`00X0?P
2P?o1g`02`?oX07l2P0O1g`02`0Oa7oo0`000goo0`001Woo00=W>@00DY@00goo0`000Woo0P000Woo
0P000goo1@001goo10001Goo1P001Woo1@0027oo00=:LP00IcT02Goo0004OonaON0;0007O00;002O
0n0;0ol7O00;0onO0O`;01l7O00;01naON0DOol00`00Oomoo`03Ool00`00Oomoo`05Ool00dYb001[
FP04Ool01@00Oomoogoo00000goo00D007ooOomoo`0000Eoo`03001oogoo00=oo`03001oogoo009o
o`8000Aoo`03001oogoo00Moo`03001oogoo00=oo`03001oogoo00Eoo`03IcT004Yb00Uoo`0017oo
/GgP2`001g`03000WP?P2`?o1g`030?oWP7l2`0O1g`0300O/7gP5Goo00D007ooOomoo`0000Qoo`03
<H`007Nm00Aoo`05001oogooOol00003Ool01000Oomoo`0037oo00<007ooOol027oo00<007ooOol0
1Woo00<007ooOol037oo00=g_@00<H`02Goo0004OonaON0;0007O00<002N0n0;0ol7O00<0onN0O`;
01l7O00<01n`ON0FOol00`00Ool00009Ool20005Ool01@00Oomoogoo00000goo00@007ooOol000ao
o`03001oogoo00Qoo`03001oogoo00Ioo`03001oogoo00eoo`8000Uoo`0017oo/GgP2`001g`03000
WP?P2`?o1g`030?oWP7l2`0O1g`0300O/7gP5goo00<007ooOol027oo0P001Goo00D007ooOomoo`00
00=oo`03001oogoo00P000Eoo`03001oogoo00Qoo`03001oogoo00Ioo`P000Qoo`8000Uoo`0017oo
/GgP2`001g`03000WP?P2`?o1g`030?oWP7l2`0O1g`0300O/7gP5Woo00<007oo00002Goo00<aS000
Li`017oo00D007ooOomoo`0000=oo`04001oogoo0006Ool00`00Oomoo`03Ool00`00Oomoo`08Ool0
0`00Oomoo`06Ool00`00Oomoo`04Ool00`00Oomoo`05Ool00gNm000U:@09Ool000Aook5mh0/000Ml
00/009l3h0/3o`Ml00/3oil1o0/07`Ml00/07k5mh1Eoo`05001oogooOol00008Ool00dYb001S6004
Ool01@00Oomoogoo00000goo00@007ooOol000Ioo`03001oogoo00=oo`03001oogoo00Qoo`8000Mo
o`03001oogoo00Aoo`03001oogoo00Eoo`03IcT004Yb00Uoo`0017oo/WgP2P001g`02`00X0?P2P?o
1g`02`?oX07l2P0O1g`02`0O/GgP57oo00<007ooOol00goo00<007ooOol01Goo00=W>@00?Nl017oo
0P000Woo0P000Woo00D007ooOomoo`0000Aoo`03001oogoo00Aoo`03001oogoo00Qoo`03001oo`00
00=oo`05001oogooOol00004Ool00`00Oomoo`06Ool00dYb001W>@09Ool000Moo`03001oogoo05]o
o`03001oogoo04ioo`X000Ml00/00:03h0X3o`Ml00/3oj01o0X07`Ml00/07dmoo`03001oogoo05]o
o`03001oogoo01Aoo`<000=oo`<000Moo`039BT006mk009oo`800003Ool00000009oo`8000Eoo`@0
00Eoo`P000Aoo`80009oo`<000Eoo`@000Qoo`03Mkd002DY00Yoo`00]Woo2P001g`02`00X0?P2P?o
1g`02`?oX07l2P0O1g`02`0Oe7oo00=Nm`00BW806goo00<007ooOol07goo00=BU000G_L02Woo002g
Ool90007O00:002BOol@On090ol7O00:0ol@On2BOol901l7O00:01oFOol00c6<001cW00JOol00`00
Oomoo`0NOol00gNm000aS00;Ool00;Moo`T000Ml00X0099ooa1oh0T3o`Ml00X3oa1oh99oo`T07`Ml
00X07mIoo`03JeX004Ha01Yoo`03001oogoo01ioo`03AS4006]J00]oo`00^7oo20001g`02@00Tgoo
4GoP20?o1g`02@?o4GoPTgoo200O1g`02@0Oogoo97oo002iOol70007O008002EOolAOn070ol7O008
0olAOn2EOol701l7O00801ooOolUOol00;Yoo`H000Ml00L009Iooa9oh0H3o`Ml00L3oa9oh9Ioo`H0
7`Ml00L07omoobIoo`00^goo1@001g`01P00Ugoo4goP1@?o1g`01P?o4goPUgoo1@0O1g`01P0Oogoo
9goo002lOol40007O005002IOolCOn040ol7O0050olCOn2IOol401l7O00501ooOolXOol00;ioo`80
00Ml00<009]ooaEoh083o`Ml00<3oaEoh9]oo`807`Ml00<07omoobYoo`00`7oo1g`0Wgoo5WoP1g`0
5goPWWoo1g`0ogoo;Goo0030Ool7O02POolEOn07O00FOn2OOol7O03oOol]Ool00<1oo`Ml0:1ooaEo
h0Ml01Ioh9moo`Ml0?moobeoo`00`7oo1g`0XGoo57oP1g`05GoPX7oo1g`0ogoo;Goo0030Ool7O02R
OolCOn07O00DOn2QOol7O03oOol]Ool00<1oo`Ml0:=ooa9oh0Ml01=oh:9oo`Ml0?moobeoo`00`7oo
1g`0Xgoo4WoP1g`04goPXWoo1g`0ogoo;Goo0030Ool7O02TOolAOn07O00BOn2SOol7O03oOol]Ool0
0<1oo`Ml0:Ioo`moh0Ml011oh:Eoo`Ml0?moobeoo`00`7oo1g`0Ygoo3WoP1g`03goPYWoo1g`0ogoo
;Goo0030Ool7O02XOol=On07O00>On2WOol7O03oOol]Ool00<1oo`Ml0:Yoo`]oh0Ml00aoh:Uoo`Ml
0?moobeoo`00`7oo1g`0Zgoo2WoP1g`02goPZWoo1g`0ogoo;Goo0030Ool7O02]Ool8On07O009On2/
Ool7O03oOol]Ool00<1oo`Ml0;1oo`Eoh0Ml00Ioh:moo`Ml0?moobeoo`00`7oo1g`0]7oo00=oh7`0
O0001G`00WoP/goo1g`0ogoo;Goo0030Ool7O02eOol7O02eOol7O03oOol]Ool00<1oo`Ml0;Eoo`Ml
0;Eoo`Ml0?moobeoo`00`7oo1g`0]Goo1g`0]Goo1g`0ogoo;Goo0030Ool7O02eOol7O02eOol7O03o
Ool]Ool00<1oo`Ml0;Eoo`Ml0;Eoo`Ml0?moobeoo`00`7oo1g`0]Goo1g`0]Goo1g`0ogoo;Goo0030
Ool7O02eOol7O02eOol7O03oOol]Ool00<1oo`Ml0;Eoo`Ml0;Eoo`Ml0?moobeoo`00`7oo1g`0]Goo
1g`0]Goo1g`0ogoo;Goo0030Ool7O02eOol7O02eOol7O03oOol]Ool00<1oo`Ml0;Eoo`Ml0;Eoo`Ml
0?moobeoo`00`7oo1g`0]Goo1g`0]Goo1g`0ogoo;Goo0030Ool7O02eOol7O02eOol7O03oOol]Ool0
0<1oo`Ml0;Eoo`Ml0;Eoo`Ml0?moobeoo`00`7oo1g`0]Goo1g`0]Goo1g`0ogoo;Goo003oOoooOooo
Oom^Ool00?mooomooomoofioo`00ogooogooogooKWoo003oOoooOoooOom^Ool00?mooomooomoofio
o`00ogooogooogooKWoo003oOoooOoooOom^Ool00?mooomooomoofioo`00ogooogooogooKWoo003o
OoooOoooOom^Ool00?mooomooomoofioo`00ogooogooogooKWoo003oOoooOoooOom^Ool00?mooomo
oomoofioo`00ogooogooogooKWoo003oOoooOoooOom^Ool00?mooomooomoofioo`00\
\>"],
  ImageRangeCache->{{{0, 874}, {124, 0}} -> {-4.0798, -7.41737, 0.0132927, \
0.149543}}],

Cell[BoxData[
    TagBox[\(\[SkeletonIndicator]  Graphics  \[SkeletonIndicator]\),
      False,
      Editable->False]], "Output"]
}, Open  ]],

Cell[BoxData[
    \(Table[
      Show[pa[n, fa, fd], g1[n, fa, fd], ga1[n, fa, fd], p[n, fa, fd], 
        pb[n, fa, fd], pc], {n, 0, 
        1/\((\((fd - fa)\)/2)\), \(1/\((\((fd - fa)\)/2)\)\)/
          1600}]\)], "Input"]
},
FrontEndVersion->"4.2 for Microsoft Windows",
ScreenRectangle->{{0, 1024}, {0, 690}},
WindowSize->{1006, 659},
WindowMargins->{{3, Automatic}, {Automatic, 0}},
PrintingCopies->1,
PrintingPageRange->{Automatic, Automatic},
CellLabelAutoDelete->True,
Magnification->1.25,
StyleDefinitions -> "Classroom.nb"
]

(*******************************************************************
Cached data follows.  If you edit this Notebook file directly, not
using Mathematica, you must remove the line containing CacheID at
the top of  the file.  The cache data will then be recreated when
you save this file from within Mathematica.
*******************************************************************)

(*CellTagsOutline
CellTagsIndex->{}
*)

(*CellTagsIndex
CellTagsIndex->{}
*)

(*NotebookFileOutline
Notebook[{
Cell[1754, 51, 810, 14, 249, "Input"],
Cell[2567, 67, 300, 6, 81, "Input"],
Cell[2870, 75, 983, 17, 270, "Input"],
Cell[3856, 94, 628, 11, 186, "Input"],
Cell[4487, 107, 1096, 20, 312, "Input"],
Cell[5586, 129, 1113, 20, 312, "Input"],
Cell[6702, 151, 62, 1, 60, "Input"],

Cell[CellGroupData[{
Cell[6789, 156, 143, 2, 60, "Input"],
Cell[6935, 160, 12315, 402, 135, 4793, 306, "GraphicsData", "PostScript", \
"Graphics"],
Cell[19253, 564, 130, 3, 60, "Output"]
}, Open  ]],
Cell[19398, 570, 226, 5, 81, "Input"]
}
]
*)



(*******************************************************************
End of Mathematica Notebook file.
*******************************************************************)

