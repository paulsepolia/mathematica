(************** Content-type: application/mathematica **************
                     CreatedBy='Mathematica 4.2'

                    Mathematica-Compatible Notebook

This notebook can be used with any Mathematica-compatible
application, such as Mathematica, MathReader or Publicon. The data
for the notebook starts with the line containing stars above.

To get the notebook into a Mathematica-compatible application, do
one of the following:

* Save the data starting with the line of stars above into a file
  with a name ending in .nb, then open the file inside the
  application;

* Copy the data starting with the line of stars above to the
  clipboard, then use the Paste menu command inside the application.

Data for notebooks contains only printable 7-bit ASCII and can be
sent directly in email or through ftp in text mode.  Newlines can be
CR, LF or CRLF (Unix, Macintosh or MS-DOS style).

NOTE: If you modify the data for this notebook not in a Mathematica-
compatible application, you must delete the line below containing
the word CacheID, otherwise Mathematica-compatible applications may
try to use invalid cache data.

For more information on notebooks and Mathematica-compatible 
applications, contact Wolfram Research:
  web: http://www.wolfram.com
  email: info@wolfram.com
  phone: +1-217-398-0700 (U.S.)

Notebook reader applications are available free of charge from 
Wolfram Research.
*******************************************************************)

(*CacheID: 232*)


(*NotebookFileLineBreakTest
NotebookFileLineBreakTest*)
(*NotebookOptionsPosition[      8864,        327]*)
(*NotebookOutlinePosition[      9544,        350]*)
(*  CellTagsIndexPosition[      9500,        346]*)
(*WindowFrame->Normal*)



Notebook[{
Cell[BoxData[
    \(p1[n_, m_] := 
      Show[Graphics[{{PointSize[ .05], RGBColor[0, 0, 1], 
              Point[{Sin[\((2*Pi*800)\)*n], 0}]}}, AspectRatio \[Rule] 1/50, 
          Axes \[Rule] True, 
          PlotRange \[Rule] {{\(-1.08\), 1.08}, {\(-0.20\), 0.20}}, 
          ImageSize \[Rule] {900, 70}]]\)], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
    \(p1[0, 0]\)], "Input"],

Cell[GraphicsData["PostScript", "\<\
%!
%%Creator: Mathematica
%%AspectRatio: .02 
%%ImageSize: 900 70 
MathPictureStart
/Mabs {
Mgmatrix idtransform
Mtmatrix dtransform
} bind def
/Mabsadd { Mabs
3 -1 roll add
3 1 roll add
exch } bind def
%% Graphics
%%IncludeResource: font Courier
%%IncludeFont: Courier
/Courier findfont 10  scalefont  setfont
% Scaling calculations
0.5 0.462963 0.01 0.05 [
[.03704 -0.0025 -6 -9 ]
[.03704 -0.0025 6 0 ]
[.26852 -0.0025 -12 -9 ]
[.26852 -0.0025 12 0 ]
[.73148 -0.0025 -9 -9 ]
[.73148 -0.0025 9 0 ]
[.96296 -0.0025 -3 -9 ]
[.96296 -0.0025 3 0 ]
[.4875 0 -24 -4.5 ]
[.4875 0 0 4.5 ]
[.4875 .005 -24 -4.5 ]
[.4875 .005 0 4.5 ]
[.4875 .015 -18 -4.5 ]
[.4875 .015 0 4.5 ]
[.4875 .02 -18 -4.5 ]
[.4875 .02 0 4.5 ]
[ 0 0 0 0 ]
[ 1 .02 0 0 ]
] MathScale
% Start of Graphics
1 setlinecap
1 setlinejoin
newpath
0 g
.25 Mabswid
[ ] 0 setdash
.03704 .01 m
.03704 .01625 L
s
[(-1)] .03704 -0.0025 0 1 Mshowa
.26852 .01 m
.26852 .01625 L
s
[(-0.5)] .26852 -0.0025 0 1 Mshowa
.73148 .01 m
.73148 .01625 L
s
[(0.5)] .73148 -0.0025 0 1 Mshowa
.96296 .01 m
.96296 .01625 L
s
[(1)] .96296 -0.0025 0 1 Mshowa
.125 Mabswid
.08333 .01 m
.08333 .01375 L
s
.12963 .01 m
.12963 .01375 L
s
.17593 .01 m
.17593 .01375 L
s
.22222 .01 m
.22222 .01375 L
s
.31481 .01 m
.31481 .01375 L
s
.36111 .01 m
.36111 .01375 L
s
.40741 .01 m
.40741 .01375 L
s
.4537 .01 m
.4537 .01375 L
s
.5463 .01 m
.5463 .01375 L
s
.59259 .01 m
.59259 .01375 L
s
.63889 .01 m
.63889 .01375 L
s
.68519 .01 m
.68519 .01375 L
s
.77778 .01 m
.77778 .01375 L
s
.82407 .01 m
.82407 .01375 L
s
.87037 .01 m
.87037 .01375 L
s
.91667 .01 m
.91667 .01375 L
s
.25 Mabswid
0 .01 m
1 .01 L
s
.5 0 m
.50625 0 L
s
[(-0.2)] .4875 0 1 0 Mshowa
.5 .005 m
.50625 .005 L
s
[(-0.1)] .4875 .005 1 0 Mshowa
.5 .015 m
.50625 .015 L
s
[(0.1)] .4875 .015 1 0 Mshowa
.5 .02 m
.50625 .02 L
s
[(0.2)] .4875 .02 1 0 Mshowa
.125 Mabswid
.5 .001 m
.50375 .001 L
s
.5 .002 m
.50375 .002 L
s
.5 .003 m
.50375 .003 L
s
.5 .004 m
.50375 .004 L
s
.5 .006 m
.50375 .006 L
s
.5 .007 m
.50375 .007 L
s
.5 .008 m
.50375 .008 L
s
.5 .009 m
.50375 .009 L
s
.5 .011 m
.50375 .011 L
s
.5 .012 m
.50375 .012 L
s
.5 .013 m
.50375 .013 L
s
.5 .014 m
.50375 .014 L
s
.5 .016 m
.50375 .016 L
s
.5 .017 m
.50375 .017 L
s
.5 .018 m
.50375 .018 L
s
.5 .019 m
.50375 .019 L
s
.25 Mabswid
.5 0 m
.5 .02 L
s
0 0 m
1 0 L
1 .02 L
0 .02 L
closepath
clip
newpath
0 0 1 r
.05 w
.5 .01 Mdot
% End of Graphics
MathPictureEnd
\
\>"], "Graphics",
  ImageSize->{900, 70},
  ImageCache->GraphicsData["Bitmap", "\<\
CF5dJ6E]HGAYHf4PAg9QL6QYHg<PAVmbKF5d0`4000>40000AQ000`40O003h00OogooogooogooQgoo
003oOoooOoooOon7Ool00?mooomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7Ool00?mo
oomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7Ool00?mooomooomoohMoo`00ogooogoo
ogooQgoo003oOoooOoooOon7Ool00?mooomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7
Ool00?mooomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7Ool00?mool1oo`@07omool9o
o`00ogoo^goo3P0Oogoo_Goo000ROol50036Ool20004Ool20004Ool2002lOolD01noOol20004Ool2
0004Ool20037Ool5000POol002Aoo`03001oogoo0<Eoo`04001oogoo0008Ool01000Oomoo`00^Woo
5P0O_Goo00@007ooOol000Qoo`04001oogoo0038Ool00`00Oomoo`0POol002Aoo`03001oogoo0<Eo
o`04001oogoo000;Ool00`00Oomoo`2fOolJ01nkOol01000Oomoo`002goo00<007ooOol0aWoo00<0
07ooOol087oo000LOol40004Ool00`00Oomoo`2oOol40002Ool01000Oomoo`002Goo0P00^7oo700O
^Woo00@007ooOol000Uoo`800<Uoo`03001oogoo021oo`0097oo00<007ooOol0aGoo00@007ooOol0
00Uoo`03001oogoo0;Iooah07kUoo`04001oogoo0009Ool00`00Oomoo`38Ool00`00Oomoo`0POol0
02=oo`800<Qoo`8000Yoo`<00;Eoob007kUoo`8000Yoo`<00<Moo`80029oo`00ogoo/Goo8P0Oogoo
/goo003oOonaOolR01ooOoncOol00?moojMoo`8000Aoo`800003Ool07`0O02807omook9oo`00ogoo
YWoo00@007ooOol000EoobH07omook5oo`00ogooYWoo00@007ooOol000AoobP07omook1oo`00ogoo
X7oo10000Woo00@007ooOol000AoobP07omook1oo`00ogooYWoo00@007ooOol000AoobP07omook1o
o`00ogooYgoo0P0017oo:P0Oogoo[goo003oOonVOol01000Oomoo`000goo:P0Oogoo[goo003oOonV
Ool01000Oomoo`000goo:P0Oogoo[goo003oOonPOol40002Ool01000Oomoo`000goo:P0Oogoo[goo
003oOonVOol01000Oomoo`000goo:P0Oogoo[goo003oOonWOol20003Ool/01ooOon^Ool00?l00:`0
02`07ol00:d0005oo`008Goo00<007ooOol09Woo00<007ooOol09goo00<007ooOol09goo00<007oo
Ool09Woo00<007ooOol09goo00<007ooOol09goo00<007ooOol09Woo00<007ooOol09goo00<007oo
Ool09Woo00<007ooOol04Goo;00O57oo00<007ooOol09Woo00<007ooOol09goo00<007ooOol09Woo
00<007ooOol09goo00<007ooOol09goo00<007ooOol09Woo00<007ooOol09goo00<007ooOol09goo
00<007ooOol09Woo00<007ooOol087oo000QOol00`00Oomoo`0VOol00`00Oomoo`0WOol00`00Oomo
o`0WOol00`00Oomoo`0VOol00`00Oomoo`0WOol00`00Oomoo`0WOol00`00Oomoo`0VOol00`00Oomo
o`0WOol00`00Oomoo`0VOol00`00Oomoo`0AOol/01lDOol00`00Oomoo`0VOol00`00Oomoo`0WOol0
0`00Oomoo`0VOol00`00Oomoo`0WOol00`00Oomoo`0WOol00`00Oomoo`0VOol00`00Oomoo`0WOol0
0`00Oomoo`0WOol00`00Oomoo`0VOol00`00Oomoo`0POol0025oo`03001oogoo02Ioo`03001oogoo
02Moo`03001oogoo02Moo`03001oogoo02Ioo`03001oogoo02Moo`03001oogoo02Moo`03001oogoo
02Ioo`03001oogoo02Moo`03001oogoo02Ioo`03001oogoo00aoo`8000AoobX07aEoo`03001oogoo
02Ioo`03001oogoo02Moo`03001oogoo02Ioo`03001oogoo02Moo`03001oogoo02Moo`03001oogoo
02Ioo`03001oogoo02Moo`03001oogoo02Moo`03001oogoo02Ioo`03001oogoo021oo`008Goo00<0
07ooOol09Woo00<007ooOol09goo00<007ooOol09goo00<007ooOol09Woo00<007ooOol09goo00<0
07ooOol09goo00<007ooOol09Woo00<007ooOol09goo00<007ooOol09Woo00<007ooOol02goo00@0
07ooOol000=oobX07aEoo`03001oogoo02Ioo`03001oogoo02Moo`03001oogoo02Ioo`03001oogoo
02Moo`03001oogoo02Moo`03001oogoo02Ioo`03001oogoo02Moo`03001oogoo02Moo`03001oogoo
02Ioo`03001oogoo021oo`008Goo00<007ooOol0cGoo00<007ooOol0/Goo00@007ooOol000=oobX0
7k]oo`03001oogoo0<eoo`03001oogoo021oo`008Goo00<007ooOol0cGoo00<007ooOol0/Goo00@0
07ooOol000=oobX07k]oo`03001oogoo0<eoo`03001oogoo021oo`00ogooYWoo10000goo:P0Oogoo
[goo003oOonVOol40004OolX01ooOon`Ool00?moojIoo`04001oogoo0004OolX01ooOon`Ool00?mo
ojIoo`04001oogoo0004OolX01ooOon`Ool00?moojIoo`04001oogoo0005OolV01ooOonaOol00?mo
ojMoo`8000Moob@07omook9oo`00ogoo/Goo8P0Oogoo/goo003oOonaOolR01ooOoncOol00?mook9o
ob007omookAoo`00ogoo/goo7P0Oogoo]Goo003oOondOolL01ooOonfOol00?mookEooaX07omookMo
o`00ogoo]goo5P0Oogoo^Goo003oOonhOolD01ooOonjOol00?mook]oo`h07omookeoo`00ogoo`7oo
100Oogoo`Woo003oOoooOoooOon7Ool00?mooomooomoohMoo`00ogooogooogooQgoo003oOoooOooo
Oon7Ool00?mooomooomoohMoo`00ogooogooogooQgoo003oOoooOoooOon7Ool00?mooomooomoohMo
o`00ogooogooogooQgoo0000\
\>"],
  ImageRangeCache->{{{0, 899}, {69, 0}} -> {-1.08001, -0.842583, 0.00240269, \
0.0222472}}],

Cell[OutputFormData["\<\
Graphics[{{PointSize[0.05], RGBColor[0, 0, 1], Point[{0, 0}]}}, 
 {AspectRatio -> 1/50, Axes -> True, PlotRange -> {{-1.08, 1.08}, {-0.2, \
0.2}}, ImageSize -> {900, 70}}]\
\>", "\<\
-Graphics-\
\>"], "Output"]
}, Open  ]],

Cell[BoxData[
    \(Table[p1[n, 800], {n, 0, 1/800, \(1/800\)/3200}]\)], "Input"]
},
FrontEndVersion->"4.2 for Microsoft Windows",
ScreenRectangle->{{0, 1024}, {0, 690}},
WindowSize->{1016, 658},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
StyleDefinitions -> "Classroom.nb"
]

(*******************************************************************
Cached data follows.  If you edit this Notebook file directly, not
using Mathematica, you must remove the line containing CacheID at
the top of  the file.  The cache data will then be recreated when
you save this file from within Mathematica.
*******************************************************************)

(*CellTagsOutline
CellTagsIndex->{}
*)

(*CellTagsIndex
CellTagsIndex->{}
*)

(*NotebookFileOutline
Notebook[{
Cell[1754, 51, 323, 6, 90, "Input"],

Cell[CellGroupData[{
Cell[2102, 61, 41, 1, 50, "Input"],
Cell[2146, 64, 6380, 249, 78, 2464, 197, "GraphicsData", "PostScript", \
"Graphics"],
Cell[8529, 315, 235, 6, 47, "Output"]
}, Open  ]],
Cell[8779, 324, 81, 1, 50, "Input"]
}
]
*)



(*******************************************************************
End of Mathematica Notebook file.
*******************************************************************)

