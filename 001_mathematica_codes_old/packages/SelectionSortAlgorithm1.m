
(*
(* : Title : SelectionSortAlgorithm.*)

(* : Authors : Paul Galiatsatos.*)

(* : Context : PaulsTools`SelectionSortAlgorithm`*)

(* : Package Version : 1.0*)

(* : Copyright : No Copyright*)

(* : History : Version 1.0 by Paul Galiatsatos, December 2004.*)

(* : Keywords :*)

(* : Source : None.*)

(* : Warning :*)

(* : Mathematica Version : 5.1*)

(* : Limitation :*)
*)
BeginPackage["PaulsTools`SelectionSortAlgorithm`"]

SelectionSortAlgorithm::usage=
"SelectionSortAlgorithm[{n1,n2,...nN},opts]
\nsorts the list {n1,2n,...nN} which is consisted of Numerics only.
\nYou can give the option 
\nToPrint->Yes or No.
\nThe Default value is Yes."


ToPrint::usage =
"ToPrint is an option which can take values Yes Or No.
\nThe Default value is Yes.
\nIf you choose Yes then all intermediates 
results are printed."


SelectionSortAlgorithm::"BadOption" =
"You gave bad options.
\nTry only rules of the form ToPrint->Yes or ToPrint->No
\nor Sequence[]... nothing to say."


SelectionSortAlgorithm::"BadElements" =
"The list's elements MUST BE ONLY NUMERICS!Try Again!"


Yes::usage = 
"Yes is one of the TWO possibles assignments for option ToPrint."


No::usage = "No is one of the TWO possibles assignments for option ToPrint."


Begin["`Private`"]

Unprotect["PaulsTools`SelectionSortAlgorithm`*"]

Needs["PaulsTools`SwapFunction`"]

Options[SelectionSortAlgorithm] = {ToPrint -> "Yes"}

SelectionSortAlgorithm[MyList_List, opts___?OptionQ] := (
    Module[
      {
        CurrentList = MyList,
        n = Length[MyList],
        TheOldPossibleMin,
        TheNewPossibleMin,
        IndexOldPossibleMin,
        IndexNewPossibleMin,
        i,
        j,
        aa,
        s,
        testOption,
        testPrint},
      
      s = 0;
      
      If[Cases[CurrentList, _Symbol] =!= {}, 
        Message[SelectionSortAlgorithm::"BadElements"],
        aa = CurrentList;
        Do[s = s + 1;
          TheOldPossibleMin = Part[CurrentList, i];
          
          IndexOldPossibleMin = IndexNewPossibleMin = i;
          
          Do[
            If[
              Part[CurrentList, j] < TheOldPossibleMin,
              TheNewPossibleMin = Part[CurrentList, j];
              TheOldPossibleMin = TheNewPossibleMin;
              IndexNewPossibleMin = j;
              ],
            {j, i + 1, n}
            ];
          
          SwapFunction[
            CurrentList,
            IndexOldPossibleMin,
            IndexNewPossibleMin
            ];
          
          testOption = ToString[Part[opts, 1]];
          
          
          testPrint = 
            ToString[(ToPrint /. {opts} /. Options[SelectionSortAlgorithm])];
          
          Which[
            (testPrint === "Yes" && testOption === "ToPrint") || (testOption \
\
\
\
==
                   "1"), If[i == 1, Print[{aa, {s}}]; 
              Print[{CurrentList, {s = s + 1}}], Print[{CurrentList, {s}}]],
            testPrint == "No" && testOption == "ToPrint", 2;,
            (testPrint =!= "Yes") || (testPrint =!= "No") || (testOption =!= 
                  "ToPrint"),
            Message[
              SelectionSortAlgorithm::"BadOption"];
            Break[]
            ],
          {i, 1, n - 1}
          ];
        ];
      {CurrentList,{If[testPrint == "No" && testOption == "ToPrint",s+1,s]}}]
    )

End[]

Protect["PaulsTools`SelectionSortAlgorithm`*"]

EndPackage[]


