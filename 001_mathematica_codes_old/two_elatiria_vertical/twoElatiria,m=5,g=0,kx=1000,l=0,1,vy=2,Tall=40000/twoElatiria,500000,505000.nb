(************** Content-type: application/mathematica **************
                     CreatedBy='Mathematica 4.2'

                    Mathematica-Compatible Notebook

This notebook can be used with any Mathematica-compatible
application, such as Mathematica, MathReader or Publicon. The data
for the notebook starts with the line containing stars above.

To get the notebook into a Mathematica-compatible application, do
one of the following:

* Save the data starting with the line of stars above into a file
  with a name ending in .nb, then open the file inside the
  application;

* Copy the data starting with the line of stars above to the
  clipboard, then use the Paste menu command inside the application.

Data for notebooks contains only printable 7-bit ASCII and can be
sent directly in email or through ftp in text mode.  Newlines can be
CR, LF or CRLF (Unix, Macintosh or MS-DOS style).

NOTE: If you modify the data for this notebook not in a Mathematica-
compatible application, you must delete the line below containing
the word CacheID, otherwise Mathematica-compatible applications may
try to use invalid cache data.

For more information on notebooks and Mathematica-compatible 
applications, contact Wolfram Research:
  web: http://www.wolfram.com
  email: info@wolfram.com
  phone: +1-217-398-0700 (U.S.)

Notebook reader applications are available free of charge from 
Wolfram Research.
*******************************************************************)

(*CacheID: 232*)


(*NotebookFileLineBreakTest
NotebookFileLineBreakTest*)
(*NotebookOptionsPosition[     13859,        340]*)
(*NotebookOutlinePosition[     14539,        363]*)
(*  CellTagsIndexPosition[     14495,        359]*)
(*WindowFrame->Normal*)



Notebook[{
Cell[BoxData[
    \(\(Clear[m, g, kx, ky, l, vx, vy, f, Tinitial, Tfinal];\)\)], "Input"],

Cell[BoxData[
    \(f1[m_, g_, kx_, ky_, l_, vx_, vy_, Tinitial_, Tfinal_] := 
      NDSolve[{m*\(x''\)[t] \[Equal] 
            ky*Abs[\((\((l - x[t])\)^2 + \((l + y[t])\)^2)\)^0.5 - 
                    l]*\((\(-1\))\)*
                Sign[\((\((l - x[t])\)^2 + \((l + y[t])\)^2)\)^0.5 - 
                    l]*\((x[t] - 
                      l)\)/\((\((l - x[t])\)^2 + \((l + y[t])\)^2)\)^0.5 + 
              kx*Abs[\((\((x[t])\)^2 + \((y[t])\)^2)\)^0.5 - l]*
                Sign[\((\((x[t])\)^2 + \((y[t])\)^2)\)^0.5 - l]*\((\(-1\))\)*
                x[t]/\((\((x[t])\)^2 + \((y[t])\)^2)\)^0.5 + m*g, 
          m*\(y''\)[t] \[Equal] 
            ky*Abs[\((\((l - x[t])\)^2 + \((l + y[t])\)^2)\)^0.5 - 
                    l]*\((\(-1\))\)*
                Sign[\((\((l - x[t])\)^2 + \((l + y[t])\)^2)\)^0.5 - 
                    l]*\((y[t] + 
                      l)\)/\((\((l - x[t])\)^2 + \((l + y[t])\)^2)\)^0.5 + 
              kx*Abs[\((\((x[t])\)^2 + \((y[t])\)^2)\)^0.5 - l]*\((\(-1\))\)*
                Sign[\((\((x[t])\)^2 + \((y[t])\)^2)\)^0.5 - l]*
                y[t]/\((\((x[t])\)^2 + \((y[t])\)^2)\)^0.5, x[0] \[Equal] l, 
          y[0] \[Equal] 0, \(x'\)[0] \[Equal] vx, \(y'\)[0] \[Equal] vy}, {x, 
          y}, {t, Tinitial, Tfinal}, 
        MaxSteps \[Rule] 50000000000000]\)], "Input"],

Cell[BoxData[
    \(m = 5; g = 0; kx = 1000; ky = 0; l = 0.1; vx = 0; vy = 2; f = 2; 
    Tinitial = 500000; Tfinal = 505000;\)], "Input"],

Cell[BoxData[
    \(\(s1 = f1[m, g, kx, ky, l, vx, vy, Tinitial, Tfinal];\)\)], "Input"],

Cell[BoxData[
    \(Clear[ti, dt]\)], "Input"],

Cell[BoxData[
    \(f2[ti_, dt_] := 
      ParametricPlot[{{x[t], y[t]} /. s1, 
          l*{Cos[2*Pi*f*t], Sin[2*Pi*f*t]}}, {t, ti, ti + dt}, 
        Compiled \[Rule] False, PlotPoints \[Rule] 500, MaxBend \[Rule] 0, 
        PlotStyle \[Rule] {{RGBColor[1, 0, 0], 
              Thickness[0.005]}, {RGBColor[0, 1, 0], Thickness[0.01]}}, 
        PlotRange \[Rule] All, Axes \[Rule] True, 
        AxesLabel \[Rule] {StyleForm["\<x(metre)\>", FontSize \[Rule] 15, 
              FontWeight \[Rule] "\<Bold\>"], 
            StyleForm["\<y(metre)\>", FontSize \[Rule] 15, 
              FontWeight -> "\<Bold\>"]}, AspectRatio \[Rule] Automatic, 
        AxesStyle \[Rule] {{RGBColor[0, 0, 0], 
              Thickness[0.004]}, {RGBColor[0, 0, 0], Thickness[0.004]}}, 
        ImageSize \[Rule] {500, 500}]\)], "Input"],

Cell[BoxData[
    \(Clear[sz, ti, tf, dt]\)], "Input"],

Cell[BoxData[
    \(f3[sz_, ti_, tf_, dt_] := 
      Table[ParametricPlot[{{x[t], y[t]} /. s1, 
            l*{Cos[2*Pi*f*t], Sin[2*Pi*f*t]}}, {t, ti, ti + dt}, 
          Compiled \[Rule] False, PlotPoints \[Rule] 50, MaxBend \[Rule] 0, 
          PlotStyle \[Rule] {{RGBColor[1, 0, 0], 
                Thickness[0.005]}, {RGBColor[0, 1, 0], Thickness[0.01]}}, 
          PlotRange \[Rule] {{\(-sz\), sz}, {\(-sz\), sz}}, 
          Axes \[Rule] False, 
          AxesLabel \[Rule] {StyleForm["\<x(metre)\>", FontSize \[Rule] 15, 
                FontWeight \[Rule] "\<Bold\>"], 
              StyleForm["\<y(metre)\>", FontSize \[Rule] 15, 
                FontWeight -> "\<Bold\>"]}, AspectRatio \[Rule] Automatic, 
          AxesStyle \[Rule] {{RGBColor[0, 0, 0], 
                Thickness[0.004]}, {RGBColor[0, 0, 0], Thickness[0.004]}}, 
          ImageSize \[Rule] {500, 500}], {n, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(\(Clear[X, Y, vX, vY, t];\)\)], "Input"],

Cell[BoxData[
    \(\(X[t_] = x[t] /. First[s1];\)\)], "Input"],

Cell[BoxData[
    \(\(vX[t_] = \(x'\)[t] /. First[s1];\)\)], "Input"],

Cell[BoxData[
    \(\(Y[t_] = y[t] /. First[s1];\)\)], "Input"],

Cell[BoxData[
    \(\(vY[t_] = \(y'\)[t] /. First[s1];\)\)], "Input"],

Cell[BoxData[
    \(\(Clear[s, dt, dS, En, meanEn, ti, dt, V, L, tf];\)\)], "Input"],

Cell[BoxData[
    \(dS[ti_, dt_] := 
      NIntegrate[\((\((vX[t])\)^2 + \((vY[t])\)^2)\)^0.5, {t, ti, ti + dt}, 
        MaxRecursion \[Rule] 50, WorkingPrecision \[Rule] 30, 
        Method \[Rule] QuasiMonteCarlo]\)], "Input"],

Cell[BoxData[
    \(dS1[ti_, dt_] := 
      NIntegrate[\((\((vX[t])\)^2 + \((vY[t])\)^2)\)^0.5, {t, ti, ti + dt}, 
        MinRecursion \[Rule] 5, MaxRecursion \[Rule] 50]\)], "Input"],

Cell[BoxData[
    \(dS2[ti_, dt_] := 
      NIntegrate[\((\((vX[t])\)^2 + \((vY[t])\)^2)\)^0.5, {t, ti, ti + dt}, 
        MaxRecursion \[Rule] 50]\)], "Input"],

Cell[BoxData[
    \(En[t_] := 
      1/2*kx*\((Abs[\((\((X[t])\)^2 + \((Y[t])\)^2)\)^0.5 - l])\)^2*
          Sign[\((\((\((X[t])\)^2 + \((Y[t])\)^2)\)^0.5 - 
                l)\)]*\((\(-1\))\) + 
        1/2*m*\((\((vX[t])\)^2 + \((vY[t])\)^2)\)\)], "Input"],

Cell[BoxData[
    \(V[t_] := \((\((vX[t])\)^2 + \((vY[t])\)^2)\)^0.5\)], "Input"],

Cell[BoxData[
    \(Ekin[t_] := 1/2*m*\((\((vX[t])\)^2 + \((vY[t])\)^2)\)\)], "Input"],

Cell[BoxData[
    \(Edyn[t_, kx_, ky_] := 
      1/2*kx*\((Abs[\((\((X[t])\)^2 + \((Y[t])\)^2)\)^0.5 - l])\)^2 + 
        1/2*ky*\((Abs[\((\((l - X[t])\)^2 + \((l + Y[t])\)^2)\)^0.5 - 
                  l])\)^2\)], "Input"],

Cell[BoxData[
    \(meanEn[ti_, dt_] := 
      NIntegrate[En[t], {t, ti, ti + dt}, MinRecursion \[Rule] 5, 
          MaxRecursion \[Rule] 50, WorkingPrecision \[Rule] 30, 
          Method \[Rule] QuasiMonteCarlo]/\((dt)\)\)], "Input"],

Cell[BoxData[
    \(meanEn1[ti_, dt_] := 
      NIntegrate[En[t], {t, ti, ti + dt}, MinRecursion \[Rule] 5, 
          MaxRecursion \[Rule] 20]/\((dt)\)\)], "Input"],

Cell[BoxData[
    \(meanEn2[ti_, dt_] := 
      NIntegrate[En[t], {t, ti, ti + dt}, 
          MaxRecursion -> 50]/\((dt)\)\)], "Input"],

Cell[BoxData[
    \(L[t_] := X[t]*m*vY[t] - Y[t]*m*vX[t]\)], "Input"],

Cell[BoxData[
    \(meanL[t_, dt_] := 
      NIntegrate[L[n]/dt, {n, t, t + dt}, MinRecursion \[Rule] 5, 
        MaxRecursion \[Rule] 50, WorkingPrecision \[Rule] 30, 
        Method \[Rule] QuasiMonteCarlo]\)], "Input"],

Cell[BoxData[
    \(meanL1[t_, dt_] := 
      NIntegrate[L[n]/dt, {n, t, t + dt}, MinRecursion \[Rule] 5, 
        MaxRecursion \[Rule] 20]\)], "Input"],

Cell[BoxData[
    \(meanL2[t_, dt_] := 
      NIntegrate[L[n]/dt, {n, t, t + dt}, MaxRecursion \[Rule] 50]\)], "Input"],

Cell[BoxData[
    \(f4dS[ti_, tf_, dt_] := Table[dS[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
    \(f4dS[500000, 504900, 100]\)], "Input"],

Cell[BoxData[
    \({12.641880044210442`, 12.640710929659068`, 12.640199160159133`, 
      12.640447091547088`, 12.64039091086231`, 12.639950636566914`, 
      12.638666964014671`, 12.638294974996398`, 12.637288462906508`, 
      12.636014411462988`, 12.635201269256958`, 12.634285609125756`, 
      12.632780784286622`, 12.63309816970543`, 12.631310348927396`, 
      12.63219400710353`, 12.632059973043939`, 12.630133973414072`, 
      12.628228683228748`, 12.628064578447356`, 12.626277799207724`, 
      12.625519359583468`, 12.625163149677935`, 12.624730864072951`, 
      12.623468515888081`, 12.621659138288244`, 12.618840339421164`, 
      12.617802918537908`, 12.615026452152291`, 12.612337959107503`, 
      12.61110891328932`, 12.608183985508049`, 12.606804662854564`, 
      12.605686011968036`, 12.603921682803847`, 12.602457154109473`, 
      12.600609953896843`, 12.59718052895118`, 12.595590898584696`, 
      12.595055879250134`, 12.593680504689292`, 12.593140681334171`, 
      12.592625919712358`, 12.591338387156943`, 12.59005127374654`, 
      12.589832505359643`, 12.587600021000851`, 12.586997916354793`, 
      12.587904244875197`, 12.587626559012959`}\)], "Output"]
}, Open  ]],

Cell[BoxData[
    \(f4dS1[ti_, tf_, dt_] := Table[dS1[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f4dS2[ti_, tf_, dt_] := Table[dS2[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f5meanEn[ti_, tf_, dt_] := 
      Table[meanEn[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
    \(f5meanEn[500000, 504900, 100]\)], "Input"],

Cell[BoxData[
    \({0.039639910118184`, 0.03963271161933134`, 0.039629449165664174`, 
      0.03963099048463312`, 0.03963066889892715`, 0.03962798736159414`, 
      0.03961992491611754`, 0.03961757623561039`, 0.039611399456745605`, 
      0.03960346002792981`, 0.03959837910407218`, 0.03959267690406082`, 
      0.039583318980068255`, 0.03958528537869538`, 0.03957418863607015`, 
      0.03957970066487407`, 0.039578814453216225`, 0.03956688442810854`, 
      0.039555043133791636`, 0.039553975782947705`, 0.0395428757815655`, 
      0.03953817938428158`, 0.03953591354137304`, 0.03953325978787017`, 
      0.039525461044009474`, 0.03951421903884698`, 0.03949664391067797`, 
      0.03949025130115481`, 0.03947302604496278`, 0.039456344302044734`, 
      0.03944868281678648`, 0.03943039068400488`, 0.03942197137810627`, 
      0.03941495924352337`, 0.039404150728194594`, 0.03939505185943519`, 
      0.03938356330963475`, 0.03936232504588549`, 0.039352389463110725`, 
      0.039349161140928055`, 0.0393405521445065`, 0.03933723645545817`, 
      0.03933399513208105`, 0.03932609667158232`, 0.03931806585647296`, 
      0.039316728594894136`, 0.03930288283531655`, 0.039299176412584104`, 
      0.039304770464229845`, 0.03930306740502272`}\)], "Output"]
}, Open  ]],

Cell[BoxData[
    \(f5meanEn1[ti_, tf_, dt_] := 
      Table[meanEn1[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f5meanEn2[ti_, tf_, dt_] := 
      Table[meanEn2[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f6meanL[ti_, tf_, dt_] := 
      Table[meanL[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
    \(f6meanL[500000, 504900, 100]\)], "Input"],

Cell[BoxData[
    \({0.0637104647093931`, 0.06370449706951523`, 0.06370185077600003`, 
      0.06370306636213681`, 0.06370285734005839`, 0.06370061968176075`, 
      0.06369397630420363`, 0.0636920652161944`, 0.06368697594656327`, 
      0.06368047925344097`, 0.06367621775542875`, 0.06367159751733009`, 
      0.0636638488278637`, 0.06366549725731353`, 0.06365634777527221`, 
      0.06366090126487471`, 0.0636601476497167`, 0.06365034142186032`, 
      0.06364058590669862`, 0.06363970375985424`, 0.06363059105887979`, 
      0.06362666444241365`, 0.06362487222041499`, 0.06362262818244424`, 
      0.06361624056795236`, 0.06360694246759824`, 0.06359250086853753`, 
      0.06358722728215703`, 0.06357301023799292`, 0.0635592800709626`, 
      0.0635529565029303`, 0.06353789876315817`, 0.06353088605487361`, 
      0.06352516933223006`, 0.0635162180643787`, 0.06350873807867724`, 
      0.06349926176650543`, 0.06348170437972095`, 0.06347354481864621`, 
      0.06347084194596901`, 0.06346379199781345`, 0.0634609786157269`, 
      0.06345836994779891`, 0.06345178806281332`, 0.06344523229034767`, 
      0.06344403872057294`, 0.06343268653406044`, 0.06342956935843225`, 
      0.06343424612766622`, 0.06343276263349684`}\)], "Output"]
}, Open  ]],

Cell[BoxData[
    \(f6meanL1[ti_, tf_, dt_] := 
      Table[meanL1[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f6meanL2[ti_, tf_, dt_] := 
      Table[meanL2[t, dt], {t, ti, tf, dt}]\)], "Input"],

Cell[BoxData[
    \(f7graphic[t_, sz_] := 
      Graphics[{{Circle[{0, 0}, l], RGBColor[0, 1, 0]}, {PointSize[ .04], 
            RGBColor[1, 0, 0], Point[{X[t], Y[t]}]}}, Axes \[Rule] True, 
        AxesLabel \[Rule] {StyleForm["\<x(metre)\>", FontSize \[Rule] 15, 
              FontWeight \[Rule] "\<Bold\>"], 
            StyleForm["\<y(metre)\>", FontSize -> 15, 
              FontWeight -> "\<Bold\>"]}, AspectRatio \[Rule] Automatic, 
        AxesStyle \[Rule] {{RGBColor[1, 0.5, 0], 
              Thickness[0.01]}, {RGBColor[1, 0.5, 0], Thickness[0.01]}}, 
        PlotRange \[Rule] {{\(-sz\), sz}, {\(-sz\), sz}}, 
        ImageSize \[Rule] {500, 500}, 
        Prolog \[Rule] {RGBColor[0, 1, 0], Thickness[0.02]}]\)], "Input"],

Cell[BoxData[
    \(Clear[ti, tf, dt]\)], "Input"],

Cell[BoxData[
    \(f8table[ti_, tf_, dt_] := 
      Table[Show[f7graphic[t, 0.22]], {t, ti, tf, dt}]\)], "Input"]
},
FrontEndVersion->"4.2 for Microsoft Windows",
ScreenRectangle->{{0, 1024}, {0, 690}},
WindowSize->{1016, 659},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
StyleDefinitions -> "Classroom.nb"
]

(*******************************************************************
Cached data follows.  If you edit this Notebook file directly, not
using Mathematica, you must remove the line containing CacheID at
the top of  the file.  The cache data will then be recreated when
you save this file from within Mathematica.
*******************************************************************)

(*CellTagsOutline
CellTagsIndex->{}
*)

(*CellTagsIndex
CellTagsIndex->{}
*)

(*NotebookFileOutline
Notebook[{
Cell[1754, 51, 89, 1, 50, "Input"],
Cell[1846, 54, 1321, 22, 210, "Input"],
Cell[3170, 78, 138, 2, 50, "Input"],
Cell[3311, 82, 88, 1, 50, "Input"],
Cell[3402, 85, 46, 1, 50, "Input"],
Cell[3451, 88, 820, 14, 150, "Input"],
Cell[4274, 104, 54, 1, 50, "Input"],
Cell[4331, 107, 916, 15, 170, "Input"],
Cell[5250, 124, 60, 1, 50, "Input"],
Cell[5313, 127, 63, 1, 50, "Input"],
Cell[5379, 130, 69, 1, 50, "Input"],
Cell[5451, 133, 63, 1, 50, "Input"],
Cell[5517, 136, 69, 1, 50, "Input"],
Cell[5589, 139, 84, 1, 50, "Input"],
Cell[5676, 142, 229, 4, 70, "Input"],
Cell[5908, 148, 184, 3, 50, "Input"],
Cell[6095, 153, 160, 3, 50, "Input"],
Cell[6258, 158, 259, 5, 70, "Input"],
Cell[6520, 165, 81, 1, 50, "Input"],
Cell[6604, 168, 86, 1, 50, "Input"],
Cell[6693, 171, 223, 4, 50, "Input"],
Cell[6919, 177, 236, 4, 70, "Input"],
Cell[7158, 183, 165, 3, 50, "Input"],
Cell[7326, 188, 136, 3, 50, "Input"],
Cell[7465, 193, 69, 1, 50, "Input"],
Cell[7537, 196, 221, 4, 70, "Input"],
Cell[7761, 202, 152, 3, 50, "Input"],
Cell[7916, 207, 119, 2, 50, "Input"],
Cell[8038, 211, 89, 1, 50, "Input"],

Cell[CellGroupData[{
Cell[8152, 216, 58, 1, 50, "Input"],
Cell[8213, 219, 1190, 17, 106, "Output"]
}, Open  ]],
Cell[9418, 239, 91, 1, 50, "Input"],
Cell[9512, 242, 91, 1, 50, "Input"],
Cell[9606, 245, 104, 2, 50, "Input"],

Cell[CellGroupData[{
Cell[9735, 251, 62, 1, 50, "Input"],
Cell[9800, 254, 1256, 17, 125, "Output"]
}, Open  ]],
Cell[11071, 274, 106, 2, 50, "Input"],
Cell[11180, 278, 106, 2, 50, "Input"],
Cell[11289, 282, 102, 2, 50, "Input"],

Cell[CellGroupData[{
Cell[11416, 288, 61, 1, 50, "Input"],
Cell[11480, 291, 1238, 17, 125, "Output"]
}, Open  ]],
Cell[12733, 311, 104, 2, 50, "Input"],
Cell[12840, 315, 104, 2, 50, "Input"],
Cell[12947, 319, 738, 12, 130, "Input"],
Cell[13688, 333, 50, 1, 50, "Input"],
Cell[13741, 336, 114, 2, 50, "Input"]
}
]
*)



(*******************************************************************
End of Mathematica Notebook file.
*******************************************************************)

